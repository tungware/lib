/*---------------------------------------------------------------------------*/
/* TWDB.cpp                                                                  */
/*                                                                           */
/* 2014-06-07 First version                                              HS  */
/* 2021-11-01 General cleanup + use setForwardOnly() for greater speed   HS  */
/* 2021-11-28 Say hello to asynchronous processing                       HS  */
/* 2022-02-25 Implement error callback only for the asynchronous db chap HS  */
/*---------------------------------------------------------------------------*/

#include "TWDB_p.h"   // (which #includes TWDB.h)

#include "qsqlerror.h"
#include "qsqlquery.h"
#include "qsqlrecord.h"

// MacOS and Linux requires some .ini file fiddling
#if defined(Q_OS_MAC) || defined(Q_OS_LINUX)
#include <QDir>
#include <QTextStream>
#endif

// BobbyTables ctor ----------------------------------------------------------
BobbyTables::BobbyTables(QString sTableNameOrNames)
{
// support either a single table name or a comma-separated list
    slRaw = TWUtils::commaList2StringList(sTableNameOrNames);
}

// the private helper for TWDB -----------------------------------------------
struct TWDBPrivate
{
// keep our db connection stashed here
    QSqlDatabase sqlDB;

    QString sConnectionName; // connection name we'll use when adding the db
    bool    bODBCReadOnly;   // if true, sets the ODBC readonly option
};

//----------------------------------------------------------------------------
// 2021-11-28 Say hello to a minor static helper for oomposing SQL statements
// (need BobbyTables as a pointer so that the base class can be overridden)
static QString selectHelper(QString sColumns, BobbyTables* pBT, QString sWhere, QString sOrderBy)
{
// currently we require the columns string to be nonempty (a single '*' will suffice)
    if (sColumns.isEmpty())
        fubar("sColumns cannot be empty");

// build the select string
    QString sSQL = "select " + sColumns + " from ";

// cook the raw table name(s) and add them
    sSQL += pBT->cook();

// got a nonempty "where" SQL clause to add?
    if (!sWhere.isEmpty())
        sSQL += " where " + sWhere;

// got a nonempty "order by" SQL clause? to add?
    if (!sOrderBy.isEmpty())
        sSQL += " order by " + sOrderBy;

// merry xmas
    return sSQL;
}

//----------------------------------------------------------------------------
// TWDB ctor and dtor
TWDB::TWDB()
{
// new up our private helper and imit our stuffs
    d_ptr = new TWDBPrivate;
    d_ptr->sConnectionName = "TWDB_1";  // default name
    d_ptr->bODBCReadOnly   = false;

    nNoOfOpens = 0;  // keep track of # opens (only one open/close cycle please)
    sError     = ""; // no errors for now
}

// TWDB dtor
TWDB::~TWDB()
{
// just delete our private chap
    delete d_ptr;
}

//----------------------------------------------------------------------------
// setConnectionName and setODBCReadOnly
//
// 2022-02-07 First version
//----------------------------------------------------------------------------
void TWDB::setConnectionName(QString sConnectionName)
{
// only allow this call before opening the db
    if (nNoOfOpens > 0)
        fubar("connection name must be set before open");

    d_ptr->sConnectionName = sConnectionName;
}

void TWDB::setODBCReadOnly()
{
// only allow this call before opening the db
    if (nNoOfOpens > 0)
        fubar("set odbc readonly mode must be done before open");

    d_ptr->bODBCReadOnly = true;
}

//-----------------------------------------------------------------------------
// open the db, two flavors: (returns "" if successful, else an error)
// vanilla ODBC
// explicitly for SQLServer (which calls the vanilla one)
//
// 2014-05-23 First version
// 2022-02-07 Support db connection name and the readonly SQL option
// 2024-07-18 Try MS ODBC Driver also on Linux and fallback to freetds
//-----------------------------------------------------------------------------
QString TWDB::open(QString sDSN, QString sUserName, QString sPassword)
{
// check that this the first (and only) open done for this TWDB instance
    if (++nNoOfOpens > 1)
        fubar("sorry but only one open/close cycle allowed for a TWDB instance");

// try to load ODBC and fubar if it fails
    d_ptr->sqlDB = QSqlDatabase::addDatabase("QODBC",d_ptr->sConnectionName);
    if (!d_ptr->sqlDB.isValid())
        fubar("addDatabase() failed with error = '%s'",qUtf8Printable(d_ptr->sqlDB.lastError().text()));

// we want ODBC version 3 please
    d_ptr->sqlDB.setConnectOptions("SQL_ATTR_ODBC_VERSION=SQL_OV_ODBC3;");

// also try to lower the initial login timeout from 10 to 5 seconds
    d_ptr->sqlDB.setConnectOptions("SQL_ATTR_LOGIN_TIMEOUT=5;");

// want a readonly ODBC experience?
    if (d_ptr->bODBCReadOnly)
        d_ptr->sqlDB.setConnectOptions("SQL_ATTR_ACCESS_MODE=SQL_MODE_READ_ONLY;");

// ok set the controls for the heart of the sun
    d_ptr->sqlDB.setDatabaseName(sDSN);
    d_ptr->sqlDB.setUserName(sUserName);
    d_ptr->sqlDB.setPassword(sPassword);

// try to open, return "" if this works nice and dandy
    sError = "";              // optimistic default (i.e. no error occurred)
    if (d_ptr->sqlDB.open())  // return true ---> worked fine
        return "";

// else get the error message
    sError = d_ptr->sqlDB.lastError().nativeErrorCode();
    if (sError.isEmpty())
    {
    // try again with .text
        sError = d_ptr->sqlDB.lastError().text();

    // make sure we really have a non-empty message
        if (sError.isEmpty())
            sError = "open(db) failed";
    }

// that's all, return with an error
    return sError;
}

// another flavor of open which calls the vanilla open above after some preamble
QString TWDB::openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo /* = 1433 */)
{
    QString sDriver = "";

#if defined(Q_OS_WIN)
// on Windows use the vanilla native driver
    sDriver = "SQL Server";
#endif

#if defined(Q_OS_MACOS)
// on the Mac we expect MS ODBC driver 17 to be installed
    sDriver = "ODBC Driver 17 for SQL Server";
#endif

#if defined(Q_OS_LINUX)
// on Linux we expect MS ODBC driver 18 which requires the TrustServerCertificate stuff (breaking change)
    sDriver = "ODBC Driver 18 for SQL Server; TrustServerCertificate=YES";
#endif

// stuck with some other OS? sorry you're up the creek currently
    if ("" == sDriver)
        fubar("unsupported OS for the openSQLServer() call");

// build a DSN for SQLServer + IP and Database into a DSN string and call the generic open() call above
    return open("Driver=" + sDriver + "; Server=" + sServerAddr + "," + QString::number(nPortNo) + "; Database=" + sDatabase,sUserName,sPassword);

/* additional notes:
for Linux: install Microsoft's ODBC driver

for the Mac: brew install cmake unixodb and download Microsoft's ODBC driver
also we need to rebuild the libqsqlodbc.dylib plugin in Qt (the original one is built for iodbc, we want to use unixodbc):

download Qt's source to (for example) ~/Downloads/qt-everywhere-src-6.2.4
$ cd ~/Downloads/qt-everywhere-src-6.2.4/qtbase/src/plugins/sqldrivers
$ ~/Qt/6.2.4/macos/bin/qt-cmake -DODBC_ROOT=/usr/local/opt/unixodbc -DCMAKE_OSX_ARCHITECTURES="x86_64" .
$ make
$ cp plugins/sqldrivers/libqsqlodbc.dylib ~/Qt/6.2.4/macos/plugins/sqldrivers

Two pitfalls on the Mac:
1) check that the dylib is really linked to unixODBC (and not iODBC)
otool -l libqsqlodbc.dylib | grep odbc   (should return: name /usr/local/opt/unixodbc/lib/libodbc.2.dylib)
2) verify that the old (iodbc-flavored) libqsqlodbc.dylib is removed (so that the plugin is not accidentally loaded)

# for future reference: if we ever need to build MySQL on the Mac:
brew install mysql
$ cd ~/Downloads/qt-everywhere-src-6.2.4/qtbase/src/plugins/sqldrivers
$ ~/Qt/6.2.4/macos/bin/qt-cmake -DMySQL_LIBRARY=/usr/local/opt/mysql/lib/libmysqlclient.dylib -DMySQL_INCLUDE_DIR=/usr/local/opt/mysql/include/mysql .
$ make

# additional notes for building MySQL on a ARM Mac:
first 2 steps above the same, then build using Ninja
~/Qt/6.7.0/macos/bin/qt-cmake -GNinja -DMySQL_LIBRARY=/opt/homebrew/opt/mysql/lib/libmysqlclient.dylib -DMySQL_INCLUDE_DIR=/opt/homebrew/opt/mysql/include/mysql -DCMAKE_OSX_ARCHITECTURES="arm64" .
sed -i -e 's/-arch x86_64/-arch arm64/g' build.ninja
cmake --build .
*/
}

//----------------------------------------------------------------------------
// close (yes it closes the db)
//
// 2014-05-11 First version
//----------------------------------------------------------------------------
void TWDB::close()
{
// this one is much easier than open()
    sError = "";
    d_ptr->sqlDB.close();

// also jettison our private struct with the QSqlDatabase inside
    delete d_ptr;
    d_ptr = nullptr;    // (so that the dtor doesn't do a delete after free)
}

//----------------------------------------------------------------------------
// isOpen (returns true if db is successfully opened)
//
// 2015-09-24 First version
//----------------------------------------------------------------------------
bool TWDB::isOpen()
{
    sError = "";
    return d_ptr->sqlDB.isOpen();
}

//-----------------------------------------------------------------------------
// select (3 flavors)
// these require table enum(s) (or:ed together) for the SQL table names
//
// 2021-11-28 First version
// 2022-08-10 Use a reference to BobbyTables to support overriding
//-----------------------------------------------------------------------------
int TWDB::select(QString sColumns, BobbyTables& BT, fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
    return sqlcmd(selectHelper(sColumns,&BT,"",""),rp,re);
}

int TWDB::select(QString sColumns, BobbyTables& BT, QString sWhere, fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
    return sqlcmd(selectHelper(sColumns,&BT,sWhere,""),rp,re);
}

int TWDB::select(QString sColumns, BobbyTables& BT, QString sWhere, QString sOrderBy, fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
    return sqlcmd(selectHelper(sColumns,&BT,sWhere,sOrderBy),rp,re);
}

//----------------------------------------------------------------------------
// sqlcmd (the 3 select() flavors above call through this one)
//
// 2021-11-20 First version
//----------------------------------------------------------------------------
int TWDB::sqlcmd(QString sSQL,fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
// query time
    QSqlQuery q(d_ptr->sqlDB);
    q.setForwardOnly(true); // more speed please (ok but now concurrent calls on same db instance usually fail)

// bombs away
    sError = "";
    if (!q.exec(sSQL))
    {
    // failed, set the error
        sError = "Your sqlcmd failed: " + sSQL;

    // and return # rows processed (as 0) to the client
        return 0;
    }

// retrieve the column names and put them in a list
    auto r = q.record();
    QStringList ln;
    for (int n = 0; (n < r.count()); ++n)
        ln += r.fieldName(n);

// retrieve the column data (if any)
    int nRows = 0;  // count the rows
    while (q.next())
    {
    // stuff all columns for this row into a map (using the column names as keys)
        QVariantMap m;
        for (auto n : ln)
            m.insert(n,q.value(n));

    // call the client callback (row processor) with the map
        rp(m);    // this will automagically/implicitly construct a DBRwowStruct

    // count 'em rows
        ++nRows;
    }

// any DBRowsExpectation set?
// (we've checked above that an error callback exists for a nondefault dbexpectation)
    switch (re)
    {
    case eDBRowsExpectAnyNumber  :
    // default: allow any result, any # of rows (no error callback needs to be set)
         break;

    case eDBRowsExpectZero       :
        if (nRows != 0)
            sError = "sqlcmd: didn't expect any rows: " + sSQL;
        break;

    case eDBRowsExpectZeroOrOne  :
        if (nRows > 1)
            sError = QString("sqlcmd: got more rows (%1) than the expected zero or one row: ").arg(nRows) + sSQL;
        break;

    case eDBRowsExpectExactlyOne :
        if (nRows < 1)
            sError = "sqlcmd: didn't get any row (no rows found): " + sSQL;
        if (nRows > 1)
            sError = QString("sqlcmd: got more than one row (%1 rows): ").arg(nRows) + sSQL;
        break;

    case eDBRowsExpectOneOrMore  :
        if (nRows < 1)
            sError = "sqlcmd: expected at least one row: " + sSQL;
        break;
    }

// that's all, return # of rows
    return nRows;
}


//================================================================================================
// introducing an asynchronous DB flavor, ctor and dtor
TWAsyncDB::TWAsyncDB()
{
// wire up our async helper
    pHelper = new DBAsyncHelper();

// set a default connection name
    pHelper->worker->setConnectionName("TWAsyncDB_1");

// keep track of # opens (only one open/close cycle please)
    nNoOfOpens = 0;
}

TWAsyncDB::~TWAsyncDB()
{
// if worker is running stop it
    pHelper->stopDBWorker();

// ok tossing time
    delete pHelper;
}

//----------------------------------------------------------------------------
// setConnectionName and setODBCReadOnly (call before open)
// (these are simple calls we can pass on to the async worker directly)
//
// 2022-02-07 First version
//----------------------------------------------------------------------------
void TWAsyncDB::setConnectionName(QString sConnectionName)
{
    pHelper->worker->setConnectionName(sConnectionName);
}

void TWAsyncDB::setODBCReadOnly()
{
    pHelper->worker->setODBCReadOnly();
}

void TWAsyncDB::setDBErrorCallback(fDBErrorCallback_t fDBError)
{
    pHelper->setDBErrorCallback(fDBError);
}

//----------------------------------------------------------------------------
// open
//
// 2021-11-26 First version
//----------------------------------------------------------------------------
QString TWAsyncDB::open(QString sDSN, QString sUserName, QString sPassword)
{
// keep track of # of opens (we support only 1 right now)
    if (++nNoOfOpens > 1)
        fubar("sorry but only one open/close cycle allowed for a TWASyncDB instance");

// fire up the DB worker
    pHelper->startDBWorker();

// and try to open the db (returns an error message or "")
    return pHelper->open(sDSN,sUserName,sPassword);
}

//----------------------------------------------------------------------------
// openSQLServer (which also starts the worker thread)
//
// 2021-11-20 First version
//----------------------------------------------------------------------------
QString TWAsyncDB::openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo /* = 1433 */)
{
// check that this the first (and only) open done
    if (++nNoOfOpens > 1)
        fubar("sorry but only one open/close cycle allowed for a TWASyncDB instance");

// fire up our DB worker
    pHelper->startDBWorker();

// and try to open the db (returns an error message or "")
    return pHelper->openSQLServer(sServerAddr,sDatabase,sUserName,sPassword,nPortNo);
}

//----------------------------------------------------------------------------
// close (which also closes the worker thread)
//
// 2021-11-30 First version
// 2022-03-17 Close forcibly if needed (if we're waiting on a blocking call)
//----------------------------------------------------------------------------
void TWAsyncDB::close()
{
// open done?
    if (0 == nNoOfOpens)
        return; // no open() done, so no close() is needed

// if the db worker isn't running then no close() can be done
    if (!pHelper->isDBWorkerRunning())
        return;

// and we're not blocking? (if we are blocking, never mind closing that db)
    if (!isBlocking())
    // close the db in the worker
        pHelper->close();

// ok kiss the worker goodbye
    pHelper->stopDBWorker();
}

//----------------------------------------------------------------------------
// isBlocking
//
// 2022-03-17 First version
//----------------------------------------------------------------------------
bool TWAsyncDB::isBlocking()
{
// if the eventloop is running ---> means we're blocking
    return pHelper->eventLoop.isRunning();
}

//----------------------------------------------------------------------------
// isOpen
//
// 2021-12-05 First version
// 2022-04-23 Fubar if called when we're blocking
//----------------------------------------------------------------------------
bool TWAsyncDB::isOpen()
{
// open done?
    if (0 == nNoOfOpens)
        return false; // no open() call yet

// db worker running?
    if (!pHelper->isDBWorkerRunning())
        return false;

// if we're blocking then venture south
    if (isBlocking())
        fubar("cannot call isOpen() when we're blocking");

// ok so ask our async helper
    return pHelper->isOpen();
}

//-----------------------------------------------------------------------------
// select (3 flavors)
// these require table enum(s) (or:ed together) for the SQL table names
//
// 2021-12-05 First version
// 2022-08-10 Use a reference to BobbyTables to support overriding
//-----------------------------------------------------------------------------
int TWAsyncDB::select(QString sColumns, BobbyTables& BT, fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
    return sqlcmd(selectHelper(sColumns,&BT,"",""),rp,re);
}

int TWAsyncDB::select(QString sColumns, BobbyTables& BT, QString sWhere, fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
    return sqlcmd(selectHelper(sColumns,&BT,sWhere,""),rp,re);
}

int TWAsyncDB::select(QString sColumns, BobbyTables& BT, QString sWhere, QString sOrderBy, fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
    return sqlcmd(selectHelper(sColumns,&BT,sWhere,sOrderBy),rp,re);
}

//----------------------------------------------------------------------------
// sqlcmd (the 3 select() flavors above call through this one)
//
// 2021-12-05 First version
// 2022-04-23 Fubar if called when we're blocking
//----------------------------------------------------------------------------
int TWAsyncDB::sqlcmd(QString sSQL,fDBRowProcessor_t rp, DBRowsExpectation re /* = eDBRowsExpectAnyNumber */)
{
// make sure we have a live db worker
    if (!pHelper->isDBWorkerRunning())
        fubar("The DB worker is not running");  // e.g. close() has been called

// also if we're blocking then venture south
    if (isBlocking())
        fubar(QString("cannot call sqlcmd('%1') when we're blocking").arg(sSQL));

// ok call our ansync helper
    return pHelper->sqlcmd(sSQL,rp,re);
}

//----------------------------------------------------------------------------
// getError
// (this is simple (thread-safe) so we can call the worker directly)
//
// 2022-02-25 First version
//----------------------------------------------------------------------------
QString TWAsyncDB::getError()
{
// if the db worker is stopped return an error for that
    if (!pHelper->isDBWorkerRunning())
        return "the db worker is not running";

// else get the most recent error from the db
    return pHelper->worker->getError();
}
