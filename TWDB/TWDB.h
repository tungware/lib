/*---------------------------------------------------------------------------*/
/* TWDB.h                                                                    */
/*                                                                           */
/* 2014-06-07 First version                                              HS  */
/* 2021-11-01 General cleanup + use setForwardOnly() for greater speed   HS  */
/* 2021-11-29 Give a big welcome to the async (nonblocking) db class     HS  */
/*---------------------------------------------------------------------------*/

#pragma once

// need TWUtils please
#include INCLUDETWUTILS

#if defined(TWDB_LIBRARY) || defined(TWSTATICBUILD)
#define TWDBSHARED_EXPORT Q_DECL_EXPORT
#else
#define TWDBSHARED_EXPORT Q_DECL_IMPORT
#endif

// internal stuff stored here
struct TWDBPrivate;

// ===============================================================================================
// basic bobbytables class for SQL table name fiddling
class TWDBSHARED_EXPORT BobbyTables
{
public:
    BobbyTables(QString sTableNameOrNames); // one table name or table names comma-separated
    QStringList slRaw;                      // one or more uncooked/undecorated table names
    virtual QString cook() { return slRaw.join(","); } // a trivial cooker (meant to be overridden)
};

// ===============================================================================================
// select() and sqlcmd() database row processor (typically called from your lambda or Qt slot)
struct TWDBSHARED_EXPORT DBRowStruct
{
// a map of the column data for this row (the column name is the index/key)
    QVariantMap m;

// use this ctor to set the map
    DBRowStruct(QVariantMap mColumns) { m = mColumns; }

// client functions (for retrieving column data
    bool      boolValue         (QString sColumnName) { return variantValue(sColumnName).toBool();             }
    QDate     dateValue         (QString sColumnName) { return variantValue(sColumnName).toDate();             }
    QTime     timeValue         (QString sColumnName) { return variantValue(sColumnName).toTime();             }
    QDateTime dateTimeValue     (QString sColumnName) { return variantValue(sColumnName).toDateTime();         }
    int       intValue          (QString sColumnName) { return variantValue(sColumnName).toInt();              }
    qint64    longlongValue     (QString sColumnName) { return variantValue(sColumnName).toLongLong();         }
    QString   stringValue       (QString sColumnName) { return variantValue(sColumnName).toString();           }
    QString   stringValueTrimmed(QString sColumnName) { return variantValue(sColumnName).toString().trimmed(); }

// the client functions above all use this function (to simplify error checking)
    QVariant variantValue(QString sColumnName)
    {
    // check that the column name actually exists in the map
        if (!m.contains(sColumnName))
            fubar("Column name '%s' not found",qUtf8Printable(sColumnName));

    // ok return the variant
        return m[sColumnName];
    }
};

// -----------------------------------------------------------------------------------------------
// introduce an enum for how many rows we can expect to be returned in a select() or sqlcmd() call
// if # of rows do not match, the error callback is invoked
enum DBRowsExpectation { eDBRowsExpectAnyNumber, eDBRowsExpectZero, eDBRowsExpectZeroOrOne, eDBRowsExpectExactlyOne, eDBRowsExpectOneOrMore };

// row processing/digesting callback (a std::function type)
using fDBRowProcessor_t = std::function<void(DBRowStruct rs)>;


// ===============================================================================================
// synchronous (blocking) db class (this is not normally what you want)
class TWDBSHARED_EXPORT TWDB
{
public:
    TWDB();
    ~TWDB();

// call these *before* open
    void setConnectionName(QString sConnectionName);  // use this if you need a 2nd, 3rd etc. TWDB instance
    void setODBCReadOnly();                           // sets the DB in ODBC readonly mode (can speed up things)

// opens the db. returns "" if successful else the error
    QString open(QString sDSN, QString sUserName, QString sPassword);

// introducing another open() flavor for TSQL/MSSQLServer (also works in Linux and MacOS)
    QString openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo = 1433);

// yes, this closes the db
    void close();

// is db currently opened or closed?
    bool isOpen();

// select() and sqlcmd() functions ---------------------------------------------------------------
// these return # of rows processed
    int select(QString sColumns, BobbyTables& BT,                                   fDBRowProcessor_t rf, DBRowsExpectation re = eDBRowsExpectAnyNumber);
    int select(QString sColumns, BobbyTables& BT, QString sWhere,                   fDBRowProcessor_t rf, DBRowsExpectation re = eDBRowsExpectAnyNumber);
    int select(QString sColumns, BobbyTables& BT, QString sWhere, QString sOrderBy, fDBRowProcessor_t rf, DBRowsExpectation re = eDBRowsExpectAnyNumber);

// the above 3 select() flavors all call into this one (which also returns # of rows seen)
    int sqlcmd(QString sSQL, fDBRowProcessor_t rp, DBRowsExpectation re = eDBRowsExpectAnyNumber);

// after every call, you can use this function to retrieve the error message (if any)
// (note: the openxx() functions return the error but you need this for the other functions)
    QString getError() { return sError; }

// -----------------------------------------------------------------------------------------------
private:
// private stash
    TWDBPrivate* d_ptr;

// reentrancy/reuse checker
    int nNoOfOpens;  // for simplicity, only allow one open/close cycle

// error message (if nonblank)
    QString sError;
};


// ===============================================================================================
// asynchronous db class (this is the good stuff)

// for error callbacks (for sql errors or when the # of rows returned does not match the DBRowsExpectation enum
class DBAsyncHelper;    // private helper class
using fDBErrorCallback_t = std::function<void(QString sError)>;

class TWDBSHARED_EXPORT TWAsyncDB
{
public:
    TWAsyncDB();
    ~TWAsyncDB();

// call these *before* open
    void setConnectionName(QString sConnectionName);  // use this if you need a 2nd, 3rd etc. TWDB instance
    void setODBCReadOnly();                           // sets the DB in ODBC readonly mode (can speed up things)

// use this to set an error callback (can be done before or after opening the db)
    void setDBErrorCallback(fDBErrorCallback_t fDBError);

// opens the db (call one of them but not both)
    QString open(QString sDSN, QString sUserName, QString sPassword);
    QString openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo = 1433);

// yes, this closes the db
    void close();

// are we inside the event loop (i.e. waiting for the worker)?
    bool isBlocking();

// is db currently opened or closed?
    bool isOpen();

// select() and sqlcmd() functions ---------------------------------------------------------------
// these return # of rows processed
    int select(QString sColumns, BobbyTables& BT,                                   fDBRowProcessor_t rf, DBRowsExpectation re = eDBRowsExpectAnyNumber);
    int select(QString sColumns, BobbyTables& BT, QString sWhere,                   fDBRowProcessor_t rf, DBRowsExpectation re = eDBRowsExpectAnyNumber);
    int select(QString sColumns, BobbyTables& BT, QString sWhere, QString sOrderBy, fDBRowProcessor_t rf, DBRowsExpectation re = eDBRowsExpectAnyNumber);

// the above 3 select() flavors all call into this one (which also returns # of rows processed)
    int sqlcmd(QString sSQL, fDBRowProcessor_t rp, DBRowsExpectation re = eDBRowsExpectAnyNumber);

// call this function to retrieve a possible error message, or use the error callback function above (can be used independently of each other)
    QString getError();

// -----------------------------------------------------------------------------------------------
private:
// private helper chap
    DBAsyncHelper* pHelper;

// reentrancy/reuse checker
    int  nNoOfOpens;  // for simplicity, only allow one open/close cycle
};
