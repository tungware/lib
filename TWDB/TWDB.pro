#--------------------------------------------------------------------------#
# TWDB.pro                                                                 #
#                                                                          #
# 2014-06-06 First version                                             HS  #
# 2021-11-28 Say hello to asynchronous processing                      HS  #
#--------------------------------------------------------------------------#

QT      += core gui widgets sql

DEFINES += TWDB_LIBRARY

HEADERS += TWDB.h TWDB_p.h
SOURCES += TWDB.cpp TWDB_p.cpp

TARGET   = TWDB
TEMPLATE = lib

# include our common QtProjects helper
include(../../include/QtProjects.pri)

# we need TWUtils
DEFINES += $$TWDefineInclude(TWUtils)
LIBS    += $$TWLibLine(TWUtils)
