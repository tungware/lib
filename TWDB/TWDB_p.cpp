/*---------------------------------------------------------------------------*/
/* TWDB_p.cpp                                                                */
/*                                                                           */
/* 2021-11-29 First version                                              HS  */
/* 2022-02-25 Change error mechanism from callback to an error variable  HS  */
/*---------------------------------------------------------------------------*/

#include "TWDB_p.h"

//----------------------------------------------------------------------------
// need just ctor (thread and worker will be deleted vis the finished() signal)
DBAsyncHelper::DBAsyncHelper()
{
// new up the worker
    worker = new DBWorker();

// and reset the error callback
    fDBErrorCallback = nullptr;
}

//----------------------------------------------------------------------------
// startDBWorker
//
// 2021-11-29 First version
//----------------------------------------------------------------------------
void DBAsyncHelper::startDBWorker()
{
// register our row function and row expectation types so that the queued connect() works
    if (0 == qRegisterMetaType<fDBRowProcessor_t>())
        fubar("qRegisterMetaType() failed for fDBRowProcessor_t");

    if (0 == qRegisterMetaType<DBRowsExpectation>())
        fubar("qRegisterMetaType() failed for fDBRowsExpectation");

// give this man his own thread
    worker->moveToThread(&thread);

// wire up the signal/slots needed for the communication between the threads
    connect(&thread,&QThread::started,this,&DBAsyncHelper::returnedVoid);    // initial handshake

// add a connect() from our forwarding funcs to each receiving slot in DBWorker
    connect(this,&DBAsyncHelper::callOpen         ,worker,&DBWorker::open         );
    connect(this,&DBAsyncHelper::callOpenSQLServer,worker,&DBWorker::openSQLServer);
    connect(this,&DBAsyncHelper::callClose        ,worker,&DBWorker::close        );
    connect(this,&DBAsyncHelper::callIsOpen       ,worker,&DBWorker::isOpen       );
    connect(this,&DBAsyncHelper::callSqlcmd       ,worker,&DBWorker::sqlcmd       );

// and the connects for returning back to us (and sometimes returning a value)
    connect(worker,&DBWorker::returnVoid   ,this,&DBAsyncHelper::returnedVoid   );
    connect(worker,&DBWorker::returnBool   ,this,&DBAsyncHelper::returnedBool   );
    connect(worker,&DBWorker::returnInt    ,this,&DBAsyncHelper::returnedInt    );
    connect(worker,&DBWorker::returnQString,this,&DBAsyncHelper::returnedQString);

// once the thread is stopped/finished we can toss the worker
    connect(&thread,&QThread::finished,worker,&DBWorker::deleteLater);

// ok launch time
    thread.start();

// wait for an returnedVoid signal() to be emitted from the worker thread
    waitForGodotAKADBWorker();

// make sure our db worker is up and running now
    if (!isDBWorkerRunning())
        fubar("expected DB worker to be up and running");
}

//----------------------------------------------------------------------------
// isDBWorkerRunning
//
// 2022-03-04 First version
//----------------------------------------------------------------------------
bool DBAsyncHelper::isDBWorkerRunning()
{
    return thread.isRunning();
}

//----------------------------------------------------------------------------
// stopDBWorker
//
// 2021-11-29 First version
// 2022-03-17 Support forcibly stopping (in case we're blocking)
//----------------------------------------------------------------------------
void DBAsyncHelper::stopDBWorker()
{
// worker started?
    if (!isDBWorkerRunning())
        return; // not started so nothing to stop

// worker is running, what about our event loop?
    if (!eventLoop.isRunning())
    {
    // no we're idle, so do a normal exit (sayonara time)
    // (expecting the worker and thread to be tossed via deleteLater signals)
        thread.quit();
        thread.wait();
        return;
    }

// yes, the thread is waiting on a blocking db call, stop it the hard way
    thread.terminate();    // try to terminate
    thread.wait();         // and wait for it

// call the error callback (or fubar if it's not set)
    if (nullptr == fDBErrorCallback)
        fubar("db forcibly closed without an error callback");
    else
        fDBErrorCallback("db forcibly closed");

// that's all, exit the event loop one last time
    eventLoop.exit();
}

//----------------------------------------------------------------------------
// introducing the forwarders, these emits/forwards calls to the db worker
QString DBAsyncHelper::open(QString sDSN, QString sUserName, QString sPassword)
{
    emit callOpen(sDSN,sUserName,sPassword);

    waitForGodotAKADBWorker();
    return sReturned;
}

QString DBAsyncHelper::openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo)
{
    emit callOpenSQLServer(sServerAddr,sDatabase,sUserName,sPassword,nPortNo);

    waitForGodotAKADBWorker();
    return sReturned;
}

void DBAsyncHelper::close()
{
    emit callClose();

    waitForGodotAKADBWorker();
    return;  // just a void (returning nothing)
}

bool DBAsyncHelper::isOpen()
{
    emit callIsOpen();

    waitForGodotAKADBWorker();
    return bReturned;
}

int DBAsyncHelper::sqlcmd(QString sSQL, fDBRowProcessor_t rp, DBRowsExpectation re)
{
    emit callSqlcmd(sSQL,rp,re);

    waitForGodotAKADBWorker();
    return nReturned;
}

//----------------------------------------------------------------------------
// the awaiter (waits for the eventLoop.exit() call in the eventloop)
void DBAsyncHelper::waitForGodotAKADBWorker()
{
// make sure our eventloop is not already on the run
    if (eventLoop.isRunning())
        guruMeditation("waitForGodotAKADBWorker(): event loop is already running!");

// remember the current loop level
    int nLoopLevel = QThread::currentThread()->loopLevel();

// slammer time
    eventLoop.exec();

// we're back, verify we're at the same loop level
    if (QThread::currentThread()->loopLevel() != nLoopLevel)
        fubar("loop levels before and after eventLoop.exec() mismatch (should not happen)");
}

//----------------------------------------------------------------------------
// helper function, invokes the error callback if there's an error
void DBAsyncHelper::doErrorCallbackOnError()
{
// do it?
    if (nullptr == fDBErrorCallback)
        return; // no callback set, skip this

    QString sError = worker->getError();
    if (sError.isEmpty())
        return; // no error, skip this

// ok do it
    fDBErrorCallback(sError);
}

//----------------------------------------------------------------------------
// and here are the slots for arriving back from DBWorker
void DBAsyncHelper::returnedVoid()
{
// arriving from DBWorker with no returned value
    doErrorCallbackOnError();

    eventLoop.exit();
}

void DBAsyncHelper::returnedBool(bool b)
{
// arriving from DBWorker with a boolean
    doErrorCallbackOnError();

    bReturned = b;
    eventLoop.exit();
}

void DBAsyncHelper::returnedInt(int i)
{
// arriving from DBWorker with an int
    doErrorCallbackOnError();

    nReturned = i;
    eventLoop.exit();
}

void DBAsyncHelper::returnedQString(QString s)
{
// arriving from DBWorker with a QString
    doErrorCallbackOnError();

    sReturned = s;
    eventLoop.exit();
}
