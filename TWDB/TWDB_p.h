/*---------------------------------------------------------------------------*/
/* TWDB_p.h                                                                  */
/*                                                                           */
/* 2021-11-28 First version                                              HS  */
/* 2022-02-25 Change error mechanism from callback to an error variable  HS  */
/*---------------------------------------------------------------------------*/
#pragma once

// pull in the vanilla/synchronous db
#include "TWDB.h"

// threads please
#include "qthread.h"

// need this for our connects() across threads
Q_DECLARE_METATYPE(fDBRowProcessor_t)
Q_DECLARE_METATYPE(DBRowsExpectation)

// ----------------------------------------------------------------------------
// introduce the worker bee (runs in another thread and contains a normal TWDB)
// note: all code for this class is here (nothing in TWDP_p.cpp)
class DBWorker : public QObject
{
// Qt boilerplate (so that signals and slots work)
    Q_OBJECT

public:
// these are simple calls, ok to be called directly (no exec())
    void setConnectionName(QString sConnectionName)
    {
        db.setConnectionName(sConnectionName);
    }

    void setODBCReadOnly()
    {
        db.setODBCReadOnly();
    }

    QString getError()
    {
        return db.getError();
    }

// these are time consuming, so use signals Luke
    void open(QString sDSN, QString sUserName, QString sPassword)
    {
        emit returnQString(db.open(sDSN, sUserName, sPassword));
    }
    void openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo)
    {
        emit returnQString(db.openSQLServer(sServerAddr, sDatabase, sUserName, sPassword, nPortNo));
    }
    void close()
    {
        db.close(); emit returnVoid();
    }
    void isOpen()
    {
        emit returnBool(db.isOpen());
    }
    void sqlcmd(QString sSQL, fDBRowProcessor_t rp, DBRowsExpectation re)
    {
        emit returnInt(db.sqlcmd(sSQL,rp,re));
    }

signals:
    void returnVoid();
    void returnBool(bool b);
    void returnInt(int i);
    void returnQString(QString s);

private:
// keep a normal synchronous db instance for the real work
    TWDB db;
};

// ---------------------------------------------------------------------------------
// and a glue chap between TWAsyncDB and DBWorker (holds all the signals and slots)
class DBAsyncHelper : public QObject
{
// Qt boilerplate (so that signals and slots work)
    Q_OBJECT

public:
    DBAsyncHelper();

// async overhead/helpers
    void startDBWorker();      // yes starts the thread with our DB worker inside
    bool isDBWorkerRunning();  // true if the worker thread is up and running
    void stopDBWorker();       // stops and tears down worker and thread

// set the (internal) error callback
    void setDBErrorCallback(fDBErrorCallback_t fDBError) { fDBErrorCallback = fDBError; }

// big wheels
    QThread    thread;
    DBWorker*  worker;
    QEventLoop eventLoop;

// these call db worker thru signals then issues an exec() waiting for a returning signal
    QString open(QString sDSN, QString sUserName, QString sPassword);
    QString openSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo);
    void close();
    bool isOpen();
    int sqlcmd(QString sSQL, fDBRowProcessor_t rp, DBRowsExpectation re);

// these are the signals we emit
signals:
    void callOpen(QString sDSN, QString sUserName, QString sPassword);
    void callOpenSQLServer(QString sServerAddr, QString sDatabase, QString sUserName, QString sPassword, int nPortNo);
    void callClose();
    void callIsOpen();
    void callSqlcmd(QString sSQL, fDBRowProcessor_t rp, DBRowsExpectation re);

public:
// here is where we block/wait inside an exec() call until the slots below are called
    void waitForGodotAKADBWorker();

public slots:
// the slots called from the DB worker (when it's done)
    void returnedVoid();
    void returnedBool(bool b);
    void returnedInt(int i);
    void returnedQString(QString s);

private:
// store the error callback here
    fDBErrorCallback_t fDBErrorCallback;

// helper method for calling the error callback (if there was an error)
    void    doErrorCallbackOnError();

// one of thse are set by the receiving slots (except for returnedVoid of course)
    bool    bReturned;
    int     nReturned;
    QString sReturned;
};
