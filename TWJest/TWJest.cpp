/*--------------------------------------------------------------------------*/
/* TWJest.cpp                                                               */
/*                                                                          */
/* 2022-07-22 First version                                             HS  */
/*--------------------------------------------------------------------------*/
#include "TWJest_p.h"

// another TWJest instance is born
TWJest::TWJest()
{
// wire up our helper class (where the real work is done)
    pHelper = new TWJestHelper();
}

// and stops
TWJest::~TWJest()
{
    delete pHelper;
}

//----------------------------------------------------------------------------
// setErrorCallback
//
// 2022-07-24 First version
//----------------------------------------------------------------------------
void TWJest::setErrorCallback(fJestErrorCallback_t fJestError)
{
    pHelper->setJestErrorCallback(fJestError);
}

//----------------------------------------------------------------------------
// setDelay
//
// 2022-11-15 First version
//----------------------------------------------------------------------------
void TWJest::setDelay(int nMilliSeconds)
{
    pHelper->setDelay(nMilliSeconds);
}

//----------------------------------------------------------------------------
// parseHeaderFile
//
// 2022-10-12 First version
//----------------------------------------------------------------------------
NameValueList TWJest::parseHeaderFile(QString sFileName)
{
    NameValueList lNV;

// get all of the file into a stringlist
    for (auto sLine : TWTextFileReader::readAll(sFileName))
    {
    // expect lines like this: -H "Name: value" use split() to parse
        auto slQuotes = sLine.split("\"");
        if (3 != slQuotes.count())
            continue;   // not recognized, skip this line

        if ("-H" != slQuotes[0].trimmed())
            continue;   // not recognized, skip this line

        if ("" != slQuotes[2].trimmed())
            continue;   // not recognized, skip this line

    // split again to get the pair
        auto slNV = slQuotes[1].split(":");
        if (2 != slNV.count())
            continue;   // not recognized, skip this line

    // ok store the pair
        lNV.append({slNV[0].trimmed(),slNV[1].trimmed()});
    }

    return lNV;
}

//----------------------------------------------------------------------------
// writeHeaderFile
//
// 2022-10-21 First version
//----------------------------------------------------------------------------
void TWJest::writeHeaderFile(QString sFileName, NameValueList lNV)
{
// write a curl header file (that can be read by parseHeaderFile() above)
    TWTextFileWriter twf(sFileName);
    for (auto nv : lNV)
        twf.ts << "-H " << "\"" << nv.sName << ": " << nv.sValue << "\"" << "\n";
    twf.close();
}

//----------------------------------------------------------------------------
// buildBasicAuthority
//
// 2022-10-21 First version
//----------------------------------------------------------------------------
NameValueStruct TWJest::buildBasicAuthority(QString sUserName, QString sPassword)
{
    return {"Authorization","Basic " + QString("%1:%2").arg(sUserName,sPassword).toUtf8().toBase64()};
}

//----------------------------------------------------------------------------
// request
//
// 2022-10-20 First version
//----------------------------------------------------------------------------
QByteArray TWJest::request(TWJestVerb jv, QString sURL, NameValueList lNV, QByteArray baData /* = QByteArray() */)
{
// use our helper which will do a blocking network call
    return pHelper->requestVerb(jv,sURL,lNV,baData);
}

//----------------------------------------------------------------------------------------
// Personno. lookup
//
// 2022-07-29 First version
// 2022-10-11 Support addresses outside of Sweden
// 2023-09-06 Support "reservnummer" and redacted names/addresses
//----------------------------------------------------------------------------------------
PKNODPLUSStruct TWJest::lookupPersonno(QString sHost, int nPort, QString sGateway, QString sPayload)
{
// prepare a default struct to return
    PKNODPLUSStruct pk;
    pk.sPersonNo          = "";
    pk.bIsReservNo        = false;
    pk.bAddressMissing    = false;
    pk.bExtraAddressInUse = false;
    pk.bOutsideSweden     = false;

// try to connect (use the blocking version, simpler)
    QTcpSocket socket;
    socket.connectToHost(sHost,static_cast<quint16>(nPort));
    if (!socket.waitForConnected(nnPKNODTimeout))
        return pk;  // so solly

// wait for the prompt and just toss it (don't bother to check it)
    if (!socket.waitForReadyRead(nnPKNODTimeout))
        return pk;  // so solly
    socket.readAll();

// xmit the gateway in utf8 followed by the payload in base64
    socket.write(sGateway.toUtf8() + sPayload.toUtf8().toBase64());
    socket.waitForBytesWritten();

// hope for the best
    if (!socket.waitForReadyRead(nnPKNODTimeout))
        return pk;  // so solly

// yes got something it should be in utf-8 encoded in base64
    auto ba64 = socket.readAll();
    socket.close();

// looks kosher?
    if (!ba64.startsWith(qUtf8Printable(ssValidLeadin)))
        return pk;  // no sorry

// yes parse away
    QString sAnswer = QByteArray::fromBase64(ba64);

// use this fine lambda to extract the different fields ------------
    auto getItem = [sAnswer] (int nOffset, int nLength)
    {
    // (the offsets and lengths are from the PKNODPLUS documentation)
    // skip over the 9 lead-in bytes ("0\n0\n1327\n") subtract 1 to change offsets to 0-based
        static const int nnSkipOver = 9 - 1;

    // get the string (toss any leading and trailing spaces)
        return sAnswer.mid(nnSkipOver + nOffset, nLength).trimmed();
    };

/* 2023-06-08: never mind checking the status code
// get the status code ("0000" means ok, "0002" means id is protected)

    QString sReturkod = getItem(5,4);
    bool bProtectedID = ("0002" == sReturkod);
*/

// get the personno. actually found (usually the same but can differ) and check if it is a reservno
    pk.sPersonNo   = getItem(29,12);
    pk.bIsReservNo = TWUtils::isReservNo(pk.sPersonNo);

// the name arrives in three parts: first name(s), middle name(s) and last name
// the first name(s) need some extra munging (parse slashes if they're present)
    QString sRawFirstName = getItem(705,80);

    QStringList sl   = sRawFirstName.split("/");  // slashes are delimiters for preferred first name(s)
    auto nSplitCount = sl.count();

    QString sCookedFirstName;
    if ((nSplitCount < 3) || (0 == (nSplitCount % 2)))
    // we're looking for pairs of slashes, require an odd split() count and at least 2 (or more) slashes
        sCookedFirstName = sRawFirstName.replace("/","");  // no good (too few or wrong number of them) just toss 'em
    else
        for (int i = 0; (i < nSplitCount); ++i)
            if (i % 2)                  // i = 1,3,5.. we want the lexical level fpr what is *inside* the slashes
                if (!sl[i].isEmpty())   // save only if there is something inside the slashes (not "//")
                    sCookedFirstName += " " + sl[i];

    sCookedFirstName    = sCookedFirstName.trimmed();  // toss possible spaces at the beginning and end
    QString sMiddleName = getItem(785,40);
    QString sLastName   = getItem(825,60);

// got all the parts of the name, smash 'em together
    QString sName = sCookedFirstName;
    if (!sMiddleName.isEmpty())
        sName += " " + sMiddleName;
    if (!sLastName.isEmpty())
        sName += " " + sLastName;

    pk.sName = TWUtils::pascalCaseIfAllUpper(sName); // as a final touch, PascalCase the name (if it's ALL CAPITAL)

// is the name redacted or missing?
    QString s = TWUtils::tossAllWhiteSpace(pk.sName);
    if ((s.length() < 4) && (s.contains("*")))
    // yes this is a John Doe (or the name is redacted), replace with "Skyddad Identitet"
        pk.sName = ssNameRedacted;

// we're done with the name, fill in the rest of the struct
    QString sCareOf   = getItem(885,35);
    QString sAddress1 = getItem(920,35);
    QString sAddress2 = getItem(955,35);
    QString sZipCode  = getItem(990, 5);
    QString sCity     = getItem(995,27);
    QString sCountry  = ssDefaultCountry;

    pk.bExtraAddressInUse = (!getItem(1132,27).isEmpty()); // if extra/specific City is nonblank, use that address
    if (pk.bExtraAddressInUse)
    {
        sCareOf   = getItem(1022,35);
        sAddress1 = getItem(1057,35);
        sAddress2 = getItem(1092,35);
        sZipCode  = getItem(1127, 5);
        sCity     = getItem(1132,27);
    }

// address outside of Sweden?
    pk.bOutsideSweden = (!getItem(1159,140).isEmpty()); // address outside of Sweden
    if (sCity.isEmpty())          // got no address yet?
        if (pk.bOutsideSweden)    // if we have a foreign adress, use that instead
        {
        // skip over the zipcode field (the foreign zipcode is likely in the 2nd or 3rd address field)
            sAddress1 = getItem(1159,35);
            sAddress2 = getItem(1194,35);
            sCity     = getItem(1229,35);
            sCountry  = getItem(1264,35);

        // city empty? if so move address2 into city
            if (sCity.isEmpty())
            {
                sCity     = sAddress2; // don't bother to check if Address2 also is empty
                sAddress2 = "";
            }
        }

// combine Address1 and Address2 into a single Address (the trimmed() is there in case sAddress1 is empty)
    QString sAddress = TWUtils::pascalCaseIfAllUpper(sAddress1 + " " + sAddress2).trimmed();

    if (sAddress.contains("Iii"))          // fix for "...Gustav Iii:s Väg..." and similar (PascalCasing fail)
        sAddress.replace("Iii","III");
    if (sAddress.contains("All@"))         // fix for "All@" detritus from vintage EBCDIC codepages
        sAddress.replace("All@","Allé");

// unknown or missing address? yes if sCity is "*" or "Okänd" or if it's empty
    pk.bAddressMissing = ("*" == sCity) || ("OKÄND" == sCity.toUpper()) || (sCity.isEmpty());
    if (pk.bAddressMissing)
    // reset the city to a more legible "Postadress saknas"
        sCity = "Postadress saknas";

// almost there, stuff the rest of the struct
    pk.sCareOf  = TWUtils::pascalCaseIfAllUpper(sCareOf);
    pk.sAddress = sAddress; // PascalCase already done for this chap
    pk.sZipCode = sZipCode; // PascalCase not needed (presumably just digits here)
    pk.sCity    = TWUtils::pascalCaseIfAllUpper(sCity);
    pk.sCountry = TWUtils::pascalCaseIfAllUpper(sCountry);

// and we're done
    return pk;
}
