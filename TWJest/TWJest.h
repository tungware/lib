/*--------------------------------------------------------------------------*/
/* TWJest.h (JSON and REST support library)                                 */
/*                                                                          */
/* 2022-07-22 First version                                             HS  */
/*--------------------------------------------------------------------------*/
#pragma once

// do the usual library #defines
#if defined(TWJest_LIBRARY) || defined(TWSTATICBUILD)
#define TWJestSHARED_EXPORT Q_DECL_EXPORT
#else
#define TWJestSHARED_EXPORT Q_DECL_IMPORT
#endif

// network request header name/value struct and list
struct NameValueStruct
{
    QString sName;
    QString sValue;
};
using NameValueList = QList<NameValueStruct>;

// REST verb enums
enum class TWJestVerb { eGet, ePut, ePost, eDelete };

// PKNOD stuff
static const QString ssNameRedacted   = "Skyddad Identitet";
static const QString ssDefaultCountry = "Sverige";

// struct returned from the personno lookup
struct PKNODPLUSStruct
{
    QString sPersonNo;          // YYYYMMDDNNNN found (or empty if there was an error)
    bool    bIsReservNo;        // personno above is a "reservnummer" (Region Stockholm flavor)

    QString sName;              // first name (s), middle name(s) and last name(s) combined
    QString sCareOf;
    QString sAddress;
    QString sZipCode;           // 5 digits no spaces (for Swedish addresses)
    QString sCity;
    QString sCountry;           // default: "Sverige"

    bool    bExtraAddressInUse; // true: address is the extra/alternative one
    bool    bOutsideSweden;     // true: address is outside of Sweden
    bool    bAddressMissing;    // true: no address found or sCity is just a '*' or "Okänd"
};

// ==========================================================================
class TWJestHelper;             // yes this one is a real helper

using fJestErrorCallback_t = std::function<void(QString sError)>;

// main chap
class TWJestSHARED_EXPORT TWJest
{
public:
    TWJest();
    ~TWJest();

// use this to set an error callback
    void setErrorCallback(fJestErrorCallback_t fJestError);

// set an optional delay (to circumvent the ratelimiting of calls at Fortnox)
    void setDelay(int nMilliSeconds);

// parse a curl header file into a name/value list (no error checking)
    static NameValueList parseHeaderFile(QString sFileName);

// write a name/value list to a curl header file (no error checking)
    static void writeHeaderFile(QString sFileName,NameValueList lNV);

// compose a basic authority header name/value struct
    static NameValueStruct buildBasicAuthority(QString sUserName, QString sPassword);

// a (blocking) rest request (get/put/post/delete) use baData to store the bytes to PUT or POST
// for a simple get from a URL an empty NameValueList will do nicely (i.e. no basic authority etc. needed)
    QByteArray request(TWJestVerb jv, QString sURL, NameValueList lNV, QByteArray baData = QByteArray());

// personno. --> name and address lookup
    static PKNODPLUSStruct lookupPersonno(QString sHost, int nPort, QString sGateway, QString sPayload);

private:
    TWJestHelper* pHelper;
};
