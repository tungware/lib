#--------------------------------------------------------------------------#
# TWJest.pro (JSON and Rest support library)                               #
#                                                                          #
# 2022-07-22 First version                                             HS  #
#--------------------------------------------------------------------------#

QT      += core gui widgets network

DEFINES += TWJest_LIBRARY

HEADERS += TWJest_p.h TWJest.h
SOURCES += TWJest_p.cpp TWJest.cpp

TARGET   = TWJest
TEMPLATE = lib

# include our common QtProjects helper
include(../../include/QtProjects.pri)

# TWUtils please
DEFINES += $$TWDefineInclude(TWUtils)
LIBS    += $$TWLibLine(TWUtils)
