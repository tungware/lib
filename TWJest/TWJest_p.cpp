/*--------------------------------------------------------------------------*/
/* TWJest_p.cpp                                                             */
/*                                                                          */
/* 2022-07-22 First version                                             HS  */
/*--------------------------------------------------------------------------*/
#include "TWJest_p.h"

TWJestHelper::TWJestHelper()
{
// reset our delay chaps
    nPrevFinishingTime = 0; // start by pretending prev. call was at midnight
    nDelayMilliSeconds = 0; // default to 0: no delay/rate limit in effect

// reset the error callback and the network reply
    fJestErrorCallback = nullptr;
    pReply             = nullptr;
}

//----------------------------------------------------------------------------
// sslErrors
//
// 2022-07-25 First version
//----------------------------------------------------------------------------
void TWJestHelper::sslErrors(const QList<QSslError> &errors)
{
    Q_UNUSED(errors);

// just go south currently
    fubar("didn't expect any sslErrors");
}

//----------------------------------------------------------------------------
// errorOccurred
//
// 2022-07-25 First version
//----------------------------------------------------------------------------
void TWJestHelper::errorOccurred(QNetworkReply::NetworkError code)
{
    Q_UNUSED(code)

// retrieve the error as a string (so we can ignore the code)
    sError = pReply->errorString();

// is there an error callback set?
    if (nullptr == fJestErrorCallback)
    // no so fubar inside here instead
        fubar(QString("error(s) occurred '%1' and no error callback is set").arg(sError));

// yes we have it, call it
    fJestErrorCallback(sError);

// tell the reply object to delete itself (when it's convenient)
    pReply->deleteLater();

// that's all
    eventLoop.exit();
}

//----------------------------------------------------------------------------
// requestVerb (where we support GET, PUT, POST and DELETE)
//
// 2022-10-20 First version
//----------------------------------------------------------------------------
QByteArray TWJestHelper::requestVerb(TWJestVerb jv, QString sURL, NameValueList lNV, QByteArray baData)
{
// prep the request chap
    QNetworkRequest request;
    request.setUrl(sURL);

// any name/value pairs? stuff 'em in the request
    for (auto nv : lNV)
        request.setRawHeader(nv.sName.toUtf8(),nv.sValue.toUtf8());

// reset any previous errors and answers
    sError.clear();
    baReceived.clear();

// check baData contents (GET and DELETE shouldn't have any, but PUT and POST should)
    switch (jv)
    {
    case TWJestVerb::eGet    :
        if (!baData.isEmpty())
            fubar("no data expected for the GET verb");
        break;

    case TWJestVerb::ePut    :
        if (baData.isEmpty())
            fubar("missing data for the PUT verb");
        break;

    case TWJestVerb::ePost   :
        if (baData.isEmpty())
            fubar("missing data for the POST verb");
        break;

    case TWJestVerb::eDelete :
        if (!baData.isEmpty())
            fubar("no data expected for the DELETE verb");
        break;
    }

// have a chat with the internetz, process differently depending on the verb
    pReply = nullptr;   // for bad verb detection
    switch (jv)
    {
    case TWJestVerb::eGet :
        pReply = manager.get(request);
        break;

    case TWJestVerb::ePut :
        pReply = manager.put(request,baData);
        break;

    case TWJestVerb::ePost :
        pReply = manager.post(request,baData);
        break;

    case TWJestVerb::eDelete :
        pReply = manager.sendCustomRequest(request,"DELETE");
        break;
    }
    if (nullptr == pReply)
        fubar("pReply is null, most likely due to bad verb or bad network");

// wire up some nice signals for the reply
    connect(pReply,&QNetworkReply::sslErrors    ,this,&TWJestHelper::sslErrors    );
    connect(pReply,&QNetworkReply::errorOccurred,this,&TWJestHelper::errorOccurred);
    connect(pReply,&QNetworkReply::finished     ,this,&TWJestHelper::finished     );

// start our own eventloop and block on it
    if (eventLoop.isRunning())
        fubar("eventLoop is already running");

// remember the current loop level
    int nLoopLevel = QThread::currentThread()->loopLevel();

// slammer time
    eventLoop.exec();

// we're back, verify we're back at the same loop level
    if (QThread::currentThread()->loopLevel() != nLoopLevel)
        guruMeditation("loop levels before and after eventLoop.exec() mismatch");

// return with the array
    return baReceived;
}

//----------------------------------------------------------------------------
// finished
//
// 2022-07-24 First version
// 2023-03-13 Delay/rate limit support
//----------------------------------------------------------------------------
void TWJestHelper::finished()
{
// any error? if so skip processing (eventloop and reply taken care of above)
    if (!sError.isEmpty())
        return;

// save the answer we got
    baReceived = pReply->readAll();

// tell the reply object to delete itself (when it's convenient)
    pReply->deleteLater();

// is there a delay time/rate limit set?
    if (0 == nDelayMilliSeconds)
        return eventLoop.exit();   // no you're home free

// rate limit in effect, get the time for the previous call
    int nPrev          = nPrevFinishingTime;
    int nNow           = QTime::currentTime().msecsSinceStartOfDay();
    nPrevFinishingTime = nNow;  // set current time for the benefit of the next call

// calc. the delay needed (use modulo # of milliseconds per day in case of wraparound/midnight)
    int nMilliSecondsPerDay = 24 * 60 * 60 * 1000;
    int nElapsedSincePrev   = (nMilliSecondsPerDay + nNow - nPrev) % nMilliSecondsPerDay;
    int nDelayNeeded        = nDelayMilliSeconds - nElapsedSincePrev;
    if (nDelayNeeded < 0)
        return eventLoop.exit();   // no need for delay at this time, we're done

 // sanity check the calculated delay
    if (nDelayNeeded > nDelayMilliSeconds)
        guruMeditation("nDelayNeeded > nDelayMilliSeconds");

    ++nDelayNeeded; // add one millisecond so we're always > 0
    QTimer::singleShot(nDelayNeeded,this,[this] { eventLoop.exit(); });
}
