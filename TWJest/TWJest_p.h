/*--------------------------------------------------------------------------*/
/* TWJest_p.h (JSON and Rest support library internal stuff)                */
/*                                                                          */
/* 2022-07-22 First version                                             HS  */
/*--------------------------------------------------------------------------*/
#pragma once

// pull in some Qt thread and network stuff
#include "qthread.h" // IWYU pragma: keep
#include "qtimer.h"  // IWYU pragma: keep
#include "qnetworkaccessmanager.h"
#include <QNetworkReply>

// and our main .h
#include "TWJest.h"

// pull in TWUtils (useful for fubars etc.)
#include INCLUDETWUTILS // IWYU pragma: keep

// personno. lookup static stuff
static const int     nnPKNODTimeout = 6789;     // timing in milliseconds
static const QString ssValidLeadin  = "MAow";   // "0\r\0\r" in base64

// the real work is done here ------------------------------------------------
class TWJestHelper : public QObject
{
// Qt boilerplate (so that signals and slots work)
    Q_OBJECT

public:
    TWJestHelper();

// our big network wheel and eventloop
    QNetworkAccessManager manager;
    QEventLoop eventLoop;

// set the error callback
    void setJestErrorCallback(fJestErrorCallback_t fJestError) { fJestErrorCallback = fJestError; }

// set optional delay
    void setDelay(int nMilliSeconds) { nDelayMilliSeconds = nMilliSeconds; }

// get/put/post/delete request (leave baData empty for get or delete requests)
    QByteArray requestVerb(TWJestVerb jv, QString sURL, NameValueList lNV, QByteArray baData);

public slots:
// QNetworkReply slots
    void sslErrors(const QList<QSslError>& errors);
    void errorOccurred(QNetworkReply::NetworkError code);
    void finished();

private:
// store the error callback here
    fJestErrorCallback_t fJestErrorCallback;

// time/delay stuff
    int nPrevFinishingTime;
    int nDelayMilliSeconds;

// the current network reply
    QNetworkReply* pReply;

// possible error and the bytarray received
    QString    sError;
    QByteArray baReceived;
};
