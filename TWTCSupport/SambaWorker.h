/*------------------------------------------------------------------------------*/
/* SambaWorker.h                                                                */
/*                                                                              */
/* 2020-08-16 First version                                                 HS  */
/* 2024-09-07 Say hello to libsmb2 and rename this file to SambaWorker.h    HS  */
/*------------------------------------------------------------------------------*/

// need some bits of Qt like QObject, QDate/QTime and QMap
#include "qobject.h"
#include "qdatetime.h"
#include "qmap.h"

// we're using the excellent libsmb2 library for samba stuff
// https://github.com/sahlberg/libsmb2
#include INCLUDESMB2SMB2
#include INCLUDESMB2LIBSMB2

//--------------------------------------------------------------------------------------------------
// filename pattern for the Samba files the crawler likes to trawl
static const QString ssSambaFilenamePattern = QStringLiteral("StgA%1%2.txt");

// expected length and column positions for Samba StgA*.txt files (0-based)
static const int nnSambaValidLineLength   = 127;

static const int nnSambaDateTimePosition  =   0;   // YYYY-MM-DD hh:mm:ss
static const int nnSambaDateTimeLength    =  19;

static const int nnSambaReceiptNoPosition =  20;   // nnn-nn-nnnn-c (c is the Luhn check digit)
static const int nnSambaReceiptNoLength   =  13;

static const int nnSambaPersonNoPosition  =  67;   // CC YYYYMMDD-nnnc (c is the Luhn check digit)
static const int nnSambaPersonNoLength    =  14;   // due to the format being slightly fishy


//--------------------------------------------------------------------------------------------------
// worker assistant for the Samba crawler
class SambaWorker : public QObject
{
// Qt boilerplate (so that signal and slots work)
    Q_OBJECT

public:
    void crawl();                   // this is where the work is really done

signals:
    void error(QString sError);
    void finished(QMap<QString,QString> mRP,QDate dFirst,QDate dLast);

public:
// expecting these chaps to be set by SambaCrawler (and expecting portNo to be appended to servername)
    bool       bConnectToServer;
    QByteArray baServerName,baShareName,baUserName,baPassword; // connecting to a Samba server? use these
    QString    sLocalDirectory;                                // else use this directory

    int   nCompanyCode,nDaysBack; // needed for the StgA* files lookup
    QDate dFirst,dLast;           // first (oldest) and last (most recent) dates for files found
                                  // will be invalid if no receiptnos/personno. found
private:
    QMap<QString,QString> mRP;    // will contain pairs of receiptno and personnos

// libsmb2 stuff
    struct smb2_context* smb2;
};
