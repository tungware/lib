/*------------------------------------------------------------------------------*/
/* TWTCSupport.cpp                                                              */
/*                                                                              */
/* 2019-10-20 First version                                                 HS  */
/* 2020-08-16 Say hello to our private .h file (TWTCSupport_p.h)            HS  */
/* 2021-10-06 Introducing the asynchronous Samba crawler                    HS  */
/* 2024-09-08 Switch to libsmb2 https://github.com/sahlberg/libsmb2         HS  */
/*------------------------------------------------------------------------------*/

#include "qthread.h"
#include "SambaWorker.h"
#include "qdir.h"
#if defined(Q_OS_WIN)
#include "qprocess.h"
#endif

#if defined(Q_OS_WIN)
#include "qfile.h"      // check if a file exists
#include <qsettings.h>  // needed in getTCPath()
#endif

#include "TWTCSupport.h"

#include <stdint.h>
#include <fcntl.h>
#if defined(Q_OS_WIN)
#include <winsock2.h>
#endif

// list of test patients (that can be excluded from normal processing)
Q_GLOBAL_STATIC_WITH_ARGS(QStringList,plTestPersonNos,(
{
    "180303030316",
    "181111111116",
    "181010101010",
    "180404040404",
    "180202020202",
    "180606060606",
    "181212121212",
    "191212121212",
    "195503262684",
    "198101079294",
    "198808112380",
    "992012608713",
    "992014327189",
// extra chaps we can add later: 19 500505-0520, 19 500505-0546 and 19 060606-0606
}))

// same list but as XTEA encrypted PatientIDs
Q_GLOBAL_STATIC_WITH_ARGS(QList<PID_t>,plTestPIDs,(
{
    Q_INT64_C(-1659729165165880497),
    Q_INT64_C( 6840461669602499519),
    Q_INT64_C(-4211760926460082735),
    Q_INT64_C(-5701500000507269660),
    Q_INT64_C( 1774449384011043526),
    Q_INT64_C(-3733116086642438660),
    Q_INT64_C(-4151740958657670628),
    Q_INT64_C(-2658222856580924101),
    Q_INT64_C( 1135878497613038810),
    Q_INT64_C( 4425289109987094651),
    Q_INT64_C( 7180594354796846142),
    Q_INT64_C( 5574168483615937809),
    Q_INT64_C(-1378913119521781158),
}))


//----------------------------------------------------------------------------
// setPrefix (sets your preferred table name prefix for all bobby tables)
//
// 2021-12-17 First version
//----------------------------------------------------------------------------
QString TCBobbyTables::sPrefix = "";    // needs initializing

void TCBobbyTables::setPrefix(QString s)
{
    sPrefix = s;
}

//----------------------------------------------------------------------------
// TC custom cooker (expects either a table or a view name prefix)
//
// 2021-12-17 First version
// 2022-08-10 Trim cosmetic white space
//----------------------------------------------------------------------------
QString TCBobbyTables::cook()
{
    QStringList slPrefixed;
    for (auto s : slRaw)
        slPrefixed += sPrefix + s.trimmed();

    return slPrefixed.join(",");
}

//----------------------------------------------------------------------------
// getAllTestPatients
//
// 2022-12-09 First version
//----------------------------------------------------------------------------
QStringList TWTCSupport::getAllTestPatients()
{
// stuff a new list without "1x1212121212" (they're not as popular nowadays)
    QStringList sl;
    for (auto s : *plTestPersonNos)
        if (!s.contains("1212121212"))
            sl += s;

    return sl;
}

//----------------------------------------------------------------------------
// isTestPatient (2 flavors)
//
// 2021-04-23 First version
//----------------------------------------------------------------------------
bool TWTCSupport::isTestPatient(QString sPersonNo)
{
    return plTestPersonNos->contains(sPersonNo);
}

bool TWTCSupport::isTestPatient(PID_t PID)
{
    return plTestPIDs->contains(PID);
}

//----------------------------------------------------------------------------
// getDefaultFee and Absentee fees applicable on the visit date
//
// 2024-01-04 First version
//----------------------------------------------------------------------------
int TWTCSupport::getDefaultFee(QDate dVisit)
{
    if ((!dVisit.isValid()) || (dVisit < QDate(2021,04,01))) // before 2021-04-01 the fee was different
        fubar("getDefaultFee(): invalid date or before 2021-04-01");

// 2024-01-01 fee changed from 250 kr to 275 kr
    if (dVisit.year() < 2024)
        return 250;

    return 275;
}

int TWTCSupport::getAbsenteeFee(QDate dVisit)
{
    if ((!dVisit.isValid()) || (dVisit < QDate(2020,10,01))) // before 2020-10-01 the fee was different
        fubar("getAbsenteeFee(): invalid date or before 2020-10-01");

    return 400;
}

//----------------------------------------------------------------------------
// getTitleForCareProviderID
//
// 2024-09-13 First version
// 2024-09-23 Also support IDs that start with "A."
//----------------------------------------------------------------------------
QString TWTCSupport::getTitleForCareProviderID(QString sCareProviderID)
{
// doctors get special treatment
    if (isDoctor(sCareProviderID))
        return "Dr.";

// treat the numeric ones in a switch
    switch (sCareProviderID.toInt())
    {
    case 70: return "sjuksköterska";
    case 71: return "audionom";
    case 72: return "logoped";
    case 73: return "synpedagog";
    case 74: return "psykolog";
    case 75: return "kurator";
    case 76: return "barnmorska";
    case 77: return "fysioterapeut/sjukgymnast";
    case 78: return "arbetsterapeut";
    case 79: return "dietist";
    case 80: return "barnsköterska";
    case 81: return "ortoptist";
    case 82: return "synpedagog";
    case 83: return "fotvårdsspecialist";
    case 84: return "utf. av intyg";
    case 85: return "synkonsulent";
    case 86: return "teambesök";
    case 87: return "hjälpmedelskonsulent";
    case 88: return "labbassistent";
    case 89: return "distriktssköterska";
    case 90: return "familjebehandlare";
    case 91: return "undersköterska";
    case 92: return "bildterapeut";
    case 93: return "diabetessköterska";
    case 94: return "med.service";
    case 95: return "dagvård";
    case 96: return "psykoterapeut";
    case 97: return "tolk";
    case 98: return "akupunktör";
    case 99: return "talpedagog";
    }

    if ("A1" == sCareProviderID.toUpper())
        return "specialpedagog";
    if ("A2" == sCareProviderID.toUpper())
        return "naprapat";
    if ("A3" == sCareProviderID.toUpper())
        return "kiropraktor";
    if ("A4" == sCareProviderID.toUpper())
        return "friskvårdare";
    if ("A5" == sCareProviderID.toUpper())
        return "case manager";
    if ("A6" == sCareProviderID.toUpper())
        return "kontaktsjuksköterska";
    if ("A8" == sCareProviderID.toUpper())
        return "vårdbiträde";
    if ("AO" == sCareProviderID.toUpper())
        return "assistent omvårdnad";

// still no hit?
    fubar(QString("Could not find a title for provider id '%1'").arg(sCareProviderID));
}

//----------------------------------------------------------------------------
// isDoctor
//
// 2024-09-08 First version
//----------------------------------------------------------------------------
bool TWTCSupport::isDoctor(QString sCareProviderID)
{
    int nID = sCareProviderID.toInt();  // IDs starting with "A.."? they're never doctors
    return ((nID > 0) && (nID < 70));
}

//----------------------------------------------------------------------------
// canonicalizeKombika
//
// 2021-10-07 First version
// 2022-09-05 Make sure there are only digits and the letters A-Z or a-z
//----------------------------------------------------------------------------
QString TWTCSupport::canonicalizeKombika(QString sKombika)
{
// make 'em all uppercase
    QString sUpperCase = sKombika.toUpper();

// change Swedish umlauts to A or O
    sUpperCase.replace("Å","A");
    sUpperCase.replace("Ä","A");
    sUpperCase.replace("Ö","O");

// and toss everything except digits and letters A-Z
    QString sCanonicalized;
    for (auto c : sUpperCase)
    {
        if (c.isDigit())
            sCanonicalized += c;

        if ((c >= 'A') && (c <= 'Z'))
            sCanonicalized += c;
    }

// nothing left? forget about it
    if (sCanonicalized.isEmpty())
        return sCanonicalized;

// if kombika is shorter than 11 chars, pad with leading 0s
    sCanonicalized = ("0000000000" + sCanonicalized).right(11);

// check for 4 and 5 letter combos (4 or 5 letters and zeros in the other positions)
// they might need some adjustments (e.g "0TAND000000" --> "TAND0000000")
    int nNoOfZeros = 0;
    QString sLetters;
    for (auto c : sCanonicalized)
        if ('0' == c)
            ++nNoOfZeros;
        else
            if (c.isLetter())
                sLetters += c;

// do we have one of those combos? left adjust please
    if ((6 == nNoOfZeros) || (7 == nNoOfZeros))
        if (11 == (nNoOfZeros + sLetters.length()))
            sCanonicalized = sLetters + QString(nNoOfZeros,'0');

// by now we should have 11 letters and/or digits
    if (sCanonicalized.length() != 11)
        guruMeditation("canonicalizeKombika: expected length 11");

// that's all the canonicalization we need right now
    return sCanonicalized;
}

//----------------------------------------------------------------------------
// receiptNo2ARVReceiptNo
// changes a TC receipt no. into a 8 digit number suitable for ARV
// returns 0 for errors
//
// 2022-08-25 First version
//----------------------------------------------------------------------------
int TWTCSupport::receiptNo2ARVReceiptNo(QString sTCReceiptNo, QString sCompanionATGCode /*= "" */)
{
// check that the receiptno is kosher
    if (!TWUtils::isLuhnCheckDigitOk(sTCReceiptNo))
        return 0;

    QStringList sl = sTCReceiptNo.split("-");
    if (sl.count() != 4) // expecting "999-99-9999-9" i.e. 4 strings after the split above
        return 0;

// from TC receipt no: 999-9x-xxxx-9 extract the x digits and construct a 5 digit number
    int nARVReceiptNo = 10000 * (sl[1].right(1).toInt()) + sl[2].toInt();

    if (nARVReceiptNo < 1000)   // should only be below 10000 if rightmost digit of counterno.is 0, e.g "999-10-xxxx-x"
        return 0;

    if (nARVReceiptNo > 99999)
        return 0;

// have a companion code?
    if (sCompanionATGCode.isEmpty())
    // nothing there so we're done
        return nARVReceiptNo;

// use the 3 digits of the companion code (e.g. "S360" becomes 360xxxxx)
// as the top 3 digits of the ARV receipt no (will fit into a 32-bits integer)
    QString sARVReceiptNo = ("0000" + QString::number(nARVReceiptNo)).right(8);   // support both 4 and 5 digits
    QString sNNN          = TWUtils::tossAllButDigits(sCompanionATGCode);
    if (sNNN.length() != 3)
    // support only [S100 .. S999]
        return 0;

// ok we're done
    return (sNNN + sARVReceiptNo.right(5)).toInt();
}

//-----------------------------------------------------------------------------
// digestVisit (analyzes the visit for you)
//
// 2022-07-15 First version
// 2023-01-14 Introduce lenient processing of absentees and OOBs
// 2023-01-16 Add age as an input param and use it for further checks
// 2024-01-03 Require visit date and change ARV insurance rate code to 80
// 2024-01-04 Say hello to the distance contact visit flavors U, W and Y
//-----------------------------------------------------------------------------
TCVisitStruct TWTCSupport::digestVisit(eTCCustomerTypes eCustomerType, TCVisitStruct v)
{
// we'll be returning the same struct (v) as we receive; set pessimistic outgoing defaults
    v.bOk             = false;
    v.evf             = eUnknownVisitFlavor;
    v.nExpectedFee    = -1;     // (-1 = unknown)
    v.bNormalFeePaid  = false;
    v.sError          = QStringLiteral("Felaktig taxa eller besökstyp");

// default the ARV settings
    v.bARVMaterial    = true;
    v.sRateCodeForARV = v.sRateCode;    // some ratecodes (09 and 12) are changed for ARV

// if essential params are out of whack, return an unknown visit flavor
    if ((1 != v.sVisitType.length()) || (2 != v.sRateCode.length()) || (v.nFeePaid < 0))
        return v;

// get the default and absentee fees
    int nDefaultFee  = getDefaultFee(v.dVisit);
    int nAbsenteeFee = getAbsenteeFee(v.dVisit);

// start by checking the visit type
    eTCVisitFlavors e = eUnknownVisitFlavor;
    v.sError = QString("Okänd eller felaktig besökstyp '%1'").arg(v.sVisitType);   // pessimistic default
    switch (v.sVisitType[0].toLatin1())
    {
    case '0' : e = eNormalVisitFlavor;          break;  // new visit
    case '1' : e = eNormalVisitFlavor;          break;  // revisit

    case '2' : e = eHomeVisitFlavor;            break;
    case '3' :  // '3' and '7': out of band visits (vaccinations, certificates, medical products etc.)
    case '7' : e = eOOBVisitFlavor;             break;

    case '4' : e = eDistanceContactVisitFlavor; break;
    case '5' : e = eAbsentVisitFlavor;          break;
    case '6' : e = eDistanceContactVisitFlavor; break;
    case '8' : e = eGroupVisitFlavor;           break;
    case '9' : e = eDistanceContactVisitFlavor; break;  // "Distanskontakt, enskilt"
    case 'A' : e = eTeamVisitFlavor;            break;
    case 'B' : e = eGroupTeamVisitFlavor;       break;
    case 'H' : e = eTeamHomeVisitFlavor;        break;
    case 'L' : e = eExternalVisitFlavor;        break;
    case 'P' : e = eTeamExternalVisitFlavor;    break;

// view these new flavors as just another distance contact flavor (no distinction)
    case 'U' : e = eDistanceContactVisitFlavor; break;  // "Distanskontakt, team"
    case 'W' : e = eDistanceContactVisitFlavor; break;  // "Distanskontakt, grupp"
    case 'Y' : e = eDistanceContactVisitFlavor; break;  // "Distanskontakt, gruppteam"
    }

// got a flavor?
    if (e == eUnknownVisitFlavor)
        return v;   // no we're done here

// --- got an age specified? if so do some more rate code consistency checks -------
    enum { eAgeUnknown, eAgeBelow18, eAge18To84, eAge85OrOlder } eAge = eAgeUnknown;
    if (v.nAgeInYears >= 0)
    {
        eAge = eAge18To84;  // default
        if (v.nAgeInYears < 18)
            eAge = eAgeBelow18;
        if (v.nAgeInYears > 84)
            eAge = eAge85OrOlder;
    }

// --- check out the ratecode ------------------------------------------------------
    v.sError = QString("Okänd eller felaktig taxa '%1'").arg(v.sRateCode);   // in case of failure

    int nRateCode = v.sRateCode.toInt(); // convert to int (simplifies the switching statement)
    switch (nRateCode)
    {
    case  1 :
        if (eLOLCustomerType == eCustomerType)
            return v;   // 01: no good ratecode for LOLs (allowed for LOF and GVR)

        v.nExpectedFee = nDefaultFee;
        v.bOk          = true;
        break;

    case  2 :
        if (eLOFCustomerType == eCustomerType)
            return v;   // 02: no good ratecode for LOFs (allowed for LOL and GVR)

        v.nExpectedFee = nDefaultFee;
        v.bOk          = true;
        break;

    case  3 :
        return v;       // 03: bad or unsupported rate code

    case  4 :           // for GVR: for EU/EES patients
        if (eGVRCustomerType != eCustomerType)
            return v;   // 4: not good for LOL/LOFs

        v.nExpectedFee = 50;  // not 100% sure (educated guess)
        v.bOk          = true;
        break;

    case  5 :
        return v;       // 05: bad or unsupported rate code

    case  6 :           // 06: no fee for all chaps (GVR and LOLs/LOFs)
        v.nExpectedFee = 0;  // (should check for GVR and > 84 years here but it might cause false positives)
        v.bOk          = true;
        break;

    case  7 :           // bad or unsupported rate code
        return v;

    case  8 :           // ratecode for free visits (GVR only)
        if (eGVRCustomerType != eCustomerType)
            return v;   // 08: LOL/LOFs no happy

        v.nExpectedFee = 0;
        v.bOk          = true;
        break;

    case  9 :           // ARV/LOL smittskydd (internal TC code before changing it to "07") or a GVR OOB
        if (eLOLCustomerType == eCustomerType)
        {
        // ARV/LOL smittskydd
            v.nExpectedFee    = 0;
            v.bOk             = true;
            v.sRateCodeForARV = "07";   // will the real ratecode "07" please stand up
        }

        if (eLOFCustomerType == eCustomerType)
            return v;        // 09: not valid for LOFs

        if (eGVRCustomerType == eCustomerType)
        {
        // GVR OOB ratecode: can be vaccinations, certs or product purchases
        // don't check the fee paid for this one
             if (!v.bLenient)
                if (eOOBVisitFlavor != e)
                // no leniency and not an OOB visit flavor: return with the generic error msg
                    return v;

        // if we're lenient: allow this rate code to subvert/change the visit flavor
            v.bOk            = true;
            v.bShownLeniency = (eOOBVisitFlavor != e);
            e                = eOOBVisitFlavor;    // in case we showed leniency
        }
        break;

    case 10 :           // ARV: zero fee, for GVR: reduced fee expected
        if (eGVRCustomerType != eCustomerType)
            v.nExpectedFee = 0; // or LOLs and LOFs: expect 0

        v.bOk = true;
        break;

    case 12 :           // for GVR: rate code for asylum seekers, treat it as a normal visit
        if (eGVRCustomerType != eCustomerType)
            return v;   // 12: no good for LOL/LOFs

        v.nExpectedFee = 50;    // not 100% sure (educated guess)
        v.bOk          = true;
        break;

    case 13 :           // for GVR OOB visit: buying/renting stuff (crutches medicine etc.)
        if (eGVRCustomerType != eCustomerType)
            return v;   // 13: no LOL/LOFs allowed

    // no leniency will be shown for this ratecode (has to match the OOB visit flavor)
        if (e != eOOBVisitFlavor)
            return v;

        v.bOk = true;
        break;

    case 14 :           // bad or unsupported (maybe later for ARV: special free visits)
        return v;

    case 15 :           // GVR OOBs (give it identical treatment as "09" for GVR)
        if (eGVRCustomerType != eCustomerType)
            return v;   // 15: only applicable to GVRs (no LOLs or LOFs please)

    // OOB ratecode: can be vaccinations, certificates or product purchases
        if (!v.bLenient)
            if (eOOBVisitFlavor != e)
            // no leniency and not an OOB visit flavor: return with (the generic) error msg
                return v;

    // if we're lenient: allow this rate code to subvert/change the visit flavor
        v.bOk            = true;
        v.bShownLeniency = (eOOBVisitFlavor != e);
        e                = eOOBVisitFlavor;    // in case we showed leniency
        break;

    case 16 :           // bad or unsupported rate code
        return v;

    case 17 :           // ARV zero fee ratecode for pat. 85 years or older
        if (eGVRCustomerType == eCustomerType)
        // 17 not good for GVR
            return v;

        if (eAgeUnknown != eAge)
            if (eAge85OrOlder != eAge)
            {
            // pat. has the wrong age for this ratecode (flag as an error regardless of leniency)
                v.sError = QString("Felaktig ålder (%1 år) för taxa 17").arg(v.nAgeInYears);
                return v;
            }

        v.nExpectedFee = 0;
        v.bOk          = true;
        break;

    case 18 :           // ARV ratecode for pat. below 18 years of age (fee = 0)
        if (eGVRCustomerType == eCustomerType)
        // 18 is not a valid choice for GVR, catch you later
            return v;

        if (eAgeUnknown != eAge)
            if (eAgeBelow18 != eAge)
            {
            // pat. has the wrong age for this ratecode (flag as an error regardless of leniency)
                v.sError = QString("Felaktig ålder (%1 år) för taxa 18 (taxa 12 i ARV-portalen)").arg(v.nAgeInYears);
                return v;
            }

        v.nExpectedFee    = 0;
        v.bOk             = true;
        v.sRateCodeForARV = "12";   // use the real ratecode "12" ("18" is an internal TC ratecode)
        break;

// --- private visits (both GVR and ARV)
    case 19 :
    // allow this ratecode to subvert/change all visit flavors except absentees (regardless of leniency setting)
        if (eAbsentVisitFlavor != e)   // unless this is an absentee overriding the visit flavor
            e = ePrivateVisitFlavor;

        v.bOk             = true;
        v.bARVMaterial    = false;
        v.sRateCodeForARV = "";        // no ARV for you (LOLs or LOFs)
        break;

// --- absentee (both GVR and ARV)
    case 20 :
    // this ratecode applies both to GVR and ARV
    // set these first, then check
        v.nExpectedFee    = nAbsenteeFee;
        v.bARVMaterial    = false;      // no ARV for you
        v.sRateCodeForARV = "";         // and make sure it's empty

        if (!v.bLenient)
            if (eAbsentVisitFlavor != e)
            {
            // no leniency and not an absentee visit type: return with a custom error message
                v.bOk    = false;
                v.sError = QString("Felaktig besökstyp (%1) för taxa 20").arg(v.sVisitType);
                return v;
            }

    // allow this rate code to subvert/change other visit flavors (if leniency is allowed)
        v.bShownLeniency = (eAbsentVisitFlavor != e);   // remember if flavor as subverted or not
        e                = eAbsentVisitFlavor;          // always set (needed if we subverted)

    // ok seems ok
        v.bOk = true;
        break;

// --- GVR zero fee for < 18 years old
// note: to support ARV reduced rate codes 21 -- 74 don't do a "return v" for codes > 20
    case 60 : // GVR zero fee ratecode for pat. below 18 years of age
        if (eGVRCustomerType == eCustomerType)
        {
            v.nExpectedFee = 0;

            if (eAgeUnknown != eAge)
                if (eAgeBelow18 != eAge)
                {
                // pat. has a bad age for this ratecode (flag as error regardless of leniency)
                    v.sError = QString("Felaktig ålder (%1 år) för taxa 60").arg(v.nAgeInYears);
                    return v;
                }
        }
        break;

// --- pat. without id/papers (GVR only)
    case 71 :
        if (eGVRCustomerType == eCustomerType)
            v.bOk = true;
        break;

// these below are higher the ARV reduced rate codes (so a "return v" is allowed)
// private insurance code (ARV only)
    case 80 :
        if (eGVRCustomerType == eCustomerType)
        // 80 no good for GVR
            return v;

    // 80 = insurance pat. for ARV
    // allow this ratecode to subvert/change all visit flavors except absentees (regardless of leniency setting)
        if (eAbsentVisitFlavor != e)   // unless this is an absentee overriding the visit flavor
            e = eInsuredPatientVisitFlavor;

        v.bOk             = true;
        v.bARVMaterial    = false;
        v.sRateCodeForARV = "";         // no ARV for you (LOLs or LOFs)
        break;

// --- vaccination (GVR only)
    case 86 :
        if (eGVRCustomerType != eCustomerType)
        // no good flavor for ARV visits, sayonara
            return v;

    // allow this ratecode to subvert/change all visit flavors except absentees (regardless of leniency setting)
        if (eAbsentVisitFlavor != e)            // absentee trumps vaccination
            e = eVaccinationVisitFlavor;        // only detected by ratecode thus no overriding

        v.bOk = true;
        break;

// --- family counseling (GVR only)
    case 87 :
        if (eGVRCustomerType != eCustomerType)
        // no good flavor for ARV visits, sayonara
            return v;

    // allow this ratecode to subvert/change all visit flavors except absentees (regardless of leniency setting)
        if (eAbsentVisitFlavor != e)            // absentee trumps family counseling
            e = eFamilyCounselingVisitFlavor;   // only detected by ratecode thus no overriding

        v.bOk = true;
        break;
    }

// --- check that absentees have kosher ratecodes and fees ---------------------------------
    if (eAbsentVisitFlavor == e)
    {
    // visit type is absentee but rate code is not "20"?
        if (20 != nRateCode)
        // if we're lenient, allow ratecode "01" and "02" as well
            if ((!v.bLenient) || ((1 != nRateCode) && (2 != nRateCode)))
            {
            // no good, return an error
                v.sError = QString("Felaktig taxa %1 för besökstyp 5 (Utebliven)").arg(v.sRateCode);
                v.bOk    = false;
                return v;
            }

    // other fee than the usual absentee fee? if we're lenient allow fee 0 kr as well
        if (nAbsenteeFee != v.nFeePaid)
            if ((!v.bLenient) || (0 != v.nFeePaid))
            {
            // no good, return an error
                v.sError = QString("Felaktigt belopp (%1 kr) för uteblivet besök").arg(v.nFeePaid);
                v.bOk    = false;
                return v;
            }
    }

 // pat. < 18 years old should pay only for absent visits, private visits or OOBs (regardless of leniency setting)
    if (eAgeBelow18 == eAge)
    {
        if ((eAbsentVisitFlavor != e) && (ePrivateVisitFlavor != e) && (eOOBVisitFlavor != e))
            if (0 != v.nFeePaid)
            {
                v.sError = QString("Felaktigt belopp (%1 kr) för pat. yngre än 18 år").arg(v.nFeePaid);
                v.bOk    = false;
                return v;
            }

    // if we're not lenient, they should be also registered with the correct rate code
        if (!v.bLenient)
        {
            int nRateCodeBelow18 = (eGVRCustomerType == eCustomerType) ? 60 /* GVR */ : 18 /* ARV */;
            if (nRateCodeBelow18 != nRateCode)
            {
                v.sError = QString("Felaktig taxa (%1) för pat. yngre än 18 år").arg(v.sRateCode);
                v.bOk    = false;
                return v;
            }
        }
    }

 // pat. > 84 years old should pay only for absent visits, private visits or OOBs (regardless of leniency setting)
    if (eAge85OrOlder == eAge)
    {
        if ((eAbsentVisitFlavor != e) && (ePrivateVisitFlavor != e) && (eOOBVisitFlavor != e))
            if (0 != v.nFeePaid)
            {
                v.sError = QString("Felaktigt belopp (%1 kr) för pat. 85 år eller äldre").arg(v.nFeePaid);
                v.bOk    = false;
                return v;
            }

    // if we're not lenient, they should be also registered with the correct rate code
        if (!v.bLenient)
        {
            int nRateCode85OrOlder = (eGVRCustomerType == eCustomerType) ? 6 /* GVR */ : 17 /* ARV */;
            if (nRateCode85OrOlder != nRateCode)
            {
                v.sError = QString("Felaktig taxa (%1) för pat. 85 år eller äldre").arg(v.sRateCode);
                v.bOk = false;
                return v;
            }
        }
    }


// -----------------------------------------------------------------------------------------
// for ARV visits: support partial fees/ratecodes (code 21: 5 kr ... code 74: 270 kr)
    if ((eGVRCustomerType != eCustomerType) && (nRateCode > 20) && (nRateCode < 75))
    {
        v.nExpectedFee = 5 * (nRateCode - 20);  // ratecode 21 = 5 kr .. ratecode 74 = 270 kr

    // support legacy ratecodes (before 2024)
        if (v.dVisit < QDate(2024,01,01))
            v.nExpectedFee = 10 * (nRateCode - 20);  // ratecode 21 = 10 kr .. ratecode 44 = 240 kr

        v.bOk = true;
    }

// normal fee paid?
    v.bNormalFeePaid = (nDefaultFee == v.nFeePaid);

// too much fee paid? (negative amounts are handled at the beginning)
    bool bTooMuchPaid = false;   // optimistic default;
    switch (e)
    {
    // for these we are a bit more relaxed about  how much fee was paid
    case ePrivateVisitFlavor          :
    case eInsuredPatientVisitFlavor   :
    case eOOBVisitFlavor              :
    case eVaccinationVisitFlavor      :
    case eFamilyCounselingVisitFlavor :
        break;

    case eAbsentVisitFlavor :
    // (we're assuming the absentee fee is greater than the default fee)
        bTooMuchPaid = (v.nFeePaid > nAbsenteeFee);
        break;

    default :
        bTooMuchPaid = (v.nFeePaid > nDefaultFee);
    }

    if (bTooMuchPaid)
    {
        v.bOk    = false;
        v.sError = QString("Felaktigt belopp %1 kr").arg(v.nFeePaid);
        return v;
    }

// struct ok?
    if (v.bOk)
    // yes. so clear any error message
        v.sError = "";
    else
    // no soup for you
        return v;

// save the visit flavor and we're done
    v.evf = e;
    return v;
}

//----------------------------------------------------------------------------
// isPulseSecureInstalled
//
// 2022-09-05 First version
//----------------------------------------------------------------------------
bool TWTCSupport::isPulseSecureInstalled()
{
// probably works only on Windows
    return qEnvironmentVariable("Path").contains("\\Pulse Secure\\");
}

//----------------------------------------------------------------------------
// prettyPrintErrors
//
// 2022-08-23 First version
// 2022-09-05 If Pulse Secure is installed, add nag text
//----------------------------------------------------------------------------
// SQL Server selected errors as strings
// (TWDB's openSQLServer() returns these error numbers as strings)
#define ERROR_UNKNOWN                    "0"
#define ERROR_SERVER_NOTRUNNING          "2"
#define ERROR_SERVER_LOSTCONNECTION     "11"
#define ERROR_SERVER_NOTFOUND           "53"
#define ERROR_BAD_NETWORK               "67"
#define ERROR_SERVER_CANNOT_CONTACT  "10022"
#define ERROR_SERVER_SOCKET_ERROR    "10061"
#define ERROR_WRONG_USERORPASSWORD   "18456"
#define ERROR_SQL_ACCOUNT_DISABLED   "18470"

QString TWTCSupport::prettyPrintErrors(QString sError)
{
// first the easy one (no error)
    if (sError.isEmpty())
        return "";

// if SAM/Pulse is installed, add some nag text
    QString sSAMReminder;
    if (isPulseSecureInstalled())
        sSAMReminder = "\n(Om du använder SAM/Pulse, kolla att den är igång.)";

// check for some popular errors
    if (ERROR_UNKNOWN               == sError)
        return "Kan inte öppna Intelligence/vårdgivarkontot (okänt fel)." + sSAMReminder;

    if (ERROR_SERVER_NOTRUNNING     == sError)
        return "Intelligence-servern är inte igång.";

    if (ERROR_SERVER_LOSTCONNECTION == sError)
        return "Förbindelsen avbruten till Intelligence-servern." + sSAMReminder;

    if ((ERROR_SERVER_NOTFOUND       == sError) ||
        (ERROR_BAD_NETWORK           == sError))
        return "Kan inte öppna Intelligence/vårdgivarkontot." + sSAMReminder;

    if (ERROR_SERVER_CANNOT_CONTACT == sError)
        return "Ingen kontakt med Intelligence-servern." + sSAMReminder;

    if (ERROR_SERVER_SOCKET_ERROR   == sError)
        return "Förbindelsen avbruten till Intelligence-servern." + sSAMReminder;

    if (ERROR_WRONG_USERORPASSWORD  == sError)
        return "Felaktigt användarnamn eller lösenord vid inloggning till vårdgivarkontot.";

    if (ERROR_SQL_ACCOUNT_DISABLED  == sError)
        return "Vårdgivarkontot tillfälligt stängt.\n(Stänger kl. 21:30 varje kväll, brukar öppna igen omkring kl. 04:30.)";

// if we get here, use a generic text + the error number
    return "Fel i Intelligence/vårdgivarkontot: " + sError + ".";
}

//----------------------------------------------------------------------------
// openSQLServerFromIniFile
// two flavors: explict .ini file location or try the default places
//
// 2023-10-16 First version
//----------------------------------------------------------------------------
QString TWTCSupport::openSQLServerFromIniFile(TWAsyncDB *pDB, TWAppSettings::IniLocation eIL, QString sSectionName)
{
// have a live connection?
    if (nullptr == pDB)
        fubar("didn't expect pDB pointer to be zero");

// Intelligence is always readonly, so set the ODBC option for greater speed
    pDB->setODBCReadOnly();

// get the settings
    TWAppSettings settings(eIL);
    settings.setCurrentSection(sSectionName);

// and try opening the db
    QString sError = pDB->openSQLServer(settings.readString("Server" + settings.readString("UseServer")),
                                        settings.readString("Database"),settings.readString("UserName"),
                                        settings.readStringEncrypted(settings.readString("UserName"),"EncryptedPassword"),
                                        settings.readInt("PortNo"));

// translate errors (if any) and return a localized message (no errors: return an empty string)
    return TWTCSupport::prettyPrintErrors(sError);
}

// simpler version for those with a simpler life
QString TWTCSupport::openSQLServerFromIniFile(TWAsyncDB *pDB)
{
// simpler version: no support for a custom Intelligence section name
    QString sSectionName = "Intelligence";   // we'll just use the default here

// expect the params in either an embedded .ini file or in appdata (on Windows, else next to the app)
// if we find stuff in the R/W appdata file, use it, else fallback to the R/O embedded one
    TWAppSettings appDataSettings(TWAppSettings::eAppData); // try to open the R/W ini file
    appDataSettings.setCurrentSection(sSectionName);
    bool bAppData = !appDataSettings.readStringWithDefault("Database","").isEmpty();

// call our explicit chap above
    return TWTCSupport::openSQLServerFromIniFile(pDB,bAppData ? TWAppSettings::eAppData : TWAppSettings::eEmbedded, sSectionName);
}

//----------------------------------------------------------------------------
// probeSQLServer
//
// 2023-10-16 First version
//----------------------------------------------------------------------------
QString TWTCSupport::probeSQLServer(TWAppSettings::IniLocation eIL, QString sSectionName)
{
// use our own asynch DB instance and do the usual boilerplate
    TWAsyncDB db_prober;
    db_prober.setDBErrorCallback([](QString sError) { Q_UNUSED(sError); }); // ignore any error(s)
    TWTCSupport::setBobbyTablesPrefixFromIniFile(eIL,sSectionName);
    db_prober.setConnectionName("TCProber");

// try to open it using the specified ini file location
    QString sError = TWTCSupport::openSQLServerFromIniFile(&db_prober,eIL,sSectionName);
    if (!sError.isEmpty())
        return sError;

// as a probe, try to count # of CareUnits
    int nCount = 0;
    db_prober.select("count(*) as Count",stTCCorral.Codes_CareUnits,
    [&nCount](DBRowStruct rs)
    {
        nCount = rs.intValue("Count");
    },eDBRowsExpectExactlyOne);

// need something > 0
    if (0 == nCount)
        sError = "Kunde inte hämta ARV-data från Intelligence-servern (felaktigt SQL-anrop).";

// ok done probing, close up
    db_prober.close();

    return sError;
}

//----------------------------------------------------------------------------
// getBobbyTablesPrefix
//
// 2022-08-09 First version
//----------------------------------------------------------------------------
QString TWTCSupport::getBobbyTablesPrefix()
{
// hello to our bobby tables
    return TCBobbyTables::sPrefix;
}

//----------------------------------------------------------------------------
// setBobbyTablesPrefix
//
// 2022-08-09 First version
//----------------------------------------------------------------------------
void TWTCSupport::setBobbyTablesPrefix(QString sPrefix)
{
// have a chat with out bobby tables
    TCBobbyTables::setPrefix(sPrefix);
}

//----------------------------------------------------------------------------
// setBobbyTablesPrefixFromIniFile
// two flavors: explict .ini file location or try the default places
//
// 2023-10-16 First version
//----------------------------------------------------------------------------
void TWTCSupport::setBobbyTablesPrefixFromIniFile(TWAppSettings::IniLocation eIL, QString sSectionName)
{
// get the settings
    TWAppSettings settings(eIL);
    settings.setCurrentSection(sSectionName);

// set the prefix from the .ini file
    setBobbyTablesPrefix(settings.readString("Server" + settings.readString("UseServer") +
                         "TablePrefix" + settings.readString("UseTablePrefix")));
}

void TWTCSupport::setBobbyTablesPrefixFromIniFile()
{
    QString sSectionName = "Intelligence";   // we'll just use the default here

// expect the params in either an embedded .ini file or next to the app
// we're using the default section name ---> only one app/user per desktop
    TWAppSettings embeddedSettings(TWAppSettings::eEmbedded);
    embeddedSettings.setCurrentSection(sSectionName);
    bool bNextToApp = embeddedSettings.readStringWithDefault("Database","").isEmpty();

// call our explicit chap above (use the default section name)
    TWTCSupport::setBobbyTablesPrefixFromIniFile(bNextToApp ? TWAppSettings::eNextToApp : TWAppSettings::eEmbedded, sSectionName);
}


//----------------------------------------------------------------------------
// getTCUserNameFromSmartcard
//
// 2022-11-23 First version
//----------------------------------------------------------------------------
QString TWTCSupport::getTCUserNameFromSmartcard(int nCardNo /* = 0 */)
{
// = Windows specific stuff follows
#if defined(Q_OS_WIN)
    QString sPrefix = "SE2321000016-";                  // hardwired for Region Stockholm

    QStringList slTCUserNames;                          // use a list if we detect more than one (after deduplication)
    auto hCertStore = CertOpenSystemStore(NULL,L"MY");  // "MY" is supposed to contain all the good stuff
    if (nullptr == hCertStore)
        return "";  // no certs no smartcards no dice

// browse the cert store, looking for a name that has our prefix
    PCCERT_CONTEXT pCertContext = nullptr;
    wchar_t pszCertString[2 * MAX_PATH];                // double MAX_PATH should be enough for everybody
    while (pCertContext = CertEnumCertificatesInStore(hCertStore,pCertContext))  // stepping through
        if (CertGetNameString(pCertContext,CERT_NAME_RDN_TYPE,0,nullptr,pszCertString,2 * MAX_PATH))
        {
            QString sCertString = QString::fromWCharArray(pszCertString);
            if (!sCertString.contains(sPrefix))
                continue;   // not there, next please

        // extract the username (expected after the prefix)
            auto sTCUserNameAndEmail = sCertString.mid(sCertString.lastIndexOf(sPrefix) + sPrefix.length());

        // toss the emailaddr. at the end (expected to begin with a comma)
            auto slTwo = sTCUserNameAndEmail.split(",");
            auto sTCUserName = slTwo[0];

        // already present in the list? (deduplication)
            if (slTCUserNames.contains(sTCUserName))
                continue;   // yes so skip to next

        // else say hello to my little friend
            slTCUserNames += sTCUserName;
        }

// we're done, close
    CertFreeCertificateContext(pCertContext);
    CertCloseStore(hCertStore,0);

// retrieve the matching user name (support multiple names/multiple smartcards but it's not very common)
    QString sTCUserName = "";
    if (slTCUserNames.count() > nCardNo)    // don't step outside the array
        sTCUserName = slTCUserNames[nCardNo];

// that's all
    return sTCUserName;
#endif
// not Windows?
    Q_UNUSED(nCardNo)
    fubar("a valiant attempt but getTCUserNameFromSmartcard() only works on Windows");
}

// --- use these chaps for TC fiddling --------------------------------------
#define TC_LOGGEDINCLASS_PREFIX "DyalogSysTray"
#define TC_CLASSNAME_PREFIX     "DyalogForm"
#define TC_MAINCAPTION          "Take Care"
#define TC_LOCKEDCAPTION        "TakeCare är låst"
#define TC_CHILD_PREFIX         "DyalogEdit"

//----------------------------------------------------------------------------
// getTCAppState
//
// 2022-12-18 First version
//----------------------------------------------------------------------------
eTCAppState TWTCSupport::getTCAppState()
{
#if defined(Q_OS_WIN)
// make this one static so it's reachable from within the lambda
    static eTCAppState appState;

    appState = eTCAppNotStarted;  // state we'll return if we find no "DyalogForm..." chaps
    EnumWindows([](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the current window's classname
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);
        if (sClassName.startsWith(TC_LOGGEDINCLASS_PREFIX))
        {
        // spotted a logged-in TC (i.e. with a systray) that's all the info we need
            appState = eTCAppLoggedIn;
            return FALSE;   // don't need any more enumerations, thanks
        }

    // look for our generic classname
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_CLASSNAME_PREFIX))
            return TRUE;    // not one of ours, next please

    // we got a classname match: means a TC instance is running
    // it's not logged in, so it can be either logged out or locked
        appState = eTCAppLoggedOut;  // assume logged out

    // check the caption
        memset(szWin32HobbyHorse,0,sizeof(szWin32HobbyHorse));   // clear before use
        GetWindowTextW(hWnd,szWin32HobbyHorse,MAX_PATH - 1);     // ignore any errors
        QString sCaption = QString::fromWCharArray(szWin32HobbyHorse);

        if (TC_LOCKEDCAPTION == sCaption)
        {
        // so this is a locked TC (not logged out)
            appState = eTCAppLocked;
            return FALSE;   // don't need any more enumerations, thanks
        }

    // continue enumerate please
        return TRUE;
    }, 0);  // 0 = currently unused param

// we're done return the state
    return appState;
#endif
// not Windows?
    fubar("getTCAppState() only works on Windows");
}

//----------------------------------------------------------------------------
// getTCPath
//
// 2022-12-19 First version
//----------------------------------------------------------------------------
QString TWTCSupport::getTCPath()
{
#if defined(Q_OS_WIN)
    static QString sTCPath; // make it static so it is reachable from within the lambda
    sTCPath = "";           // set a pessimistic default (none found)

// 1st attempt: try to retrieve the path via a running TakeCare and a window caption
    EnumWindows([](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the current window's classname
        wchar_t szWin32HobbyHorse[MAX_PATH];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for our generic classname
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_CLASSNAME_PREFIX))
            return TRUE;    // not one of ours, next please

    // we got a classname match: means a TC instance is running
    // check the caption if it contains a backslash
        memset(szWin32HobbyHorse,0,sizeof(szWin32HobbyHorse));   // clear before use
        GetWindowTextW(hWnd,szWin32HobbyHorse,MAX_PATH - 1);     // ignore any errors
        QString sCaption = QString::fromWCharArray(szWin32HobbyHorse);

        int nBackslashPos = sCaption.indexOf("\\");
        if (nBackslashPos > 20) // skip some UUID detritus
        {
            sTCPath = sCaption.mid(nBackslashPos - 2);
            return FALSE;   // we're done (that's all we need right now)
        }

    // gimme more windows
        return TRUE;

    }, 0);  // 0 = currently unused param

// got something and the directory exists?
    if (!sTCPath.isEmpty())
        if (QFile::exists(sTCPath))
            return sTCPath;

// 2nd attempt: try lookup the path in the registry
    auto regProber = [](QString sReg)
    {
        QSettings r(sReg,QSettings::NativeFormat);

        for (auto g : r.childGroups())
            if (g.startsWith("Dyalog APL"))
            { // construct a reg path and undo Qt replacing / with \ for strings read from registry
                QSettings r2(sReg + "\\" + g.replace("\\","/"),QSettings::NativeFormat);
                auto path = r2.value("dyalog").toString();
                if (path.endsWith("\\"))
                    return path;  // I think we got something useful
            }

        return QString();  // no dice return nothing
    };

// first try 64-bit flavored registry entry for TC (whic is a 32-bit app)
    sTCPath = regProber("HKEY_LOCAL_MACHINE\\SOFTWARE\\WOW6432Node\\Dyadic");
    if (sTCPath.isEmpty())
    // then try 32-bit
        sTCPath = regProber("HKEY_LOCAL_MACHINE\\SOFTWARE\\Dyadic");

// regardless if we found something or not, return the path
    return sTCPath;

#endif
// not on Windows?
    fubar("getTCPath() only works on Windows");
}

//----------------------------------------------------------------------------
// getCurrentTCPersonNo
//
// 2023-01-11 First version
//----------------------------------------------------------------------------
QString TWTCSupport::getCurrentTCPersonNo()
{
#if defined(Q_OS_WIN)
// static so reachable from within the lambda
    static HWND hWndParent;
    hWndParent = NULL; // pessimistic default

// look for the TC parent window
    EnumWindows([](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the current window's classname
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for our generic classname
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_CLASSNAME_PREFIX))
            return TRUE;    // not one of ours, next please

    // we got a classname match: check the caption of this window
        memset(szWin32HobbyHorse,0,sizeof(szWin32HobbyHorse));   // clear before use
        GetWindowTextW(hWnd,szWin32HobbyHorse,MAX_PATH - 1);     // ignore any errors
        QString sCaption = QString::fromWCharArray(szWin32HobbyHorse);

        if (TC_MAINCAPTION != sCaption)
            return TRUE;    // not ours, next please

    // almost there: does this window own any child windows?
        wchar_t awClassName[MAX_PATH * 2] = {0};
        sClassName.toWCharArray(awClassName);
        if (nullptr == FindWindowEx(hWnd,nullptr,awClassName,nullptr))
        // no children: forget about this window, next please
            return TRUE;

    // got what we looked for, save the hWnd for this window
        hWndParent = hWnd;
        return FALSE;   // don't need any more enumerations, thanks
    }, 0);  // 0 = currently unused param

// found the window?
    if (NULL == hWndParent)
        return "";  // no sorry

// step #2: look for a child window containing the personno (or reservno)
    static QString sPersonNo;   // static so it's visible from within the lambda
    sPersonNo = "";
    EnumChildWindows(hWndParent,[](HWND hWnd,LPARAM lParam)
    {
        Q_UNUSED(lParam)    // currently unused

    // get the classname (you know the drill by now)
        wchar_t szWin32HobbyHorse[MAX_PATH * 2];
        int nClassNameLen  = GetClassNameW(hWnd,szWin32HobbyHorse,MAX_PATH);
        QString sClassName = QString::fromWCharArray(szWin32HobbyHorse);

    // look for DyalogEdits
        if ((0 == nClassNameLen) || !sClassName.startsWith(TC_CHILD_PREFIX))
            return TRUE;    // next please

    // we got a classname match: check the caption for this window
    // instead of GetWindowsText use SendMessage (works better)
        ZeroMemory(szWin32HobbyHorse,sizeof(szWin32HobbyHorse));
        SendMessage(hWnd,WM_GETTEXT,static_cast<WPARAM>(sizeof(szWin32HobbyHorse)),reinterpret_cast<LPARAM>(szWin32HobbyHorse));
        QString s = QString::fromWCharArray(szWin32HobbyHorse);

    // check for a string that looks like this "nn  nnnnnn-nnnn" or "nn  nnnnnn+nnnn"
        if ((11 != s.length()) && (15 != s.length()))
        // no good, expected a length of 11 (for reservnr) or 15 (personno)
            return TRUE;    // next please

    // prepend "99" if it looks like a reservnr
        if (11 == s.length())
            s = "99" + s;

    // ask TWUtil to verify the number
        s = TWUtils::tossAllButDigits(s);
        if ("" != TWUtils::checkPersonNo(s))
        // no good, most likely not a valid personno or reservnr
            return TRUE;    // next window please

    // gotcha stop the enumeation
        sPersonNo = s;
        return FALSE;
    }, 0);  // 0 = currently unused param

    return sPersonNo;
#endif
// not Windows?
    fubar("Nice try but getCurrentTCPersonNo() only works on Windows");
}

//----------------------------------------------------------------------------
// openTCPersonNo
//
// 2022-11-24 First version
// 2022-12-19 Check for an ok app state before launching
//----------------------------------------------------------------------------
QString TWTCSupport::openTCPersonNo(QString sTCPath, QString sTCUserName, QString sPersonNo)
{
#if defined(Q_OS_WIN)
// hardwired TC app name
    QString sTakeCareApp = "dyalogrt.exe";

// make sure the path ends with a backslash
    if (!sTCPath.endsWith("\\"))
        sTCPath += "\\";

// check it the TC app exists at that place
    if (!QFile::exists(sTCPath + sTakeCareApp))
        return QString("Kan inte hitta TakeCare installerad på din dator.\n(Förväntad plats är: \"%1\".)").arg(sTCPath);

// check the state of the TC app (some states are worse than others)
    auto appState = TWTCSupport::getTCAppState();
    if (appState == eTCAppLocked)
        return "Kan inte öppna pat.\nTakeCare är låst.";
    if (appState == eTCAppLoggedOut)
        return "Kan inte öppna pat.\nTakeCare är i utloggat läge.\n(Du behöver starta TakeCare igen.)";

// ok try to launch
    QString sCmdLine = QString("%1%2 %1starter.dws system=\"1\" action=\"openrecord\" username=\"%3\" patientid=\"%4\"")
                               .arg(sTCPath,sTakeCareApp,sTCUserName,TWUtils::tossAllButDigits(sPersonNo));

// bombs away
    if (QProcess::startDetached(sCmdLine))
        return "";  // we did good

// fail
    return "Kunde inte starta TakeCare, sorry";

#endif
// not Windows?
    Q_UNUSED(sTCPath)
    Q_UNUSED(sTCUserName)
    Q_UNUSED(sPersonNo)
    fubar("openTCPersonNo() only works on Windows");
}


// -------------------------------------------------------------------------------------------------------------------
// Samba crawler classes: introducing a public helper class and the worker class (which will be run in another thread)
// the crawler picks up all receiptno/personno pairs for the given date period
SambaCrawler::SambaCrawler()
{
// just new our helpers up here, do the rest in crawl()
    thread = new QThread();
    worker = new SambaWorker();

// default to remote Samba server
    worker->bConnectToServer = true;

// no reuse please (only one crawl per class instance)
    nNoOfCrawls = 0;

// this will be set when we're done
    bCrawlingDone = false;
}

SambaCrawler::~SambaCrawler()
{
// time to toss our thread (the worker should have been deleted already)
    delete thread;
}

//----------------------------------------------------------------------------
// setSambaCredentials
//
// 2021-10-09 First version
// 2024-08-09 Introduce support for specifiying Samba portno
//----------------------------------------------------------------------------
void SambaCrawler::setSambaCredentials(QString sServerName, int nPortNo, QString sShareName, QString sUserName, QString sPassword)
{
// just store 'em and wait for the crawler to start crawling
    worker->bConnectToServer = true;

// a slight sanity check
    if (sServerName.isEmpty() || (nPortNo < 1))
        guruMeditation("empty servername or portno < 1");

// worker uses QByteArrays for easier interactions with libsmb2 (written in C)
// convert from QString to Latin1 (assuming we're 100% using characters within Latin1)
    worker->baServerName = (sServerName + ":" + QString::number(nPortNo)).toLatin1(); // always add the portNo here (simpler)
    worker->baShareName  = sShareName.toLatin1();
    worker->baUserName   = sUserName.toLatin1();
    worker->baPassword   = sPassword.toLatin1();
}

//----------------------------------------------------------------------------
// setSambaCredentialsFromIniFile two flavors
//
// 2021-12-18 First version
// 2024-08-09 Introduce support for Samba portno (default is good ol' 445)
// 2024-10-10 Add support for R/W ini file and custom sections
//----------------------------------------------------------------------------
void SambaCrawler::setSambaCredentialsFromIniFile(TWAppSettings::IniLocation eIL, QString sSectionName)
{
// prepare the settings
    TWAppSettings settings(eIL);
    settings.setCurrentSection(sSectionName);

// get and set all the stuff
    int nPortNo = settings.readIntWithDefault("PortNo",445);
    setSambaCredentials(settings.readString("ServerName"),nPortNo,
                        settings.readString("ShareName"),settings.readString("UserName"),
                        settings.readStringEncrypted(settings.readString("UserName"),"EncryptedPassword"));

// that's all, folks
}

void SambaCrawler::setSambaCredentialsFromIniFile()
{
// note: for this simple version: no support of a custom Samba section name in settings
    QString sSectionName = "Samba";   // just use the vanilla

// expect the params in either an embedded .ini file or in appdata (on Windows, else next to the app)
// if we find stuff in the R/W file use it, else fallback to the R/O (embedded)
    TWAppSettings appDataSettings(TWAppSettings::eAppData); // try to open the R/W flavor
    appDataSettings.setCurrentSection(sSectionName);
    bool bAppData = !appDataSettings.readStringWithDefault("ServerName","").isEmpty();

// call the more advanced version above
    setSambaCredentialsFromIniFile(bAppData ? TWAppSettings::eAppData : TWAppSettings::eEmbedded, sSectionName);
}

//----------------------------------------------------------------------------
// setSambaDirectory
// this for the local directory option
//
// 2021-10-17 First version
//----------------------------------------------------------------------------
void SambaCrawler::setLocalDirectory(QString sLocalDirectory)
{
// don't connect to a Samba server, instead use a local directory
    worker->bConnectToServer = false;

// if there's a trailing directory separator, toss it
// (the samba worker inserts one for you)
    if (sLocalDirectory.right(1) == QDir::separator())
        sLocalDirectory.chop(1);

// a slight check of sanity
    if (sLocalDirectory.isEmpty())
        fubar("Didn't expect an empty sLocalDirectory");

// and save it in the worker
    worker->sLocalDirectory = sLocalDirectory;
}

//----------------------------------------------------------------------------
// crawl (prepares for the asynchronous crawl below)
//
// 2021-10-10 First version
// 2021-10-16 Don't forget to call qRegisterMetaType for our map type
//----------------------------------------------------------------------------
void SambaCrawler::crawl(int nCompanyCode, int nDaysBack /* = 500 */)
{
// make sure this is the first (and only) call for this instance
    if (++nNoOfCrawls > 1)
        fubar("SambaCrawler: only one crawl allowed");

// check our params
    if (nCompanyCode < 1)
        fubar("bad or illegal CompanyCode");

    if (nDaysBack < 1)
        fubar("nDaysBack must be 1 or higher");

// register our map type so that the queued connect() works
    int nTypeID = qRegisterMetaType<MapReceiptNo2PersonNo_t>();
    if (0 == nTypeID)
        guruMeditation("qRegisterMetaType() returned 0");

// ok they seem fine, set them in the worker
    worker->nCompanyCode = nCompanyCode;
    worker->nDaysBack    = nDaysBack;

// let the worker do its thing in another thread
    worker->moveToThread(thread);

// connect so that the crawl starts right after that thread starts
    connect(thread,&QThread::started,worker,&SambaWorker::crawl);

// and connect the signals
    connect(worker,&SambaWorker::error   ,this,&SambaCrawler::error);
    connect(worker,&SambaWorker::finished,this,&SambaCrawler::finished);

// when the thread is stopped --> toss the worker
    connect(thread,&QThread::finished,worker,&SambaWorker::deleteLater);

// bombs away (let the crawling commence)
    thread->start();
}

//----------------------------------------------------------------------------
// error
//
// 2021-10-11 First version
//----------------------------------------------------------------------------
void SambaCrawler::error(QString sError)
{
// do we have an error message? if not create a generic one
    if (sError.isEmpty())
        sError = "Samba error";

// and save the message
    sErrorMsg = sError;
}

//----------------------------------------------------------------------------
// finished
//
// 2021-10-11 First version
//----------------------------------------------------------------------------
void SambaCrawler::finished(MapReceiptNo2PersonNo_t m, QDate dFirst, QDate dLast)
{
// save the map and the date range that was actually used
    mRP         = m;
    dFirstFound = dFirst;
    dLastFound  = dLast;

// we're done,
    bCrawlingDone = true;

// emit the done signal
    emit crawlingDone(mRP.count());

// and say goodbye (both the worker and the thread are disposed of via the deleteLater signal)
    thread->quit();
    thread->wait();
}

//----------------------------------------------------------------------------
// crawl (where the real work is done)
//
// 2021-10-11 First version
// 2024-08-23 Say hello to libsmb2
//----------------------------------------------------------------------------
void SambaWorker::crawl()
{
    QString sSAMReminder; // if SAM/Pulse is installed, add text (used for error messages)
    if (TWTCSupport::isPulseSecureInstalled())
        sSAMReminder = "\n(Om du använder SAM/Pulse, kolla att den är igång.)";

// make sure our map is empty
    mRP.clear();

// have the params set for a successful crawling?
    if ((sLocalDirectory.isEmpty()) && (baServerName.isEmpty()))
        fubar("Please call either setCredentials() or setLocalDirectory() before crawling");

// connecting to a Samba server?
#if defined(Q_OS_WIN)
    bool bCallWSACleanup = false;   // for Windows: keep track of WSAStartup() ok or not
#endif

    if (bConnectToServer)
    {
    // bring up the libsmb2 stuff (Windows needs a bit of preamble)
    #if defined(Q_OS_WIN)
        WSADATA wsaData;

        if (WSAStartup(0x202,&wsaData) != 0)  // 0x202: Windows XP and newer
        {
        // not a happy camper today
            emit error("Kan inte öppna Sambaservern (WSAStartup() failed)" + sSAMReminder);
            goto sayonara;
        }

        bCallWSACleanup = true; // since WSAStartup succeeded
    #endif

    // say hello to libsmb2
        smb2 = smb2_init_context();
        if (smb2 == NULL)
        {
        // no good luck today
            emit error("Kan inte öppna Sambaservern (smb2_init_context() failed)" + sSAMReminder);
            goto sayonara;
        }

    // so far so good, set the credentials
        smb2_set_user    (smb2,baUserName.data());
        smb2_set_password(smb2,baPassword.data());
        smb2_set_security_mode(smb2,SMB2_NEGOTIATE_SIGNING_ENABLED);

    // try to connect
        if (smb2_connect_share(smb2,baServerName.data(),baShareName.data(),baUserName.data()) != 0)
        {
        // server no happy today
            emit error("Kan inte öppna Sambaservern :-(" + sSAMReminder);
            goto sayonara;
        }
    }
    else
    // using local directory: check that we have a specified directory
        if (sLocalDirectory.isEmpty())
            fubar("sLocalDirectory is empty, not good");

// prepare the date chaps, they will keep track of the first and last dates when nonempty StgA* files are found
    dFirst = dLast = QDate();

// step backwards through the days trying to open Samba files for those dates (start with yesterday, the youngest file possible)
    for (QDate d = QDate::currentDate().addDays(-1); d >= QDate::currentDate().addDays(0 - nDaysBack); d = d.addDays(-1))
    {
        bool bFoundDuplicateReceiptNo = false;  // if we find a duplicate quit the loop (regardless of # of days stepped back)

    // construct the filename for this date (and construct a twin QByteArray in case we will be using libsmb2)
        QString sFilename     = QString(ssSambaFilenamePattern).arg(TWUtils::toYMD(d)).arg(nCompanyCode,4,10,QChar('0'));
        QByteArray baFileName = sFilename.toLatin1();

    // read all the contents of the file into this QStringList
        QStringList slLines;

    // use libsmb2 or reading a local file?
        if (bConnectToServer)
        {
        // prepare the libsmb2 stuff
            struct smb2fh* fh;

        // and try to open the file
            fh = smb2_open(smb2, baFileName.data(), O_RDONLY);

            if (NULL != fh)
            {
            // opened ok, allocate a big ass array for the contents
                QByteArray baBuf(16 * 1024 * 1024,Qt::Initialization::Uninitialized);
                uint32_t pos = 0;
                int count;

            // read all
                while ((count = smb2_pread(smb2, fh, reinterpret_cast<uint8_t*>(baBuf.data()), baBuf.size(), pos)) != 0)
                {
                    if (count == -EAGAIN)
                        continue;

                    if (count < 0)
                    // some problem/error, just skip this silenty
                        break;

                    pos += count;
                }

            // we're done, close up
                smb2_close(smb2, fh);

            // copy what we got into the stringlist (and convert from Latin1)
                slLines = QString::fromLatin1(baBuf.data(),pos).split("\n");
            }
        }
        else
        // local directory: just read the file into the stringlist
            slLines = TWTextFileReader::readAll(sLocalDirectory + QDir::separator() + sFilename,false);  // file is in Latin1

    // --- step thru the file (in case of failed file opens we'll just process an empty stringlist, no problem)
        for (auto sLine : slLines)
        {
        // bypass lines that are not long enough ...
            if (sLine.length() < nnSambaValidLineLength)
                continue;   // next line please

        // ... or do not have a valid date and time at the beginning
            QDateTime dt = TWUtils::fromISODateTime(sLine.mid(nnSambaDateTimePosition,nnSambaDateTimeLength));
            if (!dt.isValid())
                continue;

        // ok get the receiptno and personno
            QString sReceiptNo = sLine.mid(nnSambaReceiptNoPosition,nnSambaReceiptNoLength);
            QString sPersonNo  = sLine.mid(nnSambaPersonNoPosition ,nnSambaPersonNoLength );

        // clear 'em from detritus
            sReceiptNo = TWUtils::tossAllWhiteSpace(sReceiptNo);
            sPersonNo  = TWUtils::tossAllButDigits(sPersonNo);

        // are they *both* nonempty?
            if ((sReceiptNo.isEmpty()) || (sPersonNo.isEmpty()))
                continue;  // no at least one is empty, next line please

        // this receiptno exists already in the map?
        // (can happen since they wraparound for every 9000 receipts, i.e. nnn-9999-c + 1 --> nnn-1000-c)
            if (mRP.contains(sReceiptNo))
            {
                bFoundDuplicateReceiptNo = true;
                break;  // say we're done, regardless of # of days remaining
            }

        // ok got another pair to stash our map
            mRP.insert(sReceiptNo,sPersonNo);

        // --- remember the dates for this merry occasion
            dFirst = d;             // always set this one since we're stepping backwards in time
            if (!dLast.isValid())   // but set this one only first time
                dLast = d;
        }

    // step back one more day or call it quits?
        if (bFoundDuplicateReceiptNo)
            break;  // found at least one duplicate receiptno, that's all for now thank you
    }

// arrive here when we're done
sayonara:
    emit finished(mRP,dFirst,dLast);

// if we've used libsmb2 kiss it goodbye now
    if (bConnectToServer)
    {
        if (NULL != smb2)
        {
            smb2_disconnect_share(smb2);
            smb2_destroy_context(smb2);
        }

    #if defined(Q_OS_WIN)
        if (bCallWSACleanup)
            WSACleanup();
    #endif
    }
}
