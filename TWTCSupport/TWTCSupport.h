// implement longer date ranges in the Samba crawler: prefix receiptnos. with visit date in the QMap
// use YYMM of the visit date as a 4 digit prefix number, e.g. visit date 2023-10-06 gives prefix 2310
// we need to support date range: [2010 .. 2099]

/*------------------------------------------------------------------------------*/
/* TWTCSupport.h                                                                */
/*                                                                              */
/* 2019-10-20 First version                                                 HS  */
/* 2021-10-06 Say hello to the asynchronous Samba crawler                   HS  */
/* 2024-09-08 Switch to libsmb2 https://github.com/sahlberg/libsmb2         HS  */
/*------------------------------------------------------------------------------*/

#pragma once

// pull in TWUtils and TWDB
#include INCLUDETWUTILS
#include INCLUDETWDB

#if defined(TWTCSupport_LIBRARY) || defined(TWSTATICBUILD)
#define TWTCSupportSHARED_EXPORT Q_DECL_EXPORT
#else
#define TWTCSupportSHARED_EXPORT Q_DECL_IMPORT
#endif

// type alias chaps
using PID_t                   = qint64;                 // encrypted personnos in Intelligence
using MapReceiptNo2PersonNo_t = QMap<QString,QString>;  // the Samba crawler returns this map
using MapPID2PersonNo_t       = QMap<PID_t,QString>;    // this is what you want

// TC visit stuff -------------------------------------------------------------------
enum eTCCustomerTypes   { eUnknownCustomerType, eGVRCustomerType,
                          eLOLCustomerType, eLOFCustomerType};

enum eTCVisitFlavors    { eUnknownVisitFlavor,
                          eNormalVisitFlavor, eDistanceContactVisitFlavor,
                          eHomeVisitFlavor, eTeamVisitFlavor, eTeamHomeVisitFlavor,
                          eGroupVisitFlavor, eGroupTeamVisitFlavor,
                          eExternalVisitFlavor, eTeamExternalVisitFlavor,
                          ePrivateVisitFlavor, eFamilyCounselingVisitFlavor,
                          eVaccinationVisitFlavor,
                          eInsuredPatientVisitFlavor, eAbsentVisitFlavor,
                          eOOBVisitFlavor };

enum eAppointmentStatus { eWaitingStatus, eBookedStatus, /* skipping some enums for now */
                          eRescheduledStatus = 7, eCancelledStatus = 9, eArrivedStatus = 10 };

// TC app states
// (for getting current personno.: eTCAppLoggedin or eTCAppLocked is fine)
// (for opening a new personno.: eTCAppNotStarted or eCTCAppLoggedIn is ok)
enum eTCAppState { eTCUnknownAppState, eTCAppNotStarted, eTCAppLoggedIn, eTCAppLocked, eTCAppLoggedOut};

// struct for a TC visit-------------------------------------------------------------
struct TCVisitStruct
{
// inputs (these are expected to be set by the caller)
    QDate   dVisit;          // different years --> different fees
    QString sVisitType;      // single digit or letter e.g. A or H
    QString sRateCode;       // "01" -- "99" GVR or ARV rate code
    int     nAgeInYears;     // defaults to -1 (unknown)
    int     nFeePaid;        // what the patient paid
    bool    bLenient;        // true: absentees and OOB entries are allowed some leeway

// outputs: set by digestVisit() ----------------------------------------------------
    bool    bOk;             // if false, there's an error message below
    QString sError;          // nonempty if error(s) found
    eTCVisitFlavors evf;     // yes the flavor of the visit
    bool    bShownLeniency;  // bLenient == true means clemency was shown for absentees and OOBs
    int     nExpectedFee;    // derived from visittype and ratecode (or -1 if undecided)
    bool    bNormalFeePaid;  // the normal fee currently (early 2023): 250 kr

// ARV specific stuff (i.e. not applicable for GVR visits)
    bool    bARVMaterial;    // default = true unless ratecode is "19", "20" or "60"
    QString sRateCodeForARV; // ARV internal ratecodes: "18" (less than 18 years old) and "09" (smittskydd)
                             // same as the original above except for code "18" ---> "12" and code "09" ---> "07"
// set the defaults in the ctor
    TCVisitStruct()
    {
        nAgeInYears = nFeePaid = nExpectedFee = -1;
        bLenient = bOk = bShownLeniency = bNormalFeePaid = bARVMaterial = false;
        evf = eUnknownVisitFlavor;
    }

// prettyprints the visit flavor (in Swedish)
    QString prettyPrintVisitFlavor()
    {
        switch (evf)
        {
        case eNormalVisitFlavor           : return (("0" == sVisitType) ? "Nybesök" : "Återbesök");
        case eDistanceContactVisitFlavor  : return "Distanskontakt";
        case eHomeVisitFlavor             : return "Hembesök";
        case eTeamVisitFlavor             : return "Teambesök";
        case eTeamHomeVisitFlavor         : return "Teambesök i hemmet";
        case eGroupVisitFlavor            : return "Gruppbesök";
        case eGroupTeamVisitFlavor        : return "Gruppteambesök";
        case eExternalVisitFlavor         : return "Besök på annan plats";
        case eTeamExternalVisitFlavor     : return "Teambesök på annan plats";
        case ePrivateVisitFlavor          : return "Privatbetalande";
        case eVaccinationVisitFlavor      : return "Vaccination";
        case eFamilyCounselingVisitFlavor : return "Familjerådgivning";
        case eInsuredPatientVisitFlavor   : return "Försäkringspatient";
        case eAbsentVisitFlavor           : return "Uteblivet besök eller distanskontakt";
        case eOOBVisitFlavor              : return "Övriga avgifter";
        default                           : fubar("Unknown or unsupported visit flavor");
        }
    }

// returns true if this visit flavor ---> pat. visited the clinic
    bool isInClinicVisit()
    {
        switch (evf)
        {
        case eNormalVisitFlavor           : return true;
        case eDistanceContactVisitFlavor  : return false;
        case eHomeVisitFlavor             : return false;
        case eTeamVisitFlavor             : return true;
        case eTeamHomeVisitFlavor         : return false;
        case eGroupVisitFlavor            : return true;
        case eGroupTeamVisitFlavor        : return true;
        case eExternalVisitFlavor         : return false;
        case eTeamExternalVisitFlavor     : return false;
        case ePrivateVisitFlavor          : return true;
        case eVaccinationVisitFlavor      : return true;
        case eFamilyCounselingVisitFlavor : return true;
        case eInsuredPatientVisitFlavor   : return true;
        case eAbsentVisitFlavor           : return false;
        case eOOBVisitFlavor              : return true;
        default                           : fubar("Unknown or unsupported visit flavor");
        }
    }
};

//----------------------------------------------------------------
// our own version of TWDB's BobbyTables
class TWTCSupportSHARED_EXPORT TCBobbyTables : public BobbyTables
{
public:
    using BobbyTables::BobbyTables;    // reuse the constructor from the base class

    static void setPrefix(QString s);  // note: this affects *all* instances (the prefix is static)
    QString cook() override;           // table prefix resolver (called by TWDB)

    static QString sPrefix;            // for prefixing each table name (common for all TCBobbyTables)
};

//--------------------------------------------------
// introducing a static corral of popular TC tables
// 2024-03-22 Toss CaseNotes_ContainerUsers (it was a mirage anyway)
#define TCBOBBYTABLE(tableName) TCBobbyTables tableName{QStringLiteral(QT_STRINGIFY(tableName))};

static struct
{
    TCBOBBYTABLE(Appointments)
    TCBOBBYTABLE(CaseNotes)
    TCBOBBYTABLE(CaseNotes_ContainerDates)
    TCBOBBYTABLE(CaseNotes_ContainerMeasurements)
    TCBOBBYTABLE(CaseNotes_ContainerNotes)
    TCBOBBYTABLE(CaseNotes_DateTimes)
    TCBOBBYTABLE(CaseNotes_Measurements)
    TCBOBBYTABLE(CaseNotes_Notes)
    TCBOBBYTABLE(CaseNotes_RegistryCodes)
    TCBOBBYTABLE(CaseNotes_Templates)
    TCBOBBYTABLE(CaseNotes_Users)
    TCBOBBYTABLE(CaseNotes_ValueTerms)
    TCBOBBYTABLE(Codes_BillingCareProviders_v2)
    TCBOBBYTABLE(Codes_CareUnits)
    TCBOBBYTABLE(Codes_Companies_v2)
    TCBOBBYTABLE(Codes_Professions)
    TCBOBBYTABLE(Codes_Resources)
    TCBOBBYTABLE(Codes_ResourceTimeTypes)
    TCBOBBYTABLE(Codes_Users)
    TCBOBBYTABLE(Codes_VisitTypes)
    TCBOBBYTABLE(ConsultRefs)
    TCBOBBYTABLE(PAS)
    TCBOBBYTABLE(PAS_ActionsDRG)
    TCBOBBYTABLE(PAS_Billing)
    TCBOBBYTABLE(PAS_Billing_Providers)
    TCBOBBYTABLE(PAS_DiagnosesDRG)
    TCBOBBYTABLE(PatInfo)
    TCBOBBYTABLE(Users)
} stTCCorral;


//---------------------------------------------------------------------------------------------------------------
class TWTCSupportSHARED_EXPORT TWTCSupport
{
public:
// legacy support for storing PIDs in QStrings
    static PID_t   string2PID(QString sPID) { return sPID.toLongLong(); }
    static QString PID2String(PID_t PID)    { return QString::number(PID); }

// test patient support: retrieve a list of them or check if a personnr/PID is a test patient
    static QStringList getAllTestPatients();
    static bool        isTestPatient(QString sPersonNo);
    static bool        isTestPatient(PID_t PID);

// get the default patient fee and absentee fee (requires a visit date)
    static int     getDefaultFee(QDate dVisit);
    static int     getAbsenteeFee(QDate dVisit);

// TC careprovider support
    static QString getTitleForCareProviderID(QString sCareProviderID);
    static bool    isDoctor(QString sCareProviderID);

// prettyprint a personno.
    static QString personNoForTC     (QString sPNo) { return sPNo.left(2) + " " + sPNo.mid(2);                       }
    static QString personNoForSamba  (QString sPNo) { return sPNo.left(2) + " " + sPNo.mid(2,6) + "-" + sPNo.mid(8); }
    static QString personNoForFortnox(QString sPNo) { return sPNo.left(8) + "-" + sPNo.mid(8);                       }

// brush up your kombika code
    static QString canonicalizeKombika(QString sKombika); // reformats and corrects some common errors

// transmogrify a TC receipt no. (10 digits) into an ARV receipt no. (8 digits)
// (optionally adds a companion ATG nnn code as the upper 3 digits in the ARV receipt no.)
    static int     receiptNo2ARVReceiptNo(QString sTCReceiptNo, QString sCompanionATGCode = "");

// digest a TC visit (find out all the facts)
    static TCVisitStruct digestVisit(eTCCustomerTypes eCustomerType, TCVisitStruct stVT);

// MSSQLServer TC support ---------------------------------------------------
    static bool    isPulseSecureInstalled();
    static QString prettyPrintErrors(QString sError); // returns a nice Swedish text for some common SQL errors
    static QString openSQLServerFromIniFile(TWAsyncDB* pDB, TWAppSettings::IniLocation eIL, QString sSectionName);
    static QString openSQLServerFromIniFile(TWAsyncDB* pDB);
    static QString probeSQLServer(TWAppSettings::IniLocation eIL, QString sSectionName);

// support getting/setting the (global) prefix for your Bobby Tables --------
    static QString getBobbyTablesPrefix();
    static void    setBobbyTablesPrefix(QString sPrefix);
    static void    setBobbyTablesPrefixFromIniFile(TWAppSettings::IniLocation eIL, QString sSectionName);
    static void    setBobbyTablesPrefixFromIniFile();

// try to get a TC username from a smartcard reader (returns "" if this fails)
    static QString getTCUserNameFromSmartcard(int nCardNo = 0);

// establish state of the TC app
    static eTCAppState getTCAppState();

// try get the installed path to TC (returns an empty string if no TC can be found or TC is not installed)
    static QString getTCPath();

// get current Swedish idno. from a running TC, returns "" if no TC found or no patient is opened in TC
    static QString getCurrentTCPersonNo();

// try to launch TC with a personno. returns "" for success else an error message
    static QString openTCPersonNo(QString sTCPath, QString sTCUserName, QString sPersonNo);
};


//---------------------------------------------------------------------------------------------------------------
// Samba server crawler (reads Samba StgA* files in a separate thread)
class SambaWorker;  // (class launched from worker thread)

class TWTCSupportSHARED_EXPORT SambaCrawler : public QObject
{
// Qt boilerplate (so that signal and slots work)
    Q_OBJECT

public:
    SambaCrawler();
    ~SambaCrawler();

// set the Samba credentials or the directory where the StgA* files are
    void setSambaCredentials(QString sServerName, int nPortNo, QString sShareName, QString sUserName, QString sPassword);
    void setSambaCredentialsFromIniFile(TWAppSettings::IniLocation eIL, QString sSectionName);
    void setSambaCredentialsFromIniFile();
    void setLocalDirectory(QString sLocalDirectory);

// call here to start an asynchronous Samba crawl
    void crawl(int nCompanyCode,int nDaysBack = 444); // read StGA* files for company code and # of days back in time

// crawlingDone is emitted when we're done (or you can poll for the bCrawlingDone boolean --> true)
signals:
    void crawlingDone(int nMapCount);                                   // sErrorMsg == "" means it all went fine

private:
    QThread*     thread;
    SambaWorker* worker;
    int          nNoOfCrawls;                                           // only one crawl per class instance please

// these are the slots called from the worker
    void error(QString sError);                                         // called only if there's an error
    void finished(MapReceiptNo2PersonNo_t m,QDate dFirst,QDate dLast);  // this one is always called

public:
    bool    bCrawlingDone;             // for those who wish to poll
    QString sErrorMsg;                 // any errors? stored here
    MapReceiptNo2PersonNo_t mRP;       // receiptno --> personnos pairs are here
    QDate   dFirstFound,dLastFound;    // first and last dates set when one or more nonempty SgtA* files found
};                                     // (i.e. these will be empty (QDate()) if no receiptnos/personno. found)
