#--------------------------------------------------------------------------#
# TWTCSupport.pro                                                          #
#                                                                          #
# 2019-10-20 First version                                             HS  #
# 2024-08-11 Say hello to libsmb2 https://github.com/sahlberg/libsmb2  HS  #
#--------------------------------------------------------------------------#

QT      += core gui widgets

DEFINES += TWTCSupport_LIBRARY

HEADERS += TWTCSupport.h SambaWorker.h
SOURCES += TWTCSupport.cpp

TARGET   = TWTCSupport
TEMPLATE = lib

# include our common QtProjects helper
include(../../include/QtProjects.pri)

# note: libsm2 requires two .h files
DEFINES += $$TWDefineInclude(TWUtils)
DEFINES += $$TWDefineInclude(TWDB)
DEFINES += $$TWDefineInclude2(smb2,smb2)
DEFINES += $$TWDefineInclude2(smb2,libsmb2)

# for dynamic builds, we need to link with libsmb2 runtime
# currently we're doing dynamic builds only on Linux
linux: LIBS += $$projectRoot()/bin/libsmb2.so.1

# ---------------------------------------------------------------------
# building libsmb2: copy these .h files into lib/smb2
#   libsmb2.h and smb2.h
# to avoid copying more .h files comment out these #includes:
# in smb2.h: //#include "smb2-errors.h"
# in libsmb2: //#include <smb2/libsmb2-dcerpc-srvsvc.h>
#
#
# --- for Linux dynamic builds:
#   ~/Qt/Tools/CMake/bin/cmake .
#   # patch link.txt to give correct soname to the linker:
#   sed -i 's!-soname,libsmb2.so.1!-soname,/home/henry/Projects/bin/libsmb2.so.1!' lib/CMakeFiles/smb2.dir/link.txt
#   ~/Qt/Tools/CMake/bin/cmake --build .
#
#   # when done copy into ~Projects/bin:
#   cp -p lib/libsmb2.so.4.0.0 ~/Projects/bin/libsmb2.so.1
#
#
# --- for Windows static builds:
#  we need /MT (not the default /MD) so before building add these lines in CMakeLists.txt:
#
#      set(CompilerFlags
#        CMAKE_C_FLAGS
#        CMAKE_C_FLAGS_RELEASE
#        )
#      foreach(CompilerFlag ${CompilerFlags})
#        string(REPLACE "/MD" "/MT" ${CompilerFlag} "${${CompilerFlag}}")
#      endforeach()
#
# --- for Windows 32-bits static builds:
#   cmake -DBUILD_SHARED_LIBS=0 -A WIN32 .
#   cmake --build . --config Release
#
# after build copy smb2.lib to C:\Projects\bin
# don't forget: every app has to have this libline:
#    LIBS += $$TWLibLine(smb2)
# ---------------------------------------------------------------------
