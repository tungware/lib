/*--------------------------------------------------------------------------*/
/* TWUtils.cpp                                                              */
/*                                                                          */
/* 2014-06-08 First version                                             HS  */
/*--------------------------------------------------------------------------*/

#include "TWUtils.h"

#include "qpushbutton.h"             // IWYU pragma: keep
#include "qmessagebox.h"             // for the other dialog box flavors
#include "qtimer.h"                  //
#include "qcommandlineparser.h"      // used by the getArgs() function

#include "qstylefactory.h"            // for the Fusion style helpers
#include "qoperatingsystemversion.h"  // for the DPI unaware call

#include "qtablewidget.h"  // for the ui fixers/improvers
#include "qcombobox.h"     //
#include "qheaderview.h"   //
#include "qscrollbar.h"    //

#include "qdir.h"          // for the fubar helper

#include "qsettings.h"     // for the TWAppSettings class
#include "qsharedmemory.h" // for the single app support

#if defined(USE_PASSWORDBOX)
#include "qdialog.h"          // only pull these in if we need them
#include "qdialogbuttonbox.h" //
#include "qlabel.h"
#include "qlineedit.h"
#include "qgridlayout.h"
#endif

#if defined(Q_OS_WIN)
// pull in windows.h for ::OutputDebugString(), ::SetWindowPos() and other Win32 API chaps
#include "windows.h"
#endif

// handy console string putters
void qputs(QString s)
{
#if defined(Q_OS_WIN)
// on Windows we need to use the console OEM codepage
// Qt6 dropped support for the OEM codepage (a.k.a."IBM850") so do it using Win32
// Todo: use GetACP to check that the app is using the default "Windows 1252" code page
    QByteArray baACP = s.toLocal8Bit();
    int nWideChars = ::MultiByteToWideChar(CP_ACP, 0, baACP.constData(), baACP.count(), nullptr, 0);
    QByteArray baWChar(sizeof(WCHAR) * nWideChars,'\0');
    ::MultiByteToWideChar(CP_ACP, 0, baACP.constData(), baACP.count(), (LPWSTR) baWChar.data(), nWideChars);

// translate from ANSI CodePage to OEM (Console) CodePage
    int nBytes = ::WideCharToMultiByte(CP_OEMCP, 0, (LPWSTR) baWChar.constData(), nWideChars , nullptr, 0, nullptr, nullptr);
    QByteArray baOEM(nBytes + 1,'\0');  // be sure we have a terminating '\0' (proabably not needed)
    ::WideCharToMultiByte(CP_OEMCP, 0, (LPWSTR) baWChar.constData(), nWideChars, baOEM.data(), nBytes, nullptr, nullptr);

// let it all out
    puts(baOEM.constData());
#else
// if we're not Windows qputs() is slightly easier to implement
    puts(qUtf8Printable(s));
#endif
}

void qputs(QStringList sl) { for (auto s : sl) qputs(s); }

//-----------------------------------------------------------------------------------
// Qt specific: call this function in your main.cpp before creating QApplication
//
// 2020-05-09 First version
// 2024-04-08 Disable dark mode on Windows
//-----------------------------------------------------------------------------------
static QtMessageHandler qtDefaultMessageHandler;  // Qt's default message handler saved here

void TWUtils::beforeQApplication()
{
// require Qt 5.7 or later
#if (QT_VERSION < QT_VERSION_CHECK(5, 7, 0))
    fubar("Qt version 5.7 or later is required");
#endif

// ok, install our messagehandler and remember the Qt' default one (we'll use it later)
    qtDefaultMessageHandler = qInstallMessageHandler(TWUtils::twMessageHandler);

// support screen pixel scales > 100% (only needed for Qt5)
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    QGuiApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(5, 14, 0))
// if we're on Qt 5.14 or later we can set the HighDPI rounding policy to passthrough
    QGuiApplication::setHighDpiScaleFactorRoundingPolicy(Qt::HighDpiScaleFactorRoundingPolicy::PassThrough);
#endif

#if (QT_VERSION >= QT_VERSION_CHECK(6, 5, 0))
#if defined(Q_OS_WIN)
// starting with Qt 6.5 on Windows, we can disable a possible dark mode showing up via an environment variable
    qputenv("QT_QPA_PLATFORM","windows:darkmode=0");
#endif
#endif
}

//-----------------------------------------------------------------------------------
// and this one when Qt is up and running
//
// 2022-05-23 First version
// 2023-03-23 Change current directory to where the app is
//-----------------------------------------------------------------------------------
void TWUtils::helloQt(QWidget* pMainWindow /* == nullptr */)
{
// set organization name
    QApplication::setOrganizationName("Tungware");  // currently hardwired

// fix your widgets (if you have any?)
    if (nullptr != pMainWindow)
        fixMyWidgets(pMainWindow);

// and set current directory to where the .exe file is
// (useful mostly on Windows when the app was started via a shortcut)
    QDir::setCurrent(qApp->applicationDirPath());

// that's all for now
}

//----------------------------------------------------------------------------
// versionMajorMinor
//
// 2023-10-14 First version
//----------------------------------------------------------------------------
QString TWUtils::versionMajorMinor()
{
// try to retrieve the major and minor version(s)
    QString s = QApplication::applicationVersion();
    if (!s.contains("."))
        return "1.0";  // better default than ""

    QStringList sl = s.split(".");
    if (sl.count() < 2)
        return "";

    return sl[0] + "." + sl[1];
}

//----------------------------------------------------------------------------
// versionMajor (as an integer)
//
// 2024-01-05 First version
//----------------------------------------------------------------------------
int TWUtils::versionMajor()
{
// try to retrieve the major version no;
    QString s = QApplication::applicationVersion();
    if (!s.contains("."))
        return 1;   // better default than 0 :-)

// convert the digits before the "." to an int
    return s.split(".")[0].toInt();
}

//----------------------------------------------------------------------------
// buildDate
//
// 2023-10-19 First version
// 2023-11-06 Stomp bug (no support for dates with single digit days)
//----------------------------------------------------------------------------
QDate TWUtils::buildDate(QString sDate /* = __DATE__ */)
{
// for days 1 .. 9 toss that extra space
    sDate = sDate.simplified();

// use different strategies for Qt5 and Qt6
// for Qt5 we need to use QLocale
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    QDate dCompile = QLocale(QLocale::English).toDate(sDate,"MMM dd yyyy");
    if (!dCompile.isValid())
        dCompile = QLocale(QLocale::English).toDate(sDate,"MMM d yyyy");
#else
// for Qt6 it's easier
    QDate dCompile = QDate::fromString(sDate,"MMM dd yyyy");
    if (!dCompile.isValid())
        dCompile = QDate::fromString(sDate,"MMM d yyyy");
#endif
    return dCompile;
}

//------------------------------------------------------------------------------------
// getArgs
//
// 2023-02-13 First version
//------------------------------------------------------------------------------------
QStringList TWUtils::getArgs()
{
// small helper to retrieve the commandline argv chaps (useful for console apps)
    QCommandLineParser lp;
    lp.process(qApp->arguments());
    return lp.positionalArguments();
}

//----------------------------------------------------------------------------
// ignominiousExit
//
// 2022-08-23 First version
//----------------------------------------------------------------------------
void TWUtils::ignominiousExit(int nExitCode /* = EXIT_FAILURE */)
{
// we want to go out with no questions asked, use std::exit() unless we're on the Mac
#if defined(Q_OS_MACOS)
// std::exit() does not seem to work on the Mac, try std::abort() instead
    Q_UNUSED(nExitCode);
    std::abort();
#else
    exit(nExitCode);
#endif
}

//------------------------------------------------------------------------------------
// class for crashing with a message
// 2014-10-21 First version
// 2022-01-18 Also show program name and version
// 2023-07-26 Introduce flavors: vanilla and guru meditation
TWUtils::FubarObject::FubarObject(QString sFileName, int nLineNo, eFubarFlavors flavor)
{
// just store our 3 args, Qt will call us back via qFatalInvoker ---> twMessageHandler below
    sSourceFileName = sFileName.split(QDir::separator()).last();  // we're only interested in the filename part
    nSourceLineNo   = nLineNo;
    f               = flavor;
}

void TWUtils::FubarObject::qFatalInvoker(const char* msg, ...)
{
// build a QString crash message from a variable arg list
    va_list ap;
    va_start(ap, msg);
    auto sMsg = QString::vasprintf(msg, ap);
    va_end(ap);

// and crash using the other qFatalInvoker below
    qFatalInvoker(sMsg);
}

void TWUtils::FubarObject::qFatalInvoker(QString sMsg)
{
// check the flavor of this fubar
    if (eGuruMeditationFlavor == f)
    // for this flavor: add a nice text at the beginning of the crash message
        sMsg.prepend("Guru meditation: ");

// prefix with filename and lineno. and call qFatal
    QString s = QString("%1 %2 %3 line %4:\n%5").arg(
                        QApplication::applicationName(),QApplication::applicationVersion(),
                        sSourceFileName,QString::number(nSourceLineNo),sMsg);
    qFatal("%s",qUtf8Printable(s));
}


//-------------------------------------------------------------------
// custom Qt message filter (to save qFatal's messages to a logfile)
//
void TWUtils::twMessageHandler(QtMsgType type, const QMessageLogContext& context, const QString& msg)
{
    Q_UNUSED(context) // fugget about context (not available in release builds anyway)

// show the message via Qt's original message handler
    qtDefaultMessageHandler(type,context,msg);

// and we're done unless this is a qFatal() call (i.e. a fubar() or guruMeditation() call)
    if (QtFatalMsg != type)
        return;

// append this msg to our log file (will create the log file if this is the first time)
    auto f = new QFile("fubar.log");
    if (f->open(QIODevice::Append | QIODevice::Text))
    {
    // decorate the message with current date/time and a suffix
        QString sSuffix = QString(40,'-');
        QString s       = QString("%1:\n\'%2'\n%3\n").arg(toISODateTime(QDateTime::currentDateTime()),msg,sSuffix);

    // append it to the log file
        f->write(qUtf8Printable(s));
        f->close();
    }

// and show a dialog box
    errorBox(msg,QObject::tr("Allvarligt fel"));  // localization to Swedish

// see you on the other side
    ignominiousExit(EXIT_FAILURE);
}

//----------------------------------------------------------------------------
// getCurrentDirectory
//
// 2023-04-20 First version
//----------------------------------------------------------------------------
QString TWUtils::getCurrentDirectory()
{
    return QDir::currentPath();
}

//----------------------------------------------------------------------------
// getAppName
//
// 2024-10-29 First version
//----------------------------------------------------------------------------
QString TWUtils::getAppName()
{
    return QApplication::applicationName();
}

//----------------------------------------------------------------------------
// isShiftKeyDown: an alternative to Qt's qApp->keyboardModifiers()
// note: works only on Windows, others get the vanilla Qt function
//
// 2023-03-29 First version
//----------------------------------------------------------------------------
bool TWUtils::isShiftKeyDown()
{
#if defined(Q_OS_WIN)
    return (::GetAsyncKeyState(VK_SHIFT) & 0x8000);
#endif
    return (Qt::ShiftModifier == (Qt::ShiftModifier & qApp->keyboardModifiers()));
}

//----------------------------------------------------------------------------
// getDefaultFont (internal helper function)
//
// 2016-05-29 First version
//----------------------------------------------------------------------------
QFont getDefaultFont()
{
// cache helpers
    static QString sFontNameLoaded = "";
    static int     nFontSizeLoaded = 0;

// store the font here
    static QFont fntDefault;

// use a simple cache to minimize # of font loads
    if ((sFontNameLoaded != TWUtils::getDefaultFontName()) || (nFontSizeLoaded != TWUtils::getDefaultFontSize()))
    {
    // ok construct and load this font
        fntDefault = QFont(TWUtils::getDefaultFontName(),TWUtils::getDefaultFontSize());

    // and remember the new settings
        sFontNameLoaded = TWUtils::getDefaultFontName();
        nFontSizeLoaded = TWUtils::getDefaultFontSize();
    }

    return fntDefault;
}

//----------------------------------------------------------------------------
// getDefaultFontName
//
// 2016-05-29 First version
//----------------------------------------------------------------------------
QString TWUtils::getDefaultFontName()
{
    return QStringLiteral("Cambria");
}

//----------------------------------------------------------------------------
// getDefaultFontSize
//
// 2016-05-29 First version
//----------------------------------------------------------------------------
int TWUtils::getDefaultFontSize()
{
#if defined(Q_OS_MACOS)
    return 17;   // need bigger font sizes on the Mac
#endif
// for all others
    return 14;
}


//----------------------------------------------------------------------------
// WaitCursor simple helper class for showing an hourglass mouse cursor
//
// 2020-11-01 First version
// 2022-05-21 More sync() calls ---> less visual bugs
//----------------------------------------------------------------------------
TWUtils::WaitCursor::WaitCursor()
{
    qApp->sync();
    qApp->setOverrideCursor(Qt::WaitCursor);
    qApp->sync();
}

TWUtils::WaitCursor::~WaitCursor()
{
    qApp->sync();
    qApp->restoreOverrideCursor();
    qApp->sync();
}

//----------------------------------------------------------------------------
// noTitleBarNoButtons and titleBarNoButtons
//
// 2020-04-12 First version
//----------------------------------------------------------------------------
void TWUtils::noTitleBarNoButtons(QWidget* w)
{
#if defined(Q_OS_WIN)
// on Windows, we can add the dialog hint to get a nice border
    w->setWindowFlags(Qt::CustomizeWindowHint | Qt::MSWindowsFixedSizeDialogHint);
#else
// on other OS:es, try the Qt vanilla hint
    w->setWindowFlags(Qt::CustomizeWindowHint);
#endif
}

void TWUtils::titleBarNoButtons(QWidget* w)
{ // same as above but with a titlebar
    w->setWindowFlags(Qt::CustomizeWindowHint | Qt::WindowTitleHint);
}

//----------------------------------------------------------------------------
// truncateWithTrailingEllipses
//
// 2019-12-01 First version
//----------------------------------------------------------------------------
QString TWUtils::truncateWithTrailingEllipses(QWidget* pWidget, QString s)
{
    QFontMetrics m(pWidget->font());
    return m.elidedText(s,Qt::ElideRight,pWidget->width());
}

//----------------------------------------------------------------------------
// makeTopWindow
//
// 2019-02-05 First version
// 2019-02-10 For Windows, toss in some Win32 native calls as well
//----------------------------------------------------------------------------
void TWUtils::makeTopWindow(QWidget* w)
{
// first try raising it using normal Qt stuff
    w->activateWindow();
    w->raise();

#if defined(Q_OS_WIN)
// if we're on Windows work harder, not smarter
    HWND hWnd = reinterpret_cast<HWND>(w->winId());
    ::SetForegroundWindow(hWnd);
    ::SetWindowPos(hWnd,HWND_TOP,0,0,0,0,SWP_SHOWWINDOW | SWP_NOMOVE | SWP_NOSIZE);
#endif
}

//----------------------------------------------------------------------------
// yesNoBox
//
// 2015-07-24 First version
//----------------------------------------------------------------------------
bool TWUtils::yesNoBox(QString sText,QString sTitle /* = "" */)
{
// setup a messagebox with the question
    QMessageBox* pMsgBox = new QMessageBox(qApp->activeWindow());
    pMsgBox->setIcon(QMessageBox::Question);

    if (!sTitle.isEmpty())   // title specified?
        pMsgBox->setWindowTitle(sTitle);
    pMsgBox->setFont(getDefaultFont());
    pMsgBox->setText(sText);
    pMsgBox->setStandardButtons(QMessageBox::Yes | QMessageBox::No);
    pMsgBox->setDefaultButton(QMessageBox::No);

// localization to Swedish
    pMsgBox->button(QMessageBox::Yes)->setText(QObject::tr("Ja") );
    pMsgBox->button(QMessageBox::No) ->setText(QObject::tr("Nej"));

// try to make sure it's visible
    TWUtils::makeTopWindow(pMsgBox);

// show the question box
// note: no timeout support for this box flavor, since we're asking a question
    return (QMessageBox::Yes == pMsgBox->exec());
}

//----------------------------------------------------------------------------
// okCancelBox
//
// 2015-07-24 First version
//----------------------------------------------------------------------------
bool TWUtils::okCancelBox(QString sText, QString sTitle /* = "" */)
{
// setup a messagebox with the question
    QMessageBox* pMsgBox = new QMessageBox(qApp->activeWindow());
    pMsgBox->setIcon(QMessageBox::Question);

    if (!sTitle.isEmpty())   // title specified?
        pMsgBox->setWindowTitle(sTitle);
    pMsgBox->setFont(getDefaultFont());
    pMsgBox->setText(sText);
    pMsgBox->setStandardButtons(QMessageBox::Ok | QMessageBox::Cancel);
    pMsgBox->setDefaultButton(QMessageBox::Cancel);

// localization to Swedish
    pMsgBox->button(QMessageBox::Ok)    ->setText(QObject::tr("Ok")    );
    pMsgBox->button(QMessageBox::Cancel)->setText(QObject::tr("Avbryt"));

// try to make sure it's visible
    TWUtils::makeTopWindow(pMsgBox);

// show the question box
// note: no timeout support for this box flavor (we're asking a question)
    return (QMessageBox::Ok == pMsgBox->exec());
}

// -----------------------------------------------------------------------------
// common helper for the different boxes below that all have just an "Ok" button
void commonOkBox(QString sText, QString sTitle,int msecsTimeout,QMessageBox::Icon eIcon)
{
// new up a messagebox with a single "Ok" button and the specified icon flavor
    QMessageBox* pMsgBox = new QMessageBox(qApp->activeWindow());
    pMsgBox->setIcon(eIcon);

    if (!sTitle.isEmpty())   // title specified?
        pMsgBox->setWindowTitle(sTitle);
    pMsgBox->setFont(getDefaultFont());
    pMsgBox->setText(sText);
    pMsgBox->setStandardButtons(QMessageBox::Ok);
    pMsgBox->setDefaultButton(QMessageBox::Ok);

// localization to Swedish
    pMsgBox->button(QMessageBox::Ok)->setText(QObject::tr("Ok"));

// try to make sure it's visible
    TWUtils::makeTopWindow(pMsgBox);

// timeout requested?
    if (msecsTimeout > 0)
        QTimer::singleShot(msecsTimeout,pMsgBox,[pMsgBox]{ pMsgBox->accept(); });

// show thebox, wait for user (or perhaps a timeout) to dismiss it
    pMsgBox->exec();
}

//----------------------------------------------------------------------------------------
// infoBox
//
// 2015-07-30 First version
// 2016-11-21 Don't set any icon on the Mac (bug: info icon = exclamation icon)
// 2019-01-30 Introduce an optional timeout param
// 2022-02-21 Use the common box helper
//----------------------------------------------------------------------------------------
void TWUtils::infoBox(QString sText, QString sTitle /* = "" */, int msecsTimeout /* =0 */)
{
// use our common box helper (for those of us who only require a single "Ok" button)
    commonOkBox(sText,sTitle,msecsTimeout,QMessageBox::Information);
}

//----------------------------------------------------------------------------------------
// warningBox
//
// 2022-02-21 First version
//----------------------------------------------------------------------------------------
void TWUtils::warningBox(QString sText, QString sTitle /* = "" */, int msecsTimeout /* =0 */)
{
// use our common box helper (for those of us who only require a single "Ok" button)
    commonOkBox(sText,sTitle,msecsTimeout,QMessageBox::Warning);
}

//----------------------------------------------------------------------------
// errorBox
//
// 2015-07-30 First version
// 2019-01-30 Introduce an optional timeout param
//----------------------------------------------------------------------------
void TWUtils::errorBox(QString sText, QString sTitle /* = "" */, int msecsTimeout /* =0 */)
{
// use our common box helper (for those of us who only require a single "Ok" button)
    commonOkBox(sText,sTitle,msecsTimeout,QMessageBox::Critical);
}

//----------------------------------------------------------------------------------------
// twinkleBox
//
// 2019-02-01 First version
// 2019-02-02 Set standard buttons to 0 to create a message box lacking buttons
// 2021-10-03 Renamed from messageBox to twinkleBox
//----------------------------------------------------------------------------------------
void TWUtils::twinkleBox(QString sText, QString sTitle /* = "" */, int msecsTimeout /* =0 */)
{
// add white space? yes do it for single line messages (since this box will be devoid of icons)
    if (!sText.contains("\n"))
        sText = "\n" + sText + "    \n";

// new up a messagebox with an information message but *no* icons or buttons
    QMessageBox* pMsgBox = new QMessageBox(qApp->activeWindow());

    if (!sTitle.isEmpty())   // title specified?
        pMsgBox->setWindowTitle(sTitle);
    pMsgBox->setFont(getDefaultFont());
    pMsgBox->setText(sText);
    pMsgBox->setStandardButtons(QMessageBox::NoButton);

// try to make sure it's visible
    TWUtils::makeTopWindow(pMsgBox);

// make sure we have a timeout for this box (since there's no other way dismiss it!)
    if (msecsTimeout < 1)
        msecsTimeout = nnRubberneckDelay;   // use our standard "tourist" delay

// bombs away
    QTimer::singleShot(msecsTimeout,pMsgBox,[pMsgBox]{ pMsgBox->accept(); });

// show the info box, wait for the timeout to dismiss it
    pMsgBox->exec();
}

#if defined(USE_PASSWORDBOX)
//----------------------------------------------------------------------------
// passwordBox
// if user clicks Ok:     returns name and password in a QStringList
// if user clicks Cancel: returns "","" (in a QStringList, you guessed it)
//
// 2018-07-10 First version
// 2018-08-15 Add some margin/spacing for better visibility
//----------------------------------------------------------------------------
QStringList TWUtils::passwordBox(QString sName,QString sTitle /* = "" */)
{
// introducing a modal QDialog-derived class for a nice name/password box
    class NamePasswordDialog : public QDialog
    {
    public:
        QLabel*           labelName;
        QLineEdit*        editName;
        QLabel*           labelPassword;
        QLineEdit*        editPassword;
        QDialogButtonBox* buttonBox;

        NamePasswordDialog(QWidget* parent, QString sName, QString sTitle) : QDialog(parent)
        {
        // standard preamble
            setFont(getDefaultFont());
            if (!sTitle.isEmpty())  // title specified?
                setWindowTitle(sTitle);

        // two QLineEdits with all the trimmings for here
            labelName     = new QLabel(tr("&Namn:    "),this);  // Swedish local.
            editName      = new QLineEdit(sName        ,this);
            labelName->setBuddy(editName);

            labelPassword = new QLabel(tr("&Lösenord:"),this);  // Swedish local.
            editPassword  = new QLineEdit("",this);
            editPassword->setEchoMode(QLineEdit::Password);
            labelPassword->setBuddy(editPassword);

        // use the same 2 button layout as in okCancelBox above (Ok left, Cancel to the right)
            buttonBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel,this);
            buttonBox->button(QDialogButtonBox::Ok    )->setText(tr("Ok"    )); // Swedish localization
            buttonBox->button(QDialogButtonBox::Cancel)->setText(tr("Avbryt"));

            QGridLayout* formLayout = new QGridLayout(this);
            int nMarginSpacing = getDefaultFontSize() - 1;      // add some padding to the layout

            formLayout->setSpacing(nMarginSpacing);
            formLayout->setContentsMargins(nMarginSpacing,nMarginSpacing,nMarginSpacing,nMarginSpacing);
            formLayout->addWidget(labelName    ,0,0);
            formLayout->addWidget(editName     ,0,1);
            formLayout->addWidget(labelPassword,1,0);
            formLayout->addWidget(editPassword ,1,1);

            formLayout->addWidget(buttonBox,2,0,1,-1); // let the 2 buttons span both columns
            setLayout(formLayout);

        // if name was specified, switch the keyboard focus to the password lineedit
            if (!sName.isEmpty())
                editPassword->setFocus();

        // wire up the buttons to their matching QDialog slots
            connect(buttonBox, &QDialogButtonBox::accepted, this, &QDialog::accept);
            connect(buttonBox, &QDialogButtonBox::rejected, this, &QDialog::reject);
        }
    };

// ok fire it up, centered on the appwindow
// unless we're a console app (i.e. no active window) then center on the desktop
    auto np = new NamePasswordDialog(qApp->activeWindow(),sName,sTitle);

// show and wait (use exec() to make it modal, same as the other boxes above)
    if (QDialog::Accepted == np->exec())
    // user clicked ok, grab the strings
        return QStringList({np->editName->text(),np->editPassword->text()});

// no, clicked Cancel, return with an empty list
    return QStringList({"",""});
}
#endif

//--------------------------------------------------------------------------------------------
// Qt quirk fixing/improvements for your widgets:
//
// 1) apply Fusion style, if we're on Windows exclude/revert some widgets back to Vista style
// 2) apply some fixes for QTableWidgets:
//    Allow small column widths by setting minimum section size (see QTBUG-68503)
//    Disable QComboBox in a QTableWidget clobbering adjacent columns (see QTBUG-69563)
//    note: *only* applies to QComboBoxes inserted in QTableWidgets (normal ones are fine)
// 3) for Mac and Windows: fix font propagation for QWidgets (parent -> kid)
// 4) for the Mac: make fonts 33% bigger (fix for well known "small Mac fonts" Qt syndrome)
// 5) 50% wider QScrollbars (from 16 pixels to 24) + increase contrast/visibility of the handle
//--------------------------------------------------------------------------------------------
void TWUtils::fixMyWidgets(QWidget* pTopWidget)
{
// QStyle we use
    static const QString ssFusionStyle = QStringLiteral("fusion");

// get our current app style, is it Fusion already?
    auto stCurrent = qApp->style();

    if (ssFusionStyle.compare(stCurrent->objectName(),Qt::CaseInsensitive))
    {
    // no, so switch global/app style to Fusion now (qApp takes ownership of our QStyle)
        auto stFusion = QStyleFactory::create(ssFusionStyle);

        if (nullptr == stFusion)
            fubar("couldn't switch to Fusion style, so sorry");

        qApp->setStyle(stFusion);
    }

#if defined(Q_OS_WIN)
// ---------------------------------------------------------------------------------------------------
// we've switched to Fusion now but some widgets look positively ugly on the Windows Fusion flavor
// try to improve those widgets by selectively reverting them back to the (original) Vistastyle
// (note: this requires that the plugin file "styles\qwindowsvistastyle.dll" is present)
// need only a single static style chap here (since widgets->setStyle() doesn't take ownership)
//
// 2018-07-19 Removed QRadioButton from the fix list (amazingly, they look just fine in Fusion)
// 2018-08-14 Added QSliders to the fix list (the Fusion flavored ones look ugly, e.g. tiny grips)
// ---------------------------------------------------------------------------------------------------
    static const QString ssVistaStyle          = "windowsvista";
    static const QStringList slWidgetsForVista = { "QComboBox", "QGroupBox", "QSlider" };

    static QStyle* stVista = nullptr;
    if (nullptr == stVista)
    // first time here? create the vista style style factory instance
        stVista = QStyleFactory::create(ssVistaStyle);

    if (nullptr == stVista)
        fubar("couldn't switch to Vista style, verify that the styles\\qwindowsvistastyle.dll is present");

// check the top widget first
    if (slWidgetsForVista.contains(pTopWidget->metaObject()->className()))
        pTopWidget->setStyle(stVista);

// and then the children
    for (auto pKid : pTopWidget->findChildren<QWidget*>())
        if (slWidgetsForVista.contains(pKid->metaObject()->className()))
            pKid->setStyle(stVista);
#endif

// more classes that need some betterment
    static const QString ssTableWidget = QStringLiteral("QTableWidget");
    static const QString ssComboBox    = QStringLiteral("QComboBox");

    auto betterTableWidgets = [](QTableWidget* tw)
    {
    // apply fix for QTBUG-68503 (see above)
        tw->horizontalHeader()->setMinimumSectionSize(1);

    // apply fix for QTBUG-69563) (see above)
    // only look at the kids because we only want QComboBoxes *inside* QTableWidgets
        for (auto pKid : tw->findChildren<QComboBox*>())
            if (ssComboBox == pKid->metaObject()->className())
                static_cast<QComboBox*>(pKid)->setMinimumWidth(1);
    };

// step thru top chap and the kids, look for QTableWidgets to fix
    if (ssTableWidget == pTopWidget->metaObject()->className())
        betterTableWidgets(static_cast<QTableWidget*>(pTopWidget));

    for (auto pKid : pTopWidget->findChildren<QTableWidget*>())
        if (ssTableWidget == pKid->metaObject()->className())
            betterTableWidgets(static_cast<QTableWidget*>(pKid));

#if !defined(Q_OS_LINUX)
// this is Qt quirk/bug/feature that doesn't exist on Linux but exists on MacOS and Windows:
// some widgets disregard propagation of font settings in ui_mainwindow.h, however if we do it again here it'll work
// (this is for the benefit of those widgets that *do not* set their own font props, i.e. relies on inherited props from parents)
    auto f = pTopWidget->font();
    pTopWidget->setFont(getDefaultFont()); // momentarily switch to another font (does not have to exist)
    pTopWidget->setFont(f);                // so this will be more likely to propagate to the kids
#endif

#if defined(Q_OS_MACOS)
// on the Mac we want to increase widget font sizes
    for (auto pKid : pTopWidget->findChildren<QWidget*>())
    {
    // do it only on our own (named) widgets
        QString sName = pKid->objectName();
        if (sName.isEmpty())
            continue;
        if (sName.startsWith("qt_"))
            continue;

        auto f = pKid->font();
        f.setPointSize((f.pointSize() * 1350) / 1000);  // Honey I blew up the kids/widgets
        pKid->setFont(f);
    }
#endif

// these are not your grandfather's scrollbars: set them more than 50% wider than Qt default (which is from the previous century)
    static const QString ssScrollBar = QStringLiteral("QScrollBar");

    auto betterScrollBars = [] (QScrollBar* sb)
    {
        static const QColor cBackground("#f3f1f0");
        static const QColor cHandle    ("#cccac8");

    // apply different stylesheets for horizontal or vertical scrollbars
        if (sb->orientation() == Qt::Horizontal)
            sb->setStyleSheet(QString("QScrollBar:horizontal { background: %1; height: 24px } "
                                      "QScrollBar:handle:horizontal { background: %2; min-width: 24px; "
                                      "margin: 0 24px 0 24px; border: 1px solid grey; }").arg(cBackground.name(),cHandle.name()));
        else
            sb->setStyleSheet(QString("QScrollBar:vertical { background: %1; width: 24px; } "
                                      "QScrollBar:handle:vertical { background: %2; min-height: 24px; "
                                      "margin: 24px 0 24px 0; border: 1px solid grey; }").arg(cBackground.name(),cHandle.name()));
    };

    if (ssScrollBar == pTopWidget->metaObject()->className())   // check the parent first
        betterScrollBars(static_cast<QScrollBar*>(pTopWidget));

    for (auto pKid : pTopWidget->findChildren<QWidget*>())      // then the kids
        if (ssScrollBar == pKid->metaObject()->className())
            betterScrollBars(static_cast<QScrollBar*>(pKid));

// that's all the Qt/quirk fixing we need right now
}

//----------------------------------------------------------------------------
// fixedSizeWindow
//
// 2019-10-25 First version
// 2021-09-28 Do a dummy resize() to support HighDPI displays
//----------------------------------------------------------------------------
void TWUtils::fixedSizeWindow(QWidget* pMainWindow)
{
    auto s = pMainWindow->size();
    pMainWindow->setMaximumSize(s);
    pMainWindow->setMinimumSize(s);

// schedule a dummy resize (helps establish the correct size of highDPI displays)
    QTimer::singleShot(nnNormalDelay,pMainWindow,[pMainWindow] { pMainWindow->resize(pMainWindow->size()); });
}

//----------------------------------------------------------------------------
// stylesheet helpers
//
// 2019-12-07 First version
//----------------------------------------------------------------------------
QString TWUtils::backgroundStyleMainWindow(QColor c)
{
    return QString("QMainWindow: { background: %1; }").arg(c.name());
}

QString TWUtils::foregroundStyle(QColor c)
{
    return QString("color: %1;").arg(c.name());
}

QString TWUtils::backgroundStyle(QColor c)
{
    return QString("background-color: %1;").arg(c.name());
}

QString TWUtils::alternativeBackgroundStyle(QColor c)
{
    return QString("alternate-background-color: %1;").arg(c.name());
}

QString TWUtils::selectedBackgroundStyle(QColor c)
{
    return QString("selection-background-color: %1;").arg(c.name());
}


//-------------------------------------------------------------------
// welcome to the date and time utility functions
QString TWUtils::toISODate(QDate d)
{
    return d.toString("yyyy-MM-dd");
}

QString TWUtils::toYMD(QDate d)
{
    return d.toString("yyyyMMdd");
}

QString TWUtils::toISODate(QDateTime dt)
{
    return dt.toString("yyyy-MM-dd");
}

QString TWUtils::toYMD(QDateTime dt)
{
    return dt.toString("yyyyMMdd");
}


QString TWUtils::toISOTime(QTime t)
{
    return t.toString("HH:mm:ss");
}

QString TWUtils::toISOTime(QDateTime dt)
{
    return dt.toString("HH:mm:ss");
}

QString TWUtils::toISOTimeNoSeconds(QTime t)
{
    return t.toString("HH:mm");
}

QString TWUtils::toHHMM(QTime t)
{
    return toISOTimeNoSeconds(t);
}

QString TWUtils::toISOTimeNoSeconds(QDateTime dt)
{
    return dt.toString("HH:mm");
}

QString TWUtils::toHHMM(QDateTime dt)
{
    return toISOTimeNoSeconds(dt);
}


QString TWUtils::toISODateTime(QDateTime dt)
{
    return QString("%1 %2").arg(toISODate(dt),toISOTime(dt));
}

QString TWUtils::toISODateTime(QDate d, QTime t)
{
    return QString("%1 %2").arg(toISODate(d),toISOTime(t));
}

QString TWUtils::toISODateTimeNoSeconds(QDateTime dt)
{
    return QString("%1 %2").arg(toISODate(dt),toISOTimeNoSeconds(dt));
}

QString TWUtils::toISODateTimeNoSeconds(QDate d,QTime t)
{
    return QString("%1 %2").arg(toISODate(d),toISOTimeNoSeconds(t));
}


QDate TWUtils::fromISODate(QString s)
{
    return QDate::fromString(s,"yyyy-MM-dd");
}

QDate TWUtils::fromYMD(QString s)
{
    return QDate::fromString(s,"yyyyMMdd");
}

QDateTime TWUtils::fromISODateTime(QString s)
{
    return QDateTime::fromString(s,"yyyy-MM-dd HH:mm:ss");
}

QDateTime TWUtils::fromISODateTimeNoSeconds(QString s)
{
    return QDateTime::fromString(s,"yyyy-MM-dd HH:mm");
}

QTime TWUtils::fromISOTimeNoSeconds(QString s)
{
    return QTime::fromString(s,"HH:mm");
}

QTime TWUtils::fromHHMM(QString s)
{
    return fromISOTimeNoSeconds(s);
}


//------------------------------------------------------------------------------------------
// misc. QString functions
// commaList2StringList converts a QString with comma separated values into a QStringList
// 2022-02-08 fix bug: empty string returned a stringlist with 1 element (one empty string)
QStringList TWUtils::commaList2StringList(QString s)
{
    if (s.trimmed().isEmpty())
    // empty (or just whitespace) strings should return an empty (0 element) QStringList
        return QStringList();

    return s.split(",");
}

// stringList2CommaList converts a QStringList into a comma separated QString
QString TWUtils::stringList2CommaList(QStringList sl)
{
    if (sl.contains(","))
        qWarning("Warning: there's an unexpected comma hiding in your QStringList");

    return sl.join(",");
}

// ---------------------------------------------------------
// isEqualWildcards: supports '?' and '*" in both arguments
// note: a '?' or '*' does not match empty string/positions
// 2021-12-06 Do the comparison case insensitive
bool TWUtils::isEqualWildcards(QString a, QString b)
{
// make them both lowercase so we can make the comparison case insensitive
    a = a.toLower();
    b = b.toLower();

// establish a common length
    int nCommonLength = qMax(a.length(),b.length());

// check that any * is at the rightmost position and replace them with '?'
    if (a.indexOf('*') >= 0)
    {
    // make sure we have only one '*' and it's at the rightmost position
        if (a.indexOf('*') != (a.length() - 1))
            fubar("isEqualWildcards(): 1st string has a '*' but it's not at the rightmost position");

    // toss it
        a.chop(1);

    // replace with one or more '?'
        a += QString(nCommonLength - a.length(),'?');
    }

    if (b.indexOf('*') >= 0)
    {
    // make sure we have only one '*' and it's at the rightmost position
        if (b.indexOf('*') != (b.length() - 1))
            fubar("isEqualWildcards(): 2nd string has a '*' but it's not at the rightmost position");

    // toss it
        b.chop(1);

    // replace with one or more '?'
        b += QString(nCommonLength - b.length(),'?');
    }

// if a and b are not of equal length then they're obviously not equal
    if (a.length() != b.length())
        return false;

// ok step thru
    for (int i = 0; (i < a.length()); ++i)
    {
        QChar ca = a[i];
        QChar cb = b[i];

        if (('?' != ca) && ('?' != cb))
            if (ca != cb)
                return false;
    }

// if we made it this far, consider the strings to be equal
    return true;
}

// returns your string "PascalCased" ------------------------------------------------------
// 2023-03-12 Don't uppercase after a colon (e.g. "S:t Eriksgatan" not "S:T Eriksgatan")
QString TWUtils::pascalCase(QString s)
{
    QString sResult;
    bool bPrevIsLetterOrColon = false;    // start with uppper case

    for (auto c : s)
    {
        if (c.isLetter())
        // what about previous char? if that's a letter or a colon set this one to lowercase
            sResult += (bPrevIsLetterOrColon) ? c.toLower() : c.toUpper();
        else
        // not a letter ---> pass it through
            sResult += c;

        bPrevIsLetterOrColon = (c.isLetter() || (':' == c));
    }

    return sResult;
}

QString TWUtils::pascalCaseIfAllUpper(QString s)
{
    if (s != s.toUpper())
    // not all chars are upper case, return the untouched string
        return s;

    return pascalCase(s);
}

// for nondigits tossing ----------------------------------------------------------------
QString TWUtils::tossAllButDigits(QString s)
{
    QString sResult;
    for (auto c : s)
        if (c.isDigit())
            sResult += c;

    return sResult;
}

// tosses all leading spaces ------------------------------------------------------------
QString TWUtils::tossLeadingSpaces(QString s)
{
    QString sResult;
    bool bToss = true;
    for (auto c : s)
    {
        if (bToss)
        {
            if (' ' == c)
                continue;

            bToss = false;
        }

        sResult += c;
    }

    return sResult;
}

// tosses all white space ---------------------------------------------------------------
QString TWUtils::tossAllWhiteSpace(QString s)
{
    QString sResult = s.simplified();
    return sResult.remove(" ");
}

// trim string using ellipses -----------------------------------------------------------
QString TWUtils::truncateWithTrailingEllipses(QString s, int nMaxLength)
{
// is string length within the specified limit?
    if (s.length() <= nMaxLength)
        return s;

// we need some truncation
    QString sEllipses = "...";
    int nELength      = sEllipses.length();

// room for some ellipses?
    if (nMaxLength <= nELength)
    // no just chip away
        return s.left(nMaxLength);

// truncate + append ellipses
    return s.left(nMaxLength - nELength) + sEllipses;
}

//---------------------------------------------------------------------------------------
// get Swedish month and weekday names
QString TWUtils::getSwedishMonthName(int nMonthNo)          // January = 1 ... December = 12 ...
{
    if ((nMonthNo < 1) || (nMonthNo > 12))
        fubar("bad nMonthNo (%d)",nMonthNo);

    return commaList2StringList("dummy,januari,februari,mars,april,maj,juni,juli,augusti,september,oktober,november,december").at(nMonthNo);
}

QString TWUtils::getSwedishShortMonthName(int nMonthNo)     // January = 1 ... December = 12 ...
{
    QString sMonth = getSwedishMonthName(nMonthNo);
    if (sMonth.length() < 5)
        return sMonth;

    return sMonth.left(3) + ".";
}

QString TWUtils::getSwedishWeekdayName(int nWeekdayNo)      // monday = 1 ... sunday = 7
{
    if ((nWeekdayNo < 1) || (nWeekdayNo > 7))
        fubar("bad nWeekdayNo (%d)",nWeekdayNo);

    return commaList2StringList("dummy,måndag,tisdag,onsdag,torsdag,fredag,lördag,söndag").at(nWeekdayNo);
}

// same same as above but with the first letter in the weekday Uppercased
QString TWUtils::getSwedishWeekdayNameU1(int nWeekdayNo)    // Monday = 1 ... Sunday = 7
{
    return pascalCase(getSwedishWeekdayName(nWeekdayNo));
}


//---------------------------------------------------------------------
// ageInYears returns the age on a specified target date
// (if you want the current age use today as the target date)
//---------------------------------------------------------------------
int TWUtils::ageInYears(QDate dDOB, QDate dTarget)
{
// calculate distance in years between the birthdate and the target date
    int nAgeInYears = dTarget.year() - dDOB.year();

// has celebrated birthday in the year of the target date?
// if not, subtract 1 year from the age (note: this also works for newborns, i.e. age 0)
    QDate dBirthDay = QDate(dTarget.year(),dDOB.month(),dDOB.day());
    if (dBirthDay > dTarget)
    // has not celebrated birthday yet that year
        --nAgeInYears;

// ok we're done
    return nAgeInYears;
}

//--------------------------------------------------------------------
// pretty print the personNo, expecting YYYYMMDDNNNN as input
// returns CC YYMMDD-NNNN (note: no check for correct Luhn digit)
//
// 2016-05-03 First version
// 2022-02-04 Always return "-" as separator ("+" seems tp be obsolete)
// 2022-02-24 Return the century digits (i.e. we return 14 chars)
//--------------------------------------------------------------------
QString TWUtils::prettyPrintPersonNo(QString s12Digits)
{
// exactly 12 digits input, please (toss any junk/non-digits)
    QString s = tossAllButDigits(s12Digits);

// and if we haven't got exactly 12 digits, skip this
    if (12 != s.length())
    // just return the original string
        return s12Digits;

// check the age, betwen 0 and 199 years is fine (use today as the target date)
    int nAge = ageInYears(fromYMD(s12Digits.left(8)),QDate::currentDate());

    if ((nAge < 0) || (nAge > 199))
    // else return the original string
        return s12Digits;

// return the pretty print/14 characters version
    return s.left(2) + " " + s.mid(2,6) + "-" + s.right(4);
}

//-------------------------------------------------------------------------------
// getLuhnCheckDigit returns the check digit for a string of numbers
// Uses the Luhn checksum (see Wikipedia for the Luhn algorithm)
//
// 2020-02-23 First version
// 2021-07-19 Renamed, expect a string without any existing trailing check digit
//-------------------------------------------------------------------------------
int TWUtils::getLuhnCheckDigit(QString sDigits)
{
    QString s = TWUtils::tossAllButDigits(sDigits); // remove possible non-digits

// append a "0" as a check digit place holder
    s += "0";

// calc the Luhn sum
    int anOddPos[] = {0,1,2,3,4,6,7,8,9,0};
    int sumLuhn    = 0;
    for (int c = s.length() - 1; (c >= 0); --c)
    {
        int i    = s.mid(c,1).toInt();
        sumLuhn += i;

        if ((c % 2) == (s.length() % 2))
        // add again for every 2nd position (starting with 2nd from the right)
            sumLuhn += anOddPos[i];
    }

// return the check digit (0 .. 9)
    return (10 - (sumLuhn % 10)) % 10;
}

//----------------------------------------------------------------------------
// isLuhnCheckDigitOk
//
// 2021-07-20 First version
//----------------------------------------------------------------------------
bool TWUtils::isLuhnCheckDigitOk(QString sDigits)
{
// expect the check digit to be the last digit
    QString s = tossAllButDigits(sDigits);
    if (s.length() < 2)
    // must have a least 1 digit and 1 check digit
        return false;

    return (s.right(1).toInt() == getLuhnCheckDigit(s.chopped(1)));
}

//----------------------------------------------------------------------------
// isReservNo
//
// 2023-03-02 First version (only supports Region Stockholm style)
//----------------------------------------------------------------------------
bool TWUtils::isReservNo(QString sPersonNo)
{
// toss all non digits
    QString s = tossAllButDigits(sPersonNo);

// reservnr should contain 12 digits
    if (12 != s.length())
        return false;

// do we have a Stockholm-flavored reservnr?
    return ("99" == s.left(2));
}

//------------------------------------------------------------------------------
// checkPersonNo
// check validity, return a Swedish error message or "" if the number seems fine
//
// 2016-05-03 First version
// 2020-02-28 Support reservnr Region Stockholm flavor ("99" as century-digits)
//------------------------------------------------------------------------------
QString TWUtils::checkPersonNo(QString sPersonNo)
{
// exactly 12 digits input, please (toss any junk/non-digits)
    QString s = tossAllButDigits(sPersonNo);

// expect exactly 12 digits
    if (s.length() != 12)
        return QObject::tr("Personnumret är inte 12 siffror.");

// if this is not a reservno, do some more checks
    if (!isReservNo(s))
    {
    // check the century digits (as an added feature support the 22nd century)
        QString sCenturyDigits = s.left(2);
        if ((sCenturyDigits != "18") && (sCenturyDigits != "19") &&
            (sCenturyDigits != "20") && (sCenturyDigits != "21"))
            return QObject::tr("Personnumret har ej korrekta sekelsiffror.");

    // check for bad digit combos
        if (s.mid(2).contains("0000"))  // leave the century digits alone
            return QObject::tr("Personnumret har felaktiga siffror (innehåller 0000).");
    }

// check the check digit (the last digit)
    if (!isLuhnCheckDigitOk(s.mid(2)))  // exclude the century digits from the checking
        return QObject::tr("Personnumret har felaktig checksiffra.");

// ok everything looks fine and dandy for this personno
    return "";
}


//-----------------------------------------------------------------------------------
// file/textstream helpers  note: these chaps will not fubar on failed opens
//                          you can use isOpen() to see if the ctor succeeded or not
TWTextFileReader::TWTextFileReader()
{
    f = nullptr;
}

TWTextFileReader::TWTextFileReader(QString sFileName, bool bUtf8 /* = true */)
{
    open(sFileName,bUtf8);
}

// close and delete our file object
TWTextFileReader::~TWTextFileReader()
{
    close();
    delete f;
}

void TWTextFileReader::open(QString sFileName, bool bUtf8 /* = true */)
{
// construct and try to open the file
    f = new QFile(sFileName);
    if (!f->open(QIODevice::ReadOnly | QIODevice::Text))
        return; // no dice no file opened

// and prepare the textstream
    ts.setDevice(f);

// also set the text encoding (different flavors for Qt5 and Qt6)
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    ts.setCodec(bUtf8 ? "UTF-8" : "ISO 8859-1");
#else
    ts.setEncoding(bUtf8 ? QStringConverter::Utf8 : QStringConverter::Latin1);
#endif
}

bool TWTextFileReader::isOpen()
{
    if (nullptr == f)
        return false;

    return f->isOpen();
}

QStringList TWTextFileReader::readAll()
{
    QStringList sl;

// if file is open then our textstream should be up and running as well
    if (isOpen())
        while (!ts.atEnd())
            sl.append(ts.readLine());

// that's all
    return sl;
}

void TWTextFileReader::close()
{
    if (!isOpen())
        return;    // nothing needs to be done

    ts.flush();
    f->close();
}

bool TWTextFileReader::exists(QString sFileName)
{
    return QFile::exists(sFileName);
}

// static helper function
QStringList TWTextFileReader::readAll(QString sFileName, bool bUtf8)
{
    TWTextFileReader fr(sFileName,bUtf8);
    return fr.readAll();
}


// --- TWTextFileWriter
TWTextFileWriter::TWTextFileWriter()
{
    f = nullptr;
}

TWTextFileWriter::TWTextFileWriter(QString sFileName, bool bUtf8 /* = true */)
{
// construct and try to open the file
    open(sFileName,bUtf8);
}

// close up (delete our file object as well)
TWTextFileWriter::~TWTextFileWriter()
{
    close();
    delete f;
}

void TWTextFileWriter::open(QString sFileName, bool bUtf8 /* = true */)
{
// construct and try to open the file
    f = new QFile(sFileName);
    if (!f->open(QIODevice::WriteOnly | QIODevice::Text | QIODevice::Truncate))
        return; // no dice no file opened so solly

// and prepare the textstream
    ts.setDevice(f);

// also set the text encoding (different flavors for Qt5 and Qt6)
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
    ts.setCodec(bUtf8 ? "UTF-8" : "ISO 8859-1");
#else
    ts.setEncoding(bUtf8 ? QStringConverter::Utf8 : QStringConverter::Latin1);
#endif
}

bool TWTextFileWriter::isOpen()
{
    if (nullptr == f)
        return false;

    return f->isOpen();
}

void TWTextFileWriter::writeStringList(QStringList sl)
{
// if file is open then our textstream should be up and running as well
    if (isOpen())
        for (auto s : sl)
            ts << s << "\n";
}

void TWTextFileWriter::close()
{
    if (!isOpen())
        return;    // nothing to do

    ts.flush();
    f->close();
}

// the static chaps
void TWTextFileWriter::writeAll(QString sFileName, QStringList sl, bool bUtf8 /* = true */)
{
    TWTextFileWriter fw(sFileName,bUtf8);
    fw.writeStringList(sl);
    fw.close();
}

void TWTextFileWriter::writeBinaryFile(QString sFileName, QByteArray ba)
{
// construct and try to open the file
    QFile f(sFileName);
    if (!f.open(QIODevice::WriteOnly | QIODevice::Truncate))  // look ma: no QIODevice::Text
        return; // no dice no file opened so solly

    f.write(ba);
    f.close();
}

bool TWTextFileWriter::exists(QString sFileName)
{
    return QFile::exists(sFileName);
}

bool TWTextFileWriter::canCreateFile(QString sFileName)
{
// try open the file and write something
    QStringList sl = { "This is a test file", "with a andom number:" };
    sl.append(QString::number(rand()));  // add a random # to flush possible caches
    TWTextFileWriter::writeAll(sFileName,sl);

// read back the same stuff?
    bool bOk = (TWTextFileReader::readAll(sFileName) == sl);

// regardless, erase the file
    QFile::remove(sFileName);

// and return the result
    return bOk;
}

//-----------------------------------------------------------------------------
// App persistent settings class helper
// 2020-07-14 First version
// 2022-12-04 Introduce an enum for the location of the .ini file
// 2023-10-06 Add readStringListEncrypted()

TWAppSettings::TWAppSettings(IniLocation eLocation /* = eNextToApp */)
{
// set the flavor
    eIL = eLocation;

// not on Windows? transmogrify eAppData and eRegistry into eNextToApp flavor
#if !defined(Q_OS_WIN)
    if ((eAppData == eIL) || (eRegistry == eIL))
        eIL = eNextToApp;
#endif

// initialize our other chaps
    pSettings       = nullptr;
    bReadOnly       = false;
    sCurrentSection = "";

// defer to invoke a QSettings constructor until first call to setCurrentSection()
// set the fubar messages for people trying to r/w without setting a section
// or trying to write to an embedded .ini file
    sPleaseSetASectionFirst = "Nice try but please set a [Section] first";
    sCannotWriteToEmbedded  = "Cannot write to an embedded .ini file";
}

TWAppSettings::~TWAppSettings()
{
// close the .ini file (harmless if none was opened, i.e nullptr == pSettings)
    delete pSettings;
}

// ---- filename for the embedded flavors
static QString sEmbeddedFileName  = ":/ROSettings.ini";    // fixed text for now

// ---- call this one first to set the current section
// if this is the first call: also open the QSettings instance
void TWAppSettings::setCurrentSection(QString sNewSection)
{
// section name must not be empty
    if (sNewSection.isEmpty())
        fubar("section name cannot be empty or blank");

// no QSettings instance? set one now
    if (nullptr == pSettings)
    // first time, open please
        switch (eIL)
        {
        case eEmbedded  :
            pSettings = new QSettings(sEmbeddedFileName,QSettings::IniFormat);
            bReadOnly = true;
            break;

        case eNextToApp :
            pSettings = new QSettings(TWUtils::getAppName() + "Settings.ini",QSettings::IniFormat);
            bReadOnly = false;
            break;

        case eAppData   :
        #if defined(Q_OS_WIN)
        // on Windows, this creates AppData/Roaming/Tungware/AppName.ini
            pSettings = new QSettings(QSettings::IniFormat,QSettings::UserScope,QApplication::organizationName(),TWUtils::getAppName());
            bReadOnly = false;
        #else
            fubar("eAppData only works on Windows");
        #endif
            break;

        case eRegistry  :
        #if defined(Q_OS_WIN)
        // open HKEY_CURRENT_USER/Software (note: use Registry32Format so 32-bit and 64-bit apps open the same place in the registry)
            pSettings = new QSettings(QSettings::Registry32Format,QSettings::UserScope,QApplication::organizationName(),TWUtils::getAppName());
            bReadOnly = false;
        #else
            fubar("eRegistry only works on Windows");
        #endif
        }

    if (nullptr == pSettings)
        fubar("no QSettings instance set, sorry about that");

// for Qt5 we also need to set the .ini file codec to UTF-8 (this is done automagically in Qt6)
    #if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
        pSettings->setIniCodec("UTF-8");
    #endif

// take care of Qt's default section [General]
    if (QStringLiteral("General") == sNewSection)
        sNewSection = ""; // yes, for [General] Qt really wants an empty section name instead, kid you not

// ok switch to new setting
    sCurrentSection = sNewSection;
}

// this one used primarily for saving screen geometry (mainwindow coords.)
void TWAppSettings::setCurrentSectionBasedOnComputerName()
{
// use a hardwired prefix and append with the computer name we're running on now
    setCurrentSection(QStringLiteral("SettingsFor") + QSysInfo::machineHostName());
}

// also take care of the [General] section here
QString TWAppSettings::currentSection()
{
    return sCurrentSection.isEmpty() ? QStringLiteral("General") : sCurrentSection;
}

// private helpers
QString TWAppSettings::buildKey(QString sKeyName)
{
// note: this works also for empty section names (the standin for [General])
    return sCurrentSection + "/" + sKeyName.trimmed();
}

QString TWAppSettings::buildKeyAndVerifyItExists(QString sKeyName)
{
    QString sKey = buildKey(sKeyName);
    if (!pSettings->contains(sKey))  // (private function, no need for nullptr checking)
        fubar("'%s' file not found or key '%s' not found",qUtf8Printable(pSettings->fileName()),qUtf8Printable(sKey));

    return sKey;
}

// read booleans, integers and strings
bool TWAppSettings::readBool(QString sKeyName)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    return pSettings->value(buildKeyAndVerifyItExists(sKeyName)).toBool();
}

bool TWAppSettings::readBoolWithDefault(QString sKeyName, bool bDefaultValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

// this one returns the Default value if .ini file not found or keyname not found
    return pSettings->value(buildKey(sKeyName),bDefaultValue).toBool();
}

int TWAppSettings::readInt(QString sKeyName)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    return pSettings->value(buildKeyAndVerifyItExists(sKeyName)).toInt();
}

int TWAppSettings::readIntWithDefault(QString sKeyName, int nDefaultValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    return pSettings->value(buildKey(sKeyName),nDefaultValue).toInt();
}

QString TWAppSettings::readString(QString sKeyName)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    return pSettings->value(buildKeyAndVerifyItExists(sKeyName)).toString();
}

QString TWAppSettings::readStringOSSuffix(QString sKeyName)
{
#if defined(Q_OS_WIN)
    QString sSuffix = "Windows";
#endif
#if defined(Q_OS_MACOS)
    QString sSuffix = "MacOS";
#endif
#if defined(Q_OS_LINUX)
    QString sSuffix = "Linux";
#endif
    return readString(sKeyName + sSuffix);
}

QString TWAppSettings::readStringWithDefault(QString sKeyName, QString sDefaultValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    return pSettings->value(buildKey(sKeyName),sDefaultValue).toString();
}

// read lists. arrays and encrypted strings and lists
QStringList TWAppSettings::readStringList(QString sKeyName)
{
    return TWUtils::commaList2StringList(readString(sKeyName));
}

QByteArray TWAppSettings::readByteArray(QString sKeyName)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

// this one returns an empty QByteArray if .ini file not found or keyname not found
    return pSettings->value(buildKey(sKeyName)).toByteArray();
}

QString TWAppSettings::readStringEncrypted(QString sDecrypter, QString sKeyName)
{
// note: this one also returns an empty string if keyname is missing
// require decryption key to be at least 4 bytes long
    if (sDecrypter.length() < 4)
        fubar("nice try but decryption key must be at least of length 4");

// expect a encrypted string in base64 format
    auto baEncrypted = QByteArray::fromBase64(readByteArray(sKeyName));

// do a simple XOR loop decrypt
    QByteArray baClearText;
    QByteArray baDecrypter = sDecrypter.toUtf8();
    for (int i = 0; (i < baEncrypted.length()); ++i)
        baClearText += baEncrypted[i] ^ baDecrypter[i % baDecrypter.length()];

// convert back to Utf8 and then to a QString
    return QString::fromUtf8(baClearText);
}

QStringList TWAppSettings::readStringListEncrypted(QString sDecrypter, QString sKeyName)
{
    return TWUtils::commaList2StringList(readStringEncrypted(sDecrypter, sKeyName));
}


// ----- and the writers -----
void TWAppSettings::writeBool(QString sKeyName, bool bValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    if (bReadOnly)
        fubar(sCannotWriteToEmbedded);

    pSettings->setValue(buildKey(sKeyName),bValue);
}

void TWAppSettings::writeInt(QString sKeyName, int nValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    if (bReadOnly)
        fubar(sCannotWriteToEmbedded);

    pSettings->setValue(buildKey(sKeyName),nValue);
}

void TWAppSettings::writeString(QString sKeyName, QString sValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    if (bReadOnly)
        fubar(sCannotWriteToEmbedded);

    pSettings->setValue(buildKey(sKeyName),sValue);
}

void TWAppSettings::writeStringList(QString sKeyName, QStringList slValue)
{
    writeString(sKeyName,TWUtils::stringList2CommaList(slValue));
}

void TWAppSettings::writeByteArray(QString sKeyName, QByteArray baValue)
{
    if (nullptr == pSettings)
        fubar(sPleaseSetASectionFirst);

    if (bReadOnly)
        fubar(sCannotWriteToEmbedded);

    pSettings->setValue(buildKey(sKeyName),baValue);
}

void TWAppSettings::writeStringEncrypted(QString sEncrypter, QString sKeyName, QString sValue)
{
// require encryption key to be at least by 4 bytes long
    if (sEncrypter.length() < 4)
        fubar("nice try but encryption key must be at least of length 4");

// store our sValue QString into Utf8 and then a QByteArray
    QByteArray baClearText = sValue.toUtf8();

// do a simple XOR loop encrypt
    QByteArray baEncrypted;
    QByteArray baEncrypter = sEncrypter.toUtf8();
    for (int i = 0; (i < baClearText.length()); ++i)
        baEncrypted += baClearText[i] ^ baEncrypter[i % baEncrypter.length()];

// save the encrypted bytearray as a base64 string to the .ini file
    QString sEncrypted64 = baEncrypted.toBase64();  // use a QString, simplifies the tossing
    if (sEncrypted64.endsWith("=")) // toss possible padding
        sEncrypted64.chop(1);
    if (sEncrypted64.endsWith("=")) // - " -
        sEncrypted64.chop(1);

    writeString(sKeyName,sEncrypted64);
}

// flusher
void TWAppSettings::sync()
{
    pSettings->sync();
}


//-------------------------------------------------------------------
// SingleApp support (ctor/dtor)
//
struct TWSingleAppPrivate
{
// use a shared memory instance to implement a single app paradigm
    QSharedMemory sm;
};

// ctor and dtor chaps
TWSingleApp::TWSingleApp()
{
// new up the private chap
    d_ptr = new TWSingleAppPrivate;
}

TWSingleApp::~TWSingleApp()
{
// detach and toss
    d_ptr->sm.detach();
    delete d_ptr;
}

// try to register the current appname as the key
static const int nnKeySize = 1; // one byte suffices
bool TWSingleApp::registerApp()
{
// use the current appname as the key (must be nonblank)
    QString sKey = QApplication::applicationName();

    if (sKey.isEmpty())
        fubar("didn't expect an empty application name");

// note: first try to use nativeKey and "Global\" to make it visible on all sessions on a Terminal Server
    d_ptr->sm.setNativeKey(QString("Global\\") + sKey);
    if (d_ptr->sm.create(nnKeySize))
    // that's it, first kid on the block
        return true;

// else check the error
    if (QSharedMemory::AlreadyExists == d_ptr->sm.error())
    // other instance beat us to it
        return false;

// some other error, tly again, this time with a nonnative key
    d_ptr->sm.setKey(QString("Global\\") + sKey);
    if (d_ptr->sm.create(nnKeySize))
    // that's it, we're first
        return true;

// else check the error
    if (QSharedMemory::AlreadyExists == d_ptr->sm.error())
    // other instance beat us to it
        return false;

// bottom of the barrel :-(
    return false;
}
