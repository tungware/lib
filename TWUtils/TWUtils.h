/*--------------------------------------------------------------------------*/
/* TWUtils.h                                                                */
/*                                                                          */
/* 2014-06-09 First version                                             HS  */
/* 2014-11-29 Add methods for retrieving Swedish day and month names    HS  */
/* 2014-12-06 Support a single application instance using QSharedMemory HS  */
/* 2016-06-01 Add persistent settings class (uses QSettings internally) HS  */
/* 2020-04-13 Kiss the console flavored build goodbye (too much bother) HS  */
/* 2020-07-28 TWBigNum: tiny wrapper class for GMP (Gnu Multiprecision) HS  */
/* 2021-08-08 Say hello to TWTextFileReader and TWTextFileWriter        HS  */
/* 2022-01-27 3-tiered ini files (resource, next to app and in AppData) HS  */
/*--------------------------------------------------------------------------*/

#pragma once

// do the usual library #defines
#if defined(TWUtils_LIBRARY) || defined(TWSTATICBUILD)
#define TWUtilsSHARED_EXPORT Q_DECL_EXPORT
#else
#define TWUtilsSHARED_EXPORT Q_DECL_IMPORT
#endif

// bring in the qApp (note: means apps using TWUtils need to link in gui and widgets)
#include "qapplication.h"

// pull in QWidgets, QDateTime and for Qt5: QTextStream (for the file readers/writers)
#include "qwidget.h"
#include "qdatetime.h"
#if (QT_VERSION < QT_VERSION_CHECK(6, 0, 0))
#include "qtextstream.h"
#endif

#if defined(__GMP_PLUSPLUS__)
//============================================================================
// A Gnu Multiprecision very small wrapper class
/*
# on the Mac: brew install gmp
mac: INCLUDEPATH += /usr/local/opt/gmp/include
mac: LIBS += -L /usr/local/opt/gmp/lib -lgmp
# on Linux/Ubuntu: sudo apt install libgmp-dev
linux: LIBS += -lgmp
# on Windows: maybe later
*/
// don't forget to #include <gmpxx.h> before #including INCLUDETWUTILS

struct TWBignum
{
    mpz_class mpz;            // the beef is in here
    TWBignum(QString s = "0") { mpz = qUtf8Printable(s); }
    TWBignum(mpz_class m)     { mpz = m; }
    TWBignum(int n)           { mpz = n; }
    operator QString()        { return QString::fromStdString(mpz.get_str()); }
};
#endif

//----------------------------------------------------------------------------
// useful QTimer and animation delay settings (in milliseconds)
static constexpr int nnShortDelay      =   10;
static constexpr int nnNormalDelay     =   20;
static constexpr int nnLongDelay       =   75;
static constexpr int nnRubberneckDelay = 2222;

// forward declare some stuff
// mostly for avoiding #including "qfile.h" and "qsettings.h" in here
class QFile;
class QSettings;

// handy console string putters (mostly useful on Windows)
void qputs(QString s);
void qputs(QStringList sl);

// main chap
class TWUtilsSHARED_EXPORT TWUtils
{
public:
//----------------------------------------------------------------------------
// app launching functions
    static void beforeQApplication();                       // stuff that needs to be done *before* QApplication starts
    static void helloQt(QWidget* pMainWindow = nullptr);    // call this when Qt is up and running (eg. QMainWindow's ctor)
    static QString     versionMajorMinor();                 // get major and minor version no. concatenated (Windows only)
    static int         versionMajor();                      // get major version no. as an integer (Windows only)
    static QDate       buildDate(QString sDate = __DATE__); // gets the build/compile date (for the call site)
                                                            // (i.e. use the compiler to fill in the default)
    static QStringList getArgs();                           // returns the argv chaps in a QStringList

//---------------------------------------------------------------------------
// declare a fubar/crash class (calls qFatal with filename/lineno prefixed)
    enum eFubarFlavors { eVanillaFlavor, eGuruMeditationFlavor };

    class TWUtilsSHARED_EXPORT FubarObject
    {
        QString sSourceFileName = "unknown";
        int     nSourceLineNo   = 0;
        eFubarFlavors f         = eVanillaFlavor;

    public:
        FubarObject(QString sFileName, int nLineNo, eFubarFlavors flavor);
        [[noreturn]] void qFatalInvoker(const char* msg,...);
        [[noreturn]] void qFatalInvoker(QString sMsg);  // overload if you prefer a QString (but no trailing args)
    };

// presenting handy macros for crashing/going south -------------------------
    #define fubar          TWUtils::FubarObject(__FILE__, __LINE__, TWUtils::eVanillaFlavor).qFatalInvoker
    #define guruMeditation TWUtils::FubarObject(__FILE__, __LINE__, TWUtils::eGuruMeditationFlavor).qFatalInvoker

// need to exit no questions asked? use this function
    static void ignominiousExit(int nExitCode = EXIT_FAILURE);

// custom Qt message filter (for saving qFatal's messages to a logfile)
    static void twMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg);

// ---------------------------------------
// get the current directory and app name
    static QString getCurrentDirectory();
    static QString getAppName();

// is the shift key pressed on the keyboard? use this for real-time readout (not queued)
    static bool isShiftKeyDown();

//----------------------------------------------------------------------------
// common app font functions
    static QString getDefaultFontName();
    static int     getDefaultFontSize();

//----------------------------------------------------------------------------
// ui functions
    class TWUtilsSHARED_EXPORT WaitCursor
    {
    public:
    // helper class for showing an hourglass mouse cursor until you're done
    // just create an instance of this chap at the top of your lengthy function
        WaitCursor();
        ~WaitCursor();
    };

    static void noTitleBarNoButtons(QWidget* w);
    static void titleBarNoButtons  (QWidget* w);
    static QString truncateWithTrailingEllipses(QWidget* pWidget, QString s);           // truncates if needed to fit into the QWidget's font and width
                                                                                        // note: similar func below exists (takes a string and a length)
// activates and hoists widget to the top of the z-order
    static void makeTopWindow(QWidget* w);

// (custom made for you) dialog boxes
    static bool yesNoBox   (QString sText,QString sTitle = "");                         // messagebox with "Yes" and "No"; click on "Yes" -> true
    static bool okCancelBox(QString sText,QString sTitle = "");                         // this one shows "Ok" and "Cancel", click on "Ok" -> true
    static void infoBox    (QString sText,QString sTitle = "", int msecsTimeout = 0);   // shows Ok button and an info icon
    static void warningBox (QString sText,QString sTitle = "", int msecsTimeout = 0);   // shows Ok button and a warning icon
    static void errorBox   (QString sText,QString sTitle = "", int msecsTimeout = 0);   // shows Ok button and an error icon
    static void twinkleBox (QString sText,QString sTitle = "", int msecsTimeout = 0);   // look ma no hands (no button and no icon)

// passwordBox() has currently been put out to pasture, to resurrect it just uncomment the next line
// #define USE_PASSWORDBOX
#if defined(USE_PASSWORDBOX)
static QStringList passwordBox(QString sName,QString sTitle = "");  // user clicked Ok: returns name/password, clicked Cancel: returns 2 empty strings
#endif

//---------------------------------------------------------------------------------------------------
// Qt specific stuff: bug/feature fixes, style and stylesheet helpers
//
// note for MacOS: to opt out of dark mode for your app, add this line to Info.plist in your app folder:
// <key>NSRequiresAquaSystemAppearance</key><string>true</string>
    static void fixMyWidgets(QWidget* pTopWidget);      // quirk fixing (a lot) for your widgets
    static void fixedSizeWindow(QWidget* pMainWindow);  // bonus extra: sets your app window non-resizable

// for more decoration you can use these stylesheet helpers
    static QString backgroundStyleMainWindow (QColor c);
    static QString foregroundStyle           (QColor c); // use these for foreground/background color setters for table/list widgets
    static QString backgroundStyle           (QColor c);
    static QString alternativeBackgroundStyle(QColor c); // note: for listWidgets (to keep the scrollbars rendered ok) use this one
    static QString selectedBackgroundStyle   (QColor c);

//------------------------------------------------------------------
// handy UTF-8 char sequences
    static QString rightArrow()      { return QByteArray::fromHex("e2 86 92"); }
    static QString midLineEllipsis() { return QByteArray::fromHex("e2 8b af"); }

    #if defined(MAYBE_USEFUL_IN_THE_BRIGHT_FUTURE)
    // static QString leftArrow()     { return QByteArray::fromHex("e2 86 90"); }
    // static QString upArrow()       { return QByteArray::fromHex("e2 86 91"); }
    // static QString upArrowDashed() { return QByteArray::fromHex("e2 87 a1"); }
    // static QString thumbsUp()      { return QByteArray::fromHex("f0 9f 91 8d"); }
    #endif

//------------------------------------------------------------------
// date and time utility methods
// note: the xxYMDxx and xxHHMMxx functions are just aliases/shortcuts (saving some typing for you)
    static QString toISODate(QDate     d);                    // e.g. returns "2025-11-17" for 11/17/25
    static QString toYMD    (QDate     d);                    // e.g. returns "20251117" for 11/17/25
    static QString toISODate(QDateTime dt);                   // same as above but expects QDateTimes
    static QString toYMD    (QDateTime dt);                   //             -  "  -

    static QString toISOTime(QTime     t);				      // e.g. "08:23:14"
    static QString toISOTime(QDateTime dt);				      // same as above
    static QString toISOTimeNoSeconds(QTime     t);		      // e.g. "08:23"
    static QString toHHMM            (QTime     t);           // same as toISOTimeNoSeconds()
    static QString toISOTimeNoSeconds(QDateTime dt);	      //
    static QString toHHMM            (QDateTime dt);          //          -  "  -

    static QString toISODateTime         (QDateTime dt);	  // e.g. "2024-12-01 08:23:14"
    static QString toISODateTime         (QDate d, QTime t);  // same as above
    static QString toISODateTimeNoSeconds(QDateTime dt);      // e.g. "2024-12-01 08:23"
    static QString toISODateTimeNoSeconds(QDate d, QTime t);  // same as above
    
    static QDate     fromISODate(QString s);                  // parses YYYY-MM-DD into a QDate
    static QDate     fromYMD    (QString s);                  // parses YYYYMMDD into a QDate
    static QDateTime fromISODateTime         (QString s);     // parses YYYY-MM-DD HH:MM:SS into a QDateTime
    static QDateTime fromISODateTimeNoSeconds(QString s);     // parses YYYY-MM-DD HH:MM into a QDateTime
    static QTime     fromISOTimeNoSeconds(QString s);	      // parses HH:MM into QTime
    static QTime     fromHHMM            (QString s);         // same as above

//------------------------------------------------------------------
// misc. QString functions
    static QStringList commaList2StringList(QString      s);  // "a,b,c" --> "a","b","c"
    static QString     stringList2CommaList(QStringList sl);  // "a","b","c" --> "a,b,c"

    static bool    isEqualWildcards(QString a, QString b);    // accepts wildcards (? and *) either in a or b

    static QString pascalCase          (QString s);           // "now Is THE time" --> "Now Is The Time"
    static QString pascalCaseIfAllUpper(QString s);           // "NOW IS THE TIME" --> "Now Is The Time" (i.e. checks for ALL UPPER)
    static QString tossAllButDigits    (QString s);
    static QString tossLeadingSpaces   (QString s);
    static QString tossAllWhiteSpace   (QString s);

    static QString truncateWithTrailingEllipses(QString s, int nMaxLength);

//------------------------------------------------------------------
// get Swedish month and weekday names
    static QString getSwedishMonthName     (int nMonthNo  );    // January = 1 ... December = 12
    static QString getSwedishShortMonthName(int nMonthNo  );    // Jan = 1 ... Dec = 12
    static QString getSwedishWeekdayName   (int nWeekdayNo);    // monday = 1 ... sunday = 7
    static QString getSwedishWeekdayName   (QDate d) { return getSwedishWeekdayName(d.dayOfWeek()); }
    static QString getSwedishWeekdayNameU1 (int nWeekdayNo);    // Monday = 1 ... Sunday = 7
    static QString getSwedishWeekdayNameU1 (QDate d) { return getSwedishWeekdayNameU1(d.dayOfWeek()); }

//-------------------------------------------------------------------
// Swedish personal.no support functions
    static int     ageInYears         (QDate dDOB, QDate dTarget);  // returns the age in years on the specified target date
    static QString prettyPrintPersonNo(QString s12Digits);          // reformat 12 digits to CC YYMMDD-NNNN

    static int     getLuhnCheckDigit  (QString sDigits);        // get the Luhn check digit (assumes sDigits has no trailing check digit)
    static bool    isLuhnCheckDigitOk (QString sDigits);        // returns true if the last digit is a correct Luhn value
    static bool    isReservNo         (QString sPersonNo);      // returns true if the personno is of reservno. flavor
    static QString checkPersonNo      (QString sPersonNo);      // returns "" for a valid personno else a Swedish error message
};


//---------------------------------------------------------------------------------------------
// file/textstream helper classes
class TWUtilsSHARED_EXPORT TWTextFileReader
{
public:
    TWTextFileReader();
    TWTextFileReader(QString sFileName, bool bUtf8 = true); // false --> use the ISO 8859-1 codec
    ~TWTextFileReader();    // deallocate the QFile

    void open(QString sFileName, bool bUtf8 = true);        // use this if you called the default constructor
    bool isOpen();                                          // returns true if the file is opened
    QTextStream ts;                                         // use this textstream to get stuff from the file
    QStringList readAll();                                  // or retrieve all of it into a stringlist
    void close();

    static bool exists(QString sFileName);                  // returns true if that filename exists

// static helper that reads it all into a QStringList
    static QStringList readAll(QString sFileName, bool bUtf8 = true);

private:
    QFile* f; // actual file i/o happens here
};

class TWUtilsSHARED_EXPORT TWTextFileWriter
{
public:
    TWTextFileWriter();
    TWTextFileWriter(QString sFileName, bool bUtf8 = true);  // false --> use the ISO 8859-1 codec
    ~TWTextFileWriter();                                     // closes/kisses the QFile goodbye

    void open(QString sFileName, bool bUtf8 = true);        // use this if you called the default constructor
    bool isOpen();                                          // returns true if the file is opened
    QTextStream ts;                                         // use this textstream to write stuff to the file
    void writeStringList(QStringList sl);                   // or write lots of lines at once
    void close();

// static helpers
    static void writeAll(QString sFileName, QStringList sl, bool bUtf8 = true);
    static void writeBinaryFile(QString sFileName, QByteArray ba);
    static bool exists(QString sFileName);        // returns true if the file exists
    static bool canCreateFile(QString sFileName); // returns true if the file can be created

private:
    QFile* f; // actual file i/o happens here
};


//-------------------------------------------------------------------------------------------
// App persistence using .ini files
// note: example for your header file: TWAppSettings settings{TWAppSettings::eEmbedded};
// ------------------------------------------------------------------------------------------
class TWUtilsSHARED_EXPORT TWAppSettings
{
public:
// introduce the different settings flavors
// note: if you're not on Windows, eAppData and eRegistry are the same as eNextApp (i.e. "Settings.ini" suffixed)
    enum IniLocation { eEmbedded, eNextToApp, eAppData, eRegistry };

    TWAppSettings(IniLocation eLocation = eNextToApp);  // (default location in ctor: same directory as the app)
    ~TWAppSettings();

// first you need to set a section, e.g. [MySection] or based on computer name (prefixed with "SettingsFor")
    void setCurrentSection(QString sNewSection);
    void setCurrentSectionBasedOnComputerName();
    QString currentSection();

// readers: for booleans, integers and strings they come in 2 flavors: with and without defaults
// without defaults: requires the current section and keyname to be present in the .ini file
    bool readBool                (QString sKeyName);
    bool readBoolWithDefault     (QString sKeyName, bool bDefaultValue);
    int  readInt                 (QString sKeyName);
    int  readIntWithDefault      (QString sKeyName, int nDefaultValue);
    QString readString           (QString sKeyName);
    QString readStringOSSuffix   (QString sKeyName);  // appends either "Windows", "MacOS" or "Linux" to the keyname
    QString readStringWithDefault(QString sKeyName, QString sDefaultValue);

    QStringList readStringList         (QString sKeyName);  // reads a comma separated list from a single line
    QByteArray  readByteArray          (QString sKeyName);  // note: returns empty for a nonexisting key
    QString     readStringEncrypted    (QString sDecrypter, QString sKeyName); // returns empty for missing key
    QStringList readStringListEncrypted(QString sDecrypter, QString sKeyName); // reads an encrypted comma sep. list

// writers: simpler, just one flavor
    void writeBool           (QString sKeyName, bool         bValue);
    void writeInt            (QString sKeyName, int          nValue);
    void writeString         (QString sKeyName, QString      sValue);
    void writeStringList     (QString sKeyName, QStringList slValue);
    void writeByteArray      (QString sKeyName, QByteArray  baValue);
    void writeStringEncrypted(QString sEncrypter, QString sKeyName, QString sValue);

// extra stuff
    void sync();  // flushes any unwritten changes

private:
// helpers
    QString sPleaseSetASectionFirst;                     // fubar message when trying r/w without a section set
    QString sCannotWriteToEmbedded;                      // fubar message when trying to write to an embedded .ini
    QString buildKey                 (QString sKeyName); // used by the readers with defaults and all the writers
    QString buildKeyAndVerifyItExists(QString sKeyName); // used by the readers without defaults

// QSettings instance, location and section name
    IniLocation eIL;
    QSettings*  pSettings;
    bool        bReadOnly;  // if true: reading from inside the .exe e.g. ":/ROSettings.ini"
    QString     sCurrentSection;
};


//-------------------------------------------------------------------------------
// helper class for implementing an app singleton
struct TWSingleAppPrivate;  // (just forward it in this .h file)

class TWUtilsSHARED_EXPORT TWSingleApp
{
public:
    TWSingleApp();

// to check if we're running already, try to register the current appname
// if this function returns false, some other instance beat us to it
    bool registerApp();

// for tossing the shared memory
    ~TWSingleApp();

private:
    TWSingleAppPrivate* d_ptr;
};
