#--------------------------------------------------------------------------#
# TWUtils.pro                                                              #
#                                                                          #
# 2014-06-09 First version                                             HS  #
# 2018-08-30 Introduce a console (non-GUI) build version               HS  #
# 2020-04-13 Kiss our console flavored build goodbye (too much bother) HS  #
#--------------------------------------------------------------------------#

QT      += core gui widgets

DEFINES += TWUtils_LIBRARY

HEADERS += TWUtils.h
SOURCES += TWUtils.cpp

TARGET   = TWUtils
TEMPLATE = lib

# include our common QtProjects helper
include(../../include/QtProjects.pri)

# if we're on Windows and MSVC then we need to pull in the user32 library
win32-msvc: LIBS += -luser32

# (no need for any TWLiblines for TWUtil)
